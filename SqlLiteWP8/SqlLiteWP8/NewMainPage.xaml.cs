﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Windows.Media.Imaging;
using System.IO.IsolatedStorage;
using System.Collections.ObjectModel;
using Microsoft.Phone.Notification;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.Phone.Shell;
using SQLite;
using SqlLiteWP8.Model;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Info;



namespace SqlLiteWP8
{
    public partial class NewMainPage : PhoneApplicationPage
    {
        // Constructor
        string notificationMessage;
        //IsolatedStorageSettings settings;
        ObservableCollection<SideBarItem> trainStations = new ObservableCollection<SideBarItem>();
        string channelName = "MindsArrayChannel";
        HttpNotificationChannel pushChannel;
        public static string MPNSString;
          public static bool isNetOrSms;
          NotificationListDB notificationDataDB;
          SQLiteAsyncConnection conn;
        public NewMainPage()
        {
            // Login.isTrue = false;
            InitializeComponent();
           // LocationAddress.GetLocation();
           
           // string a = DeviceStatus.DeviceManufacturer + " " + Environment.OSVersion.Version.ToString(); 

            Constatnt.hideControls(border1, border2, balText);
            Login.isTrue = Constatnt.LoadPersistent<Boolean>(Constatnt.IS_LOGIN);
            Constatnt.mCurrentBalance = Constatnt.LoadPersistent<String>(Constatnt.CURRENT_BALANCE);
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            
            //CreateDatabase();
            Constatnt.SHOPNAME=Constatnt.LoadPersistent<String>("merchant");
            //Boolean net = Constatnt.SavePersistent("isNetOrSms",true);
            
            notificationDataDB = new NotificationListDB();
            isNetOrSms = Constatnt.LoadPersistent<Boolean>("isNetOrSms");
            if (Login.isTrue)
            {
                Cookie cookie = Constatnt.LoadPersistent<Cookie>("cookie");
                cookie.Name = Constatnt.LoadPersistent<String>("cookieName");
                TextMerchant.Text = Constatnt.SHOPNAME;
                cookie.Value = Constatnt.LoadPersistent<String>("cookieValue");
                Login.cooki = new Cookie(cookie.Name, cookie.Value);
                Console.WriteLine("sgsdfg");
            }

            /* Cookie cookie = new Cookie();
            string name = Constatnt.LoadPersistent<string>("cookie");
            if (name == null)
            {
                
            }
            else
            {
                cookie.Name = name;
            }

            string value = Constatnt.LoadPersistent<string>("cookieName");
            if (value == null)
            {
            }
            else
            {
                cookie.Value = value;
                //Cookie cookie = new Cookie(Constatnt.LoadPersistent<string>
            }
            //Cookie cookie = new Cookie(Constatnt.LoadPersistent<string>("cookie"),Constatnt.LoadPersistent<string>("cookieName"));
            System.Diagnostics.Debug.WriteLine(name+"  dfgysdhststystystrystysaerysteysteysty       "+value);
            MessageBox.Show(name);
            MessageBox.Show(value);
           // Login.cooki = cookie;// Constatnt.LoadPersistent<Cookie>("cookie");
            Console.WriteLine("dsg");*/
            try
            {

                // Try to find the push channel.
                pushChannel = HttpNotificationChannel.Find(channelName);

                // If the channel was not found, then create a new connection to the push service.
                if (pushChannel == null)
                {
                    pushChannel = new HttpNotificationChannel(channelName);

                    // Register for all the events before attempting to open the channel.
                    pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                    pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);

                    // Register for this notification only if you need to receive the notifications while your application is running.
                    pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(PushChannel_ShellToastNotificationReceived);

                    pushChannel.Open();

                    // Bind this new channel for toast events.
                    pushChannel.BindToShellToast();
                    pushChannel.BindToShellTile();

                }
                else
                {

                    pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                    pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);


                    pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(PushChannel_ShellToastNotificationReceived);


                    System.Diagnostics.Debug.WriteLine(pushChannel.ChannelUri.ToString());
                    /*  MessageBox.Show(String.Format("Channel Uri is {0}",
                          pushChannel.ChannelUri.ToString()));*/

                    MPNSString = pushChannel.ChannelUri.ToString();

                }
            }
            catch (Exception e)
            {
                //Console.WriteLine("dfghf");
            }
            //Constatnt.SetProgressIndicator(true);

        }


        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }

        private async void CreateDatabase()
        {
            conn = new SQLiteAsyncConnection("plans");
            await conn.CreateTableAsync<NotificationListDB>();


        }


        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
            Constatnt.handleSidebar(index, this.NavigationService, "NewMainPage");

        }


        private void Click_Mobile(object sender, RoutedEventArgs e)
        {
            if (Login.isTrue)
            {
                NavigationService.Navigate(new Uri("/MobileMain.xaml", UriKind.Relative));
            }
            else
            {
                if (isNetOrSms)
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                }
                else
                {
                    MessageBoxResult result =
                   MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                           "Settings", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        NavigationService.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                    }
                    if (result == MessageBoxResult.Cancel)
                    {
                        NavigationService.Navigate(new Uri("/MobileMain.xaml?", UriKind.Relative));
                    }
                }

                /* MessageBox.Show("Please login First.");
                 NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));*/
            }


        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
            //NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }


        private void col20(object sender, RoutedEventArgs e)
        {
            if (Login.isTrue)
            {
                NavigationService.Navigate(new Uri("/EntertainmentMain.xaml", UriKind.Relative));
            }
            else
            {
                if (isNetOrSms)
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                }
                else
                {
                    MessageBoxResult result =
                   MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                           "Settings", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        NavigationService.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                    }
                    if (result == MessageBoxResult.Cancel)
                    {
                        NavigationService.Navigate(new Uri("/EntertainmentMain.xaml?", UriKind.Relative));
                    }
                }

                /* MessageBox.Show("Please login First.");
                 NavigationService.Navigate(new Uri("/Login.xaml?page=EntertainmentMain", UriKind.Relative));*/
            }

        }

        private void data_Click(object sender, RoutedEventArgs e)
        {
            if (Login.isTrue)
            {
                NavigationService.Navigate(new Uri("/DataCardmain.xaml", UriKind.Relative));
            }
            else
            {


                MessageBox.Show("Please login First.");
                NavigationService.Navigate(new Uri("/Login.xaml?page=DataCardmain", UriKind.Relative));
            }

        }

        private void Quick_Click(object sender, RoutedEventArgs e)
        {

            if (Login.isTrue)
            {
                NavigationService.Navigate(new Uri("/NewQuickRecharge.xaml", UriKind.Relative));
                //NavigationService.Navigate(new Uri("/DataCardmain.xaml", UriKind.Relative));
            }
            else
            {
                if (isNetOrSms)
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                }
                else
                {
                    MessageBoxResult result =
                   MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                           "Settings", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        NavigationService.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                    }
                    if (result == MessageBoxResult.Cancel)
                    {
                        NavigationService.Navigate(new Uri("/NewQuickRecharge.xaml?", UriKind.Relative));
                    }
                }
                /*  MessageBox.Show("Please login First.");
                  NavigationService.Navigate(new Uri("/Login.xaml?page=NewQuickRecharge", UriKind.Relative));*/
            }

        }

        private void Click_Dth(object sender, RoutedEventArgs e)
        {

            if (Login.isTrue)
            {
                NavigationService.Navigate(new Uri("/DTHMain.xaml", UriKind.Relative));
                //NavigationService.Navigate(new Uri("/DataCardmain.xaml", UriKind.Relative));
            }
            else
            {
                if (isNetOrSms)
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                }
                else
                {
                    MessageBoxResult result =
                   MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                           "Settings", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        NavigationService.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                    }
                    if (result == MessageBoxResult.Cancel)
                    {
                        NavigationService.Navigate(new Uri("/DTHMain.xaml?", UriKind.Relative));
                    }
                }

                /* MessageBox.Show("Please login First.");
                 NavigationService.Navigate(new Uri("/Login.xaml?page=DTHMain", UriKind.Relative));*/
            }

        }

        private void Click_Reversal(object sender, RoutedEventArgs e)
        {
            if (Login.isTrue)
            {
                NavigationService.Navigate(new Uri("/ReversalNewMain.xaml", UriKind.Relative));
                //NavigationService.Navigate(new Uri("/DataCardmain.xaml", UriKind.Relative));
            }
            else
            {
                if (isNetOrSms)
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                }
                else
                {
                    MessageBoxResult result =
                   MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                           "Settings", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        NavigationService.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                    }
                    if (result == MessageBoxResult.Cancel)
                    {
                        NavigationService.Navigate(new Uri("/ReversalNewMain.xaml?", UriKind.Relative));
                    }
                }
                /*MessageBox.Show("Please login First.");
                NavigationService.Navigate(new Uri("/Login.xaml?page=ReversalMain", UriKind.Relative));*/
            }

        }

        private void Click_Settings(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.Relative));
        }

        private void col12(object sender, RoutedEventArgs e)
        {
            if (Login.isTrue)
            {
                NavigationService.Navigate(new Uri("/BillPaymentMain.xaml", UriKind.Relative));
                //NavigationService.Navigate(new Uri("/DataCardmain.xaml", UriKind.Relative));
            }
            else
            {
                if (isNetOrSms)
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                }
                else
                {
                    MessageBoxResult result =
                   MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                           "Settings", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        NavigationService.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                    }
                    if (result == MessageBoxResult.Cancel)
                    {
                        NavigationService.Navigate(new Uri("/BillPaymentMain.xaml?", UriKind.Relative));
                    }
                }

                /*MessageBox.Show("Please login First.");
                NavigationService.Navigate(new Uri("/Login.xaml?page=BillPaymentMain", UriKind.Relative));*/
            }

        }

        private void Click_Chat(object sender, RoutedEventArgs e)
        {
            if (Login.isTrue)
            {
                NavigationService.Navigate(new Uri("/SocketCheck.xaml", UriKind.Relative));
                //NavigationService.Navigate(new Uri("/DataCardmain.xaml", UriKind.Relative));
            }
            else
            {
                if (isNetOrSms)
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                }
                else
                {
                    MessageBoxResult result =
                   MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                           "Settings", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        NavigationService.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                    }
                    if (result == MessageBoxResult.Cancel)
                    {
                        NavigationService.Navigate(new Uri("/ReportMain.xaml?", UriKind.Relative));
                    }
                }
                /* MessageBox.Show("Please login First.");
                 NavigationService.Navigate(new Uri("/Login.xaml?page=ReportMain", UriKind.Relative));*/
            }

        }

        private void Click_Report(object sender, RoutedEventArgs e)
        {
            if (Login.isTrue)
            {
                NavigationService.Navigate(new Uri("/ReportMain.xaml", UriKind.Relative));
                //NavigationService.Navigate(new Uri("/DataCardmain.xaml", UriKind.Relative));
            }
            else
            {
                if (isNetOrSms)
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                }
                else
                {
                    MessageBoxResult result =
                   MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                           "Settings", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        NavigationService.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                    }
                    if (result == MessageBoxResult.Cancel)
                    {
                        NavigationService.Navigate(new Uri("/ReportMain.xaml?", UriKind.Relative));
                    }
                }
                /* MessageBox.Show("Please login First.");
                 NavigationService.Navigate(new Uri("/Login.xaml?page=ReportMain", UriKind.Relative));*/
            }

        }

        private void col03(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("nkljkj");
        }





        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }


        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void Click_Bus(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BusBookingSearch.xaml", UriKind.Relative));
        }

        private void Click_Live_Support(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/SocketCheck.xaml", UriKind.Relative));
        }



        void PushChannel_ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {

            Dispatcher.BeginInvoke(() =>
            {
                // Display the new URI for testing purposes.   Normally, the URI would be passed back to your web service at this point.
                System.Diagnostics.Debug.WriteLine(e.ChannelUri.ToString());
             

                MPNSString = e.ChannelUri.ToString();

            });
        }

        /// <summary>
        /// Event handler for when a push notification error occurs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void PushChannel_ErrorOccurred(object sender, NotificationChannelErrorEventArgs e)
        {
            // Error handling logic for your particular application would be here.
            Dispatcher.BeginInvoke(() =>
                MessageBox.Show(String.Format("A push notification {0} error occurred.  {1} ({2}) {3}",
                    e.ErrorType, e.Message, e.ErrorCode, e.ErrorAdditionalData))
                    );
        }


        void PushChannel_ShellToastNotificationReceived(object sender, NotificationEventArgs e)
        {
             SQLiteAsyncConnection conn = new SQLiteAsyncConnection("plans");
             /*StringBuilder message = new StringBuilder();
             string relativeUri = string.Empty;
           

             message.AppendFormat("Received Toast {0}:\n", DateTime.Now.ToShortTimeString());

             // Parse out the information that was part of the message.
             int i = 0;
             foreach (string value in e.Collection.Values)
             {
                 i++;
                 // message.AppendFormat("{0}: {1}\n", key, e.Collection[key]);
                 string str = value;
                 notificationMessage = str;
                 if (i == 3)
                 {
                     notificationMessage = notificationMessage.Remove(0, 1);
                     var dict = (JObject)JsonConvert.DeserializeObject(notificationMessage);
                     //notificationMessage = dict.ToString();
                 }
                 System.Diagnostics.Debug.WriteLine(str);
                 /*  if (string.Compare(
                       key,
                       "wp:Param",
                       System.Globalization.CultureInfo.InvariantCulture,
                       System.Globalization.CompareOptions.IgnoreCase) == 0)
                   {
                       relativeUri = e.Collection[key];
                   }*/
              /* }


               foreach (string key in e.Collection.Keys)
               {
                   message.AppendFormat("{0}: {1}\n", key, e.Collection[key]);

                   if (string.Compare(
                       key,
                       "wp:Param",
                       System.Globalization.CultureInfo.InvariantCulture,
                       System.Globalization.CompareOptions.IgnoreCase) == 0)
                   {
                       relativeUri = e.Collection[key];
                   }
               }

            // Display a dialog of all the fields in the toast.

             Dispatcher.BeginInvoke(() => MessageBox.Show(message.ToString()));
              ShellToast toast = new ShellToast();
              toast.Title = "Pay1";
              toast.Content = "Mindsarray";
            
              string con = toast.Content;
              toast.Show();*/


            StringBuilder message = new StringBuilder();
            string relativeUri = string.Empty;

            message.AppendFormat("Received Toast {0}:\n", DateTime.Now.ToShortTimeString());

            // Parse out the information that was part of the message.
            Dispatcher.BeginInvoke(() =>  MessageBox.Show(e.Collection["wp:Text1"] + "\n" + e.Collection["wp:Text2"]));

            Task<string> abc = AddNotificationToDatabase(e.Collection["wp:Text1"], e.Collection["wp:Text2"], e.Collection["wp:Text3"], e.Collection["wp:Text4"]);
            foreach (string key in e.Collection.Keys)
            {
                message.AppendFormat("{0}: {1}\n", key, e.Collection[key]);
                if (key.Equals("wp:Text1"))
                {
                    
                }

                if (key.Equals("wp:Text2"))
                {

                }

                if (key.Equals("wp:Text3"))
                {

                }

                if (key.Equals("wp:param"))
                {

                }
                
                //Task<string> abc = AddNotificationToDatabase(e.Collection["wp:Text1"], e.Collection["wp:Text2"], e.Collection["wp:Text3"], e.Collection["wp:Text1"]);
                if (string.Compare(
                    key,
                    "wp:Param",
                    System.Globalization.CultureInfo.InvariantCulture,
                    System.Globalization.CompareOptions.IgnoreCase) == 0)
                {
                    relativeUri = e.Collection[key];
                }
            }

            // Display a dialog of all the fields in the toast.
           // Dispatcher.BeginInvoke(() => MessageBox.Show(message.ToString()));


        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            
            if (NavigationContext.QueryString.ContainsKey("pull"))
            {
                string pull = NavigationContext.QueryString["pull"];

                if (pull.Equals("true"))
                {
                    NavigationService.Navigate(new Uri("/NotificationList.xaml?pull=noti", UriKind.Relative));
                    //Task<string> abs = MakeWebRequestForStatus();
                }
            }



        }

        public async Task<string> AddNotificationToDatabase(string a,string b,string c,string d)
        {
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection("plans");
        /*    //SQLiteCommand sqlCommand = new SQLiteCommand(conn);
            notificationDataDB.notificationMobileID = c;
            notificationDataDB.notificationData = b;
            notificationDataDB.notificationTime = c;
            notificationDataDB.notificationType = d;
            //notificationDataDB.notificationID = 1000;
            
            await conn.InsertAsync(notificationDataDB);*/

            NotificationListDB not=new NotificationListDB();
            not.notificationMobileID = c;
            not.notificationTime = d;
            not.notificationData = b;
            await conn.InsertAsync(not);
            int count = Constatnt.LoadPersistent<int>(Constatnt.NOTIFICATION_COUNT);
            //notificationCount.Text = count+"";
           IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            settings[Constatnt.NOTIFICATION_COUNT] = ++count;
            settings.Save();
            var query = conn.Table<NotificationListDB>();//.Where(x =>x.notificationID == 1);
               // var col = conn.QueryAsync<Plans>("SELECT * FROM Plans WHERE planOpertorID='2' && planCircleID='BR'");
                Console.WriteLine("xfggf");
               
                var result = await query.ToListAsync();

           
            return "aa";
        }

        private void btnClickWallet(object sender, RoutedEventArgs e)
        {
            if (Login.isTrue)
            {
                NavigationService.Navigate(new Uri("/Pay1 Wallet.xaml", UriKind.Relative));
            }
            else
            {
                if (isNetOrSms)
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                }
                else
                {
                    MessageBoxResult result =
                   MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                           "Settings", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        NavigationService.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                    }
                    if (result == MessageBoxResult.Cancel)
                    {
                        NavigationService.Navigate(new Uri("/Pay1 Wallet.xaml?", UriKind.Relative));
                    }
                }

                /* MessageBox.Show("Please login First.");
                 NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));*/
            }
        }


        async private void GetLocation()
        {
            try
            {
                Geolocator locator = new Geolocator();

                Geoposition position = await locator.GetGeopositionAsync();

                Geocoordinate coordinate = position.Coordinate;

                string Location = "Latitude = " + coordinate.Latitude + " Longitude = " + coordinate.Longitude;

                MessageBox.Show(Location);
            }
            catch (Exception e)
            {
               string e1= e.Message; ;
            }

        }


    }




}