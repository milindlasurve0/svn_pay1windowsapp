﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SqlLiteWP8.Model;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using SQLite;

using System.Diagnostics;
using System.Windows.Threading;
using System;

namespace SqlLiteWP8
{
    public partial class PlansActivity : PhoneApplicationPage
    {
        Plans CirclePlans = new Plans();
        SQLiteAsyncConnection conn;
        string code;
        int i = 0;
        bool isNetOrSms;
        DispatcherTimer newTimer;
        string op_id;
        public PlansActivity()
        {
            InitializeComponent();

           

            CreateDatabase();
            isNetOrSms = Constatnt.LoadPersistent<Boolean>("isNetOrSms");
            
         /*  Task<string> abc = MakeWebRequest("http://192.168.0.32:9091/apis/receiveWeb/mindsarray/mindsarray/json?method=getPlanDetails&operator=2&circle=BR");*/

       
        }


        private void Home_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }


        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }


        private async void getRechargetype()
        {
            List<String> lst=new List<string>();
             var query = conn.Table<Plans>();
             string val = "value";
             List<Plans> result = await query.ToListAsync();
             List<string> l = new List<string>();
             if (!result.Exists(x => x.planType == val))
                 foreach (var item in result)
                 {
                     lst.Add(item.planType);
                 }

           /*  IEnumerable<string> uniqueValues = lst.SelectMany(x => x).Distinct();
             int uniqueCount = uniqueValues.Count();
            String[] unique = duration.Distinct().ToArray();*/


        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            if (NavigationContext.QueryString.ContainsKey("op_id") && NavigationContext.QueryString.ContainsKey("code"))
            {
                op_id = NavigationContext.QueryString["op_id"];
                code = NavigationContext.QueryString["code"];
                getData();
            }

            /*if (NavigationContext.QueryString.ContainsKey("code"))
            {
                code = NavigationContext.QueryString["code"];
               
                //getRechargetype();
                //

            }*/
            
        }
        
        public async Task<string> MakeWebRequest(string apiUrl)
        {
            Constatnt.SetProgressIndicator(true);
            String result = "";
            var httpClient = new HttpClient(new HttpClientHandler());

            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, null);
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            int count = 0;
            try
            {

                string output = responseString.Remove(responseString.Length - 1, 1);
                string output1 = output.Remove(output.Length - 1, 1);
                string output2 = output1.Remove(0, 1);
                string output3 = output2.Remove(0, 1);
                string output4 = output3.Remove(output3.Length - 1, 1);
                var dict = (JObject)JsonConvert.DeserializeObject(output4);
                var desc = dict[op_id];
               

                var prod_code_pay1 = desc["prod_code_pay1"];
                CirclePlans.planOpertorID = prod_code_pay1.ToString();

                var opr_name = desc["opr_name"];
                CirclePlans.planOperatorName = opr_name.ToString();

                var circles = desc["circles"];



                var BR = circles[code];
                var circle_id = BR["circle_id"];
                CirclePlans.planCircleID = circle_id.ToString();
                var circle_name = BR["circle_name"];
                CirclePlans.planCircleName = circle_name.ToString();
                var plans = BR["plans"];
                JObject jObject = (JObject)JsonConvert.DeserializeObject(plans.ToString());
                Dictionary<string, string> res = new Dictionary<string, string>(jObject.Count);
                foreach (var kvp in jObject)
                {
                    var planType = kvp.Key;
                    var planDetails = kvp.Value;
                    CirclePlans.planType = planType.ToString();
                    foreach (var plansDetails in planDetails)
                    {
                        count++;
                        var plan_amt = plansDetails["plan_amt"];
                        CirclePlans.planAmount = Convert.ToInt32(plansDetails["plan_amt"].ToString());
                        var plan_validity = plansDetails["plan_validity"];
                        CirclePlans.planValidity = plan_validity.ToString();
                        var plan_desc = plansDetails["plan_desc"];
                        CirclePlans.planDescription = plan_desc.ToString();
                        CirclePlans.planUptateTime = DateTime.Now;//.ToString();
                        await conn.InsertAsync(CirclePlans);
                    }
                }






            }
            catch (Exception e)
            {
                Console.WriteLine("");
            }

            if (count == 0 && count == 1)
            {
                MessageBox.Show("No Plans Available.");
                NavigationService.Navigate(new Uri("/RechargeMobile.xaml?", UriKind.Relative));
                NavigationService.RemoveBackEntry();
            }
            else
            {
                getData();
            }

            Constatnt.SetProgressIndicator(false);
            return result;

        }

        private async void CreateDatabase()
        {
            conn = new SQLiteAsyncConnection("plans");
            await conn.CreateTableAsync<Plans>();
        
                
        }

     

        public async void getData()
        {
            Constatnt.SetProgressIndicator(true);
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection("plans");

            try
            {
                //Plans objPersonMaster = conn.Query<Plans>("select * from PersonMaster where personId = ?", 9).First();


                var query = conn.Table<Plans>().Where(x => x.planCircleID == code && x.planOpertorID == op_id);
                // var col = conn.QueryAsync<Plans>("SELECT * FROM Plans WHERE planOpertorID='2' && planCircleID='BR'");
                Console.WriteLine("xfggf");
                //var result1 = await query.FirstAsync(); //ToListAsync();
                var result = await query.ToListAsync();
               
                  
                    /* DateTime myDate = DateTime.ParseExact(updatedDate, "yyyy-MM-dd HH:mm:ss,fff",
                                            System.Globalization.CultureInfo.InvariantCulture);*/

                if (result.Count == 0)
                {
                    if (isNetOrSms)
                    {
                        Task<string> abc = MakeWebRequest(Constatnt.url + "method=getPlanDetails&operator=" + op_id + "&circle=" + code);
                    }
                    else
                    {
                        MessageBox.Show("Please enable internet settings to check plans.");
                    }
                }
                else
                {
                    var item1 = result[0];
                    DateTime currentDate = DateTime.Now;//.ToString();
                    DateTime updatedDate = item1.planUptateTime;
                    TimeSpan duration = currentDate - updatedDate;
                    if (duration.Days >= 2)
                    {
                        Task<string> abc = MakeWebRequest(Constatnt.url + "method=getPlanDetails&operator=" + op_id + "&circle=" + code);
                    }
                    else
                    {
                        List<PlanUserControl>[] a = new List<PlanUserControl>[25];
                        List<String> ptype = new List<String>();
                        foreach (var item in result)
                        {
                            PlanUserControl row = new PlanUserControl();
                            row.Validity.Text = item.planValidity;

                            row.amount.Text = "Rs. " + item.planAmount;
                            row.PlanDesc.Text = item.planDescription;
                            string typr = item.planType;
                            //L1.FindName(item.planType);
                            int x = 0;

                            if (!ptype.Contains(typr))//ptype does not contain itrType
                            {
                                ptype.Add(typr);
                                x = ptype.IndexOf(typr);
                                a[x] = new List<PlanUserControl>();

                            }
                            else
                            {
                                x = ptype.IndexOf(typr);
                            }
                            a[x].Add(row);

                            i++;

                            //L1.Items.Add(row);
                        }
                        //Pivot myPivot = new Pivot();
                        System.Diagnostics.Debug.WriteLine(a);
                        for (int j = 0; j < a.Count(); j++)
                        {
                            if (a[j] != null)
                            {

                                PivotItem myNewPivotItem = new PivotItem();
                                myNewPivotItem.Name = ptype[j];
                               /* var textBlock = new TextBlock();
                                textBlock.Text = ptype[j];
                                textBlock.FontSize = 27;
                                myNewPivotItem.Header = textBlock;// ptype[j];*/

                                myNewPivotItem.Header = ptype[j];
                                //myNewPivotItem.Header.
                                myNewPivotItem.FontSize = 24;
                                Grid myNewGrid = new Grid();
                                ListBox txtblk = new ListBox();

                                txtblk.ItemsSource = a[j];


                                myNewGrid.Children.Add(txtblk);

                                myNewPivotItem.Content = myNewGrid;

                                myPivot.Items.Add(myNewPivotItem);
                                txtblk.SelectionChanged += new SelectionChangedEventHandler(listBox1_SelectedIndexChanged);

                            }
                        }
                    }
                }
            }
            catch (Exception e2)
            {
                //Debug.WriteLine(e2.StackTrace);
                /* MessageBox.Show("No Plans Availables");*/
                /*NavigationService.Navigate(new Uri("/RechargeMobile.xaml?id="+op_id, UriKind.Relative));
                NavigationService.RemoveBackEntry();*/
            }

            
            if (i == 1)
            {
                MessageBox.Show("No Plans Availables");
                NavigationService.Navigate(new Uri("/RechargeMobile.xaml?", UriKind.Relative));
                NavigationService.RemoveBackEntry();

            }
            else
            {
            }
            Constatnt.SetProgressIndicator(false);
        }

        private void listBox1_SelectedIndexChanged(object sender, SelectionChangedEventArgs e)
        {
            PlanUserControl control = (sender as ListBox).SelectedItem as PlanUserControl;
            string validity = control.Validity.Text;
            string desc = control.PlanDesc.Text;
            string amt = control.amount.Text;
            NavigationService.Navigate(new Uri("/RechargeMobile.xaml?amt=" + amt + "&validity=" + validity + "&desc=" + desc + "&op_id=" + op_id, UriKind.Relative));
            NavigationService.RemoveBackEntry();
        }
    }
}