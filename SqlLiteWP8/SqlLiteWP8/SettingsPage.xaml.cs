﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class SettingsPage : PhoneApplicationPage
    {
        IsolatedStorageSettings settings;
        bool isNetOrSms;// = true;
        public SettingsPage()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            
            settings = IsolatedStorageSettings.ApplicationSettings;
            isNetOrSms = Constatnt.LoadPersistent<Boolean>("isNetOrSms");
            
            if (isNetOrSms)
            {
                CheckInternet.IsChecked = true;
                checkSms.IsChecked = false;
                Boolean i = Constatnt.SavePersistent("isNetOrSms", true);
            }
            else
            {
                CheckInternet.IsChecked = false;
                checkSms.IsChecked = true;
            }
        }

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }

        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }

        private void CheckBox_Checked_1(object sender, RoutedEventArgs e)
        {
            CheckBox currentCheckBoxItem = sender as CheckBox;
            if (currentCheckBoxItem.IsChecked == true)
            {
                checkSms.IsChecked = false;
                CheckInternet.IsChecked = true;
            }
        }

        private void CheckBox_Checked_2(object sender, RoutedEventArgs e)
        {
            CheckBox currentCheckBoxItem1 = sender as CheckBox;
            if (currentCheckBoxItem1.IsChecked == true)
            {
                CheckInternet.IsChecked = false;
                checkSms.IsChecked = true;
            }
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            bool tr = (Boolean)CheckInternet.IsChecked;
            if (tr)
            {
                settings["isNetOrSms"] = true;
            }
            else
            {
                settings["isNetOrSms"] = false;
            }
            //settings["isNetOrSms"] = CheckInternet.IsChecked==true?true:false;
            settings.Save();
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
            NavigationService.RemoveBackEntry();
        }
    }
}