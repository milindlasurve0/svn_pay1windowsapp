﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace SqlLiteWP8
{
    public partial class NewMarchent : PhoneApplicationPage
    {
        public NewMarchent()
        {
            InitializeComponent();
        }

       
        private void btn_Cancel(object sender, RoutedEventArgs e)
        {
             this.NavigationService.Navigate(new Uri("/Login.xaml?", UriKind.Relative));
            NavigationService.RemoveBackEntry();
        }

        private void btn_Proceed(object sender, RoutedEventArgs e)
        {
           // userName  userEmail  userMobile  userCity userState userComment
            if (userName.Text.Length == 0)
            {
                MessageBox.Show("Please Enter Coreect Name");
            }
            else
            {
                if (userEmail.Text.Length == 0)
                {
                    MessageBox.Show("Please Enter Coreect Amount");
                }
                else
                {
                    if (userMobile.Text.Length != 10)
                    {
                        MessageBox.Show("Please Enter Coreect Mobile Number");
                    }
                    else
                    {
                        if (userCity.Text.Length == 0 )
                        {
                            MessageBox.Show("Please Enter Coreect City Name");
                        }
                        else
                        {
                           /* if (userComment.Text.Length == 0)
                            {
                                MessageBox.Show("Please Enter Coreect Amount");
                            }
                            else
                            {*/
                            if (userState.Text.Length == 0)
                            {
                                MessageBox.Show("Please Enter Coreect State Number");
                            }
                            else
                            {
                                Task<string> abc = MakeWebRequestForNewMerchant1();
                            }
                            //}

                        }
                    }
                }

            }
        }


        private bool ValidateEmail(string email)
        {
            //string email = userEmail.Text;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (match.Success)
                return true;
            else
                return false;
        }

        public async Task<string> MakeWebRequestForNewMerchant1()
        {
            //string responseString = "";
             using (HttpClient hc = new HttpClient())
                                    {
                                        var keyValuePairs = new Dictionary<string, string>();
                                        // Fill keyValuePairs

                                        //var content = new FormUrlEncodedContent(keyValuePairs);
                                        var content = new FormUrlEncodedContent(new[]
    {
       new KeyValuePair<string, string>("method", "addLeads"),
                        new KeyValuePair<string, string>("full_name", userName.Text),
                        
                        new KeyValuePair<string, string>("email", userEmail.Text),
                        new KeyValuePair<string, string>("contact_no", userMobile.Text),
                        new KeyValuePair<string, string>("city", userCity.Text),
                        new KeyValuePair<string, string>("state", userState.Text),
                        new KeyValuePair<string, string>("comment", userComment.Text),
                        new KeyValuePair<string, string>("req_by", "windows8"),
    });
                                        var response = await hc.PostAsync(Constatnt.url, content);
                 
                                        response.EnsureSuccessStatusCode();
                                      string  responseString = await response.Content.ReadAsStringAsync();
                  string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status = dict["status"].ToString();
            if (status.Equals("success"))
            {
                MessageBox.Show("Your request sent successfully.");
                this.NavigationService.Navigate(new Uri("/Login.xaml?", UriKind.Relative));
                NavigationService.RemoveBackEntry();
            }
            else if (status.Equals("failure"))
            {
                MessageBox.Show(dict["description"].ToString());

            }
                                    }
             return "acd";
                                    
        }




        public async Task<string> MakeWebRequestForNewMerchant()
        {
            string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
       new KeyValuePair<string, string>("method", "addLeads"),
                        new KeyValuePair<string, string>("full_name", userName.Text),
                        
                        new KeyValuePair<string, string>("email", userEmail.Text),
                        new KeyValuePair<string, string>("contact_no", userMobile.Text),
                        new KeyValuePair<string, string>("city", userCity.Text),
                        new KeyValuePair<string, string>("state", userState.Text),
                        new KeyValuePair<string, string>("comment", userComment.Text),
                        new KeyValuePair<string, string>("req_by", "0"),
    });
                cookieContainer.Add(baseAddress, new Cookie("",""));
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status = dict["status"].ToString();
            if (status.Equals("success"))
            {
                string bal = dict["balance"].ToString();
                bool isOk = Constatnt.SavePersistent(Constatnt.CURRENT_BALANCE, bal);
                MessageBox.Show("Recharge Request Sent Successfully.\n Transaction Id: " + dict["description"].ToString());
                NavigationService.Navigate(new Uri("/MobileMain.xaml", UriKind.Relative));
                NavigationService.RemoveBackEntry();
            }
            else if (status.Equals("failure"))
            {
                MessageBox.Show(dict["description"].ToString());

            }
            return output4;
        }



 
        
    }
}