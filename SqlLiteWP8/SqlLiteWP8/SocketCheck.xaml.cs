﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Windows.Media;
using System.IO.IsolatedStorage;
using System.Windows.Input;
using SQLite;
using SqlLiteWP8.Model;

namespace SqlLiteWP8
{
    public partial class SocketCheck : PhoneApplicationPage
    {
        // Constants
        SQLiteAsyncConnection conn;
        string chatWith="adminsupport@dev.pay1.in";
        const int ECHO_PORT = 5222;  // The Echo protocol uses port 7 in this sample
        const int QOTD_PORT = 5222; // The Quote of the Day (QOTD) protocol uses port 17 in this sample
        SocketClient client;
        string mobile;
        string result;
        int i = 0;
        Thread thread;
        public SocketCheck()
        {
            InitializeComponent();
            CreateDatabase();
            client = new SocketClient();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            
            TextMerchant.Text = Constatnt.SHOPNAME;
            thread = new Thread(new ThreadStart(this.Thread_ContinuousChecker));
            thread.IsBackground = false;
            thread.Name = "Data Polling Thread";
             result = client.Connect("dev.pay1.in", ECHO_PORT);
           // Log(result, false);
             mobile = Constatnt.LoadPersistent<string>("mobile");
             String str =
                 "<?xml version='1.0'?>" +
 "<stream:stream xmlns:stream=\"http://etherx.jabber.org/streams\" to=\"dev.pay1.in\" xmlns=\"jabber:client\">" +
                 "<iq type='set' id='auth'>" +
 "<query xmlns='jabber:iq:register'>" +
 "<username>"+mobile+
 "</username>" +
 "<password>"+mobile+
 "</password>" +
 "<email>" + mobile +
 "</email>" +
 "<name>" + mobile +
 "</name>" +
 "<resource>mobile</resource></query></iq><presence/>";

             result = client.Send(str);

            Login();
        }
        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }

        private async void CreateDatabase()
        {
            conn = new SQLiteAsyncConnection("plans");
            await conn.CreateTableAsync<ChatData>();
        }



        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
            Constatnt.handleSidebar(index, this.NavigationService, "NewMainPage");

        }




        private void Login()
        {
           


            //Log(String.Format("Connecting to server '{0}' over port {1} (echo) ...", "192.168.0.38", ECHO_PORT), true);

            //string mobile = (String)IsolatedStorageSettings.ApplicationSettings["mobile"];
            String str =
                "<?xml version='1.0'?>" +
"<stream:stream xmlns:stream=\"http://etherx.jabber.org/streams\" to=\"dev.pay1.in\" xmlns=\"jabber:client\">" +
                "<iq type='set' id='auth'>" +
"<query xmlns='jabber:iq:auth'>" +
"<username>"+mobile+
            "</username>" +
"<password>"+mobile+
            "</password>" +
"<resource>mobile</resource></query></iq><presence/>" +
        "<message to='adminsupport@dev.pay1.in' type='chat'>" +

"<body>Hi</body>" +
"</message>";




           // Log(String.Format("Sending '{0}' to server ...", txtInput.Text), true);
            result = client.Send(str);
           // Log(result, false);
            if (result == "Success")
            {
                thread.Start();
            }
            else
            {
            }

//            Log("Requesting Receive ...", true);
            result = client.Receive();
            if (result.Equals(""))
            {
            }
            else
            {
                parseLoginXml(result);
            }
           // Log(result, false);
           // ClearLog();
        }

        private void parseLoginXml(string result)
        {
            
        }

        private void UpdateText(string text)
        {
            // Set the textbox text.
            //txtOutput.Text = text;
        }

        public delegate void UpdateTextCallback(string text);



        private void btnGetQuote_Click(object sender, RoutedEventArgs e)
        {

            string msg = txtInput.Text;
            if (msg.Length != 0)
            {
                string str4Msg = "<message to=\"" + chatWith + "\" type=\"chat\">" + "<body>" + msg + "</body>" + "</message>";

                string result = client.Send(str4Msg);
                txtInput.Text = String.Empty;
                Log(msg, true);
            }
            else
            {
                MessageBox.Show("Enter Message");
            }

        }

      

        private bool ValidateInput()
        {
            // txtInput must contain some text
            if (String.IsNullOrWhiteSpace(txtInput.Text))
            {
                MessageBox.Show("Please enter some text to echo");
                return false;
            }

            return true;
        }

        public void Thread_ContinuousChecker()
        {

            while (true)
            {

                String res = client.Receive();
                client.responseNew = System.String.Empty;
                System.Diagnostics.Debug.WriteLine("Recieve       " + res);
                if (res.Contains("message"))
                {

                    UIThread.Invoke(() => Log(parseMessage(res), false));


                }

                try
                {

                    Thread.Sleep(1000);

                }
                catch (Exception eee)
                {

                }


            }
        }

        private async void Log(string message, bool isOutgoing)
        {
            //message = message.Substring(message.Length - 7, 7);

            
            string direction = (isOutgoing) ? ">> " : "<< ";
            string dateTime="";
            ChatData chatData = new ChatData();
            if (message.Contains("support-ex"))
            {
               // chatr.TextChatBody.Text = "Pay1:  How May I help you?";
            }
            else
            {
                if (!isOutgoing && message.Length>8)
                {
                    message = message.Remove(message.Length -6);
                }
                chatData.chatBody = message;
                chatData.chatPosition = isOutgoing;
                 dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                chatData.chatTime = dateTime;
                await conn.InsertAsync(chatData);
            }
           


           
            if (isOutgoing)
            {



                chatRight chatright = new chatRight();
                chatright.TextChatBody.Text = "Me:  "+message;
                chatright.Margin = new Thickness(110, 10, -10, 0);
                //chatright.HorizontalAlignment = HorizontalAlignment.Right;
                
                chatright.TextChatTime.Text = dateTime;
                chatright.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#40BDAB"));
                
                chatList.Items.Insert(chatList.Items.Count, chatright);
            }
            else
            {



                chatRow chatr = new chatRow();
                if (message.Contains("support-ex"))
                {
                    chatr.TextChatBody.Text = "Pay1:  How May I help you?";
                }
                else
                {
                    chatr.TextChatBody.Text = "Pay1:  " + message;
                }
                //string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                chatr.TextChatTime.Text = dateTime;
               chatr.Margin = new Thickness(10, 10, 110, 0);
                chatr.LayoutRoot.Background = new SolidColorBrush(Colors.Blue);
                chatr.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#00FFFF"));
                
                chatList.Items.Insert(chatList.Items.Count, chatr);
                
            }
            
            if (chatList.Items.Count < 2)
            {
            }
            else
            {
                var lastItem = chatList.Items[chatList.Items.Count-1];
                chatList.ScrollIntoView(lastItem);
            }
        }

        private async void LogDB(string message,string time, bool isOutgoing)
        {
            string direction = (isOutgoing) ? ">> " : "<< ";
            string dateTime = "";
            ChatData chatData = new ChatData();
           
            if (isOutgoing)
            {



                chatRight chatright = new chatRight();
                chatright.TextChatBody.Text = "Me:  " + message;
                chatright.Margin = new Thickness(110, 10, -10, 0);
                //chatright.HorizontalAlignment = HorizontalAlignment.Right;

                chatright.TextChatTime.Text = time;
                chatright.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#40BDAB"));

                chatList.Items.Insert(chatList.Items.Count, chatright);
            }
            else
            {



                chatRow chatr = new chatRow();
                if (message.Contains("support-ex"))
                {
                    chatr.TextChatBody.Text = "Pay1:  How May I help you?";
                }
                else
                {
                    chatr.TextChatBody.Text = "Pay1:  " + message;
                }
                //string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                chatr.TextChatTime.Text = time;
                chatr.Margin = new Thickness(10, 10, 110, 0);
                chatr.LayoutRoot.Background = new SolidColorBrush(Colors.Blue);
                chatr.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#00FFFF"));

                chatList.Items.Insert(chatList.Items.Count, chatr);

            }

            if (chatList.Items.Count < 2)
            {
            }
            else
            {
                var lastItem = chatList.Items[chatList.Items.Count - 1];
                chatList.ScrollIntoView(lastItem);
            }
        }






        private void ClearLog()
        {
            //txtOutput.Text = String.Empty;
        }


        public String parseMessage(String message)
        {
            try
            {


                string parentTag = "<doc>";
                string endparentTag = "</doc>";

                XDocument doc;
               // XDocument ddd = XDocument.Load(message);
                if (message.Contains("<stream:stream xmlns" ))
                {
                    doc = XDocument.Parse(message);
                }
                else
                {
                    doc = XDocument.Parse(parentTag + message + endparentTag);
                }
              






                var items = from i in doc.Descendants("message")
                            select new
                            {
                                to = (string)i.Attribute("to"),

                                type = (string)i.Attribute("type"),
                                fromSt = (string)i.Attribute("from")
                            };


                foreach (var item in items)
                {
                    System.Diagnostics.Debug.WriteLine("To              " + item.to);
                    System.Diagnostics.Debug.WriteLine("Type             " + item.type);
                    System.Diagnostics.Debug.WriteLine("From              " + item.fromSt);
                }


                string math = (string)doc.Element("body");
              

                var students = doc.Root.Value;// ("body");
                string[] split = students.ToString().Split('|');
                
                if (split[0].Equals("1"))
                {
                    //string[] split1 = split[1].ToString().Split('/');
                    chatWith = split[1]; //"support-ex3@dev.pay1.in/TelnetClient";// "sandeep@dev.pay1.in/smack";// split1[0];
                    //Login();
                }
                else
                {

                }
                //string val = students.Value("");

               // var bodyTag = doc.Descendants("body").Select(o => o.Value).ToString();
                if (students.ToString().Contains("suppory-ex"))
                {
                    return "";
                }
                else
                {
                    return students.ToString();
                }
            }
            catch(Exception eee)
            {
                System.Diagnostics.Debug.WriteLine(eee.Message);
               // Log("Error",false);

            }
            return "Error";

        }

        private async void Button_Click_Load_Msgs(object sender, RoutedEventArgs e)
        {
            chatList.Items.Clear();
            var query = conn.Table<ChatData>();//.Where(x => x.planCircleID == code && x.planOpertorID == op_id);
            var result = await query.ToListAsync();
            foreach(var item in result)
            {
                LogDB(item.chatBody,item.chatTime, item.chatPosition);
            }
        }

       

      



    }

}

