﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class TopUp : PhoneApplicationPage
    {
        String dateWeek = "";
        string preWeekStr; 
        string nextWeekStr;
        string currWeekStr;
        public TopUp()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            
            Task<string> abc = MakeWebRequest(dateWeek);
        }/*"", ""));
			listValuePair.add(new BasicNameValuePair("date", date));*/

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }

        public async Task<string> MakeWebRequest(string dateWeek)
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";
            ListTopUp.Items.Clear();
            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "topups"),
                        new KeyValuePair<string, string>("date",dateWeek),
                       
                        new KeyValuePair<string, string>("device_type", "")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


          
            int i = 0;



            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            var item1 = desc[0];
            var preWeek = dict["prevWeek"];
            var nextWeek = dict["nextWeek"];
            var currWeek = dict["currWeek"];
            preWeekStr = preWeek[0].ToString();//preWeek.ToString();
            currWeekStr = currWeek[0].ToString();
            nextWeekStr = nextWeek[0].ToString();
            currWeekText.Text = currWeekStr;
            foreach (var item in desc)
            {
                foreach (var innerItem in item)
                {
                    var subInner = innerItem[0];
                    var amount = subInner["amount"];
                    var day = subInner["day"];

                   

                    TopUpRow discRow = new TopUpRow();
                    if (i % 2 == 0)
                    {
                        discRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE")); 
                    }
                    else
                    {
                        discRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                    }
                    decimal d = 0;
                    try
                    {
                        d = Decimal.Parse(amount.ToString());
                    }
                    catch (Exception e)
                    {
                    }
                    discRow.TopUpAmount.Text = "Rs. "+d.ToString("0.00");
                    discRow.TopUpDate.Text = day.ToString();

                    ListTopUp.Items.Add(discRow);
                    i++;
                }

            }
            Constatnt.SetProgressIndicator(false);
            return responseString;

        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void preWeek_Click(object sender, RoutedEventArgs e)
        {
            if (preWeekStr.Equals(""))
            {
                MessageBox.Show("Can not proceed");
            }
            else
            {
                Task<string> abc = MakeWebRequest(preWeekStr);
            }
        }

        private void nextWeek_Click(object sender, RoutedEventArgs e)
        {
            if (nextWeekStr.Equals(""))
            {
                MessageBox.Show("Can not proceed");
            }
            else
            {

                Task<string> abc = MakeWebRequest(nextWeekStr);
            }
        }

       
    }
}