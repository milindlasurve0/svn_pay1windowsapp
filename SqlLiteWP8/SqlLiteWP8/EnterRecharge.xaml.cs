﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http;
using System.Windows.Media;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class EnterRecharge : PhoneApplicationPage
    {
        string fieldName, product_id;
        JArray param;
       
        string txn;
        TextBox textbox;
        string recharge = "";
        public EnterRecharge()
        {
            InitializeComponent();

            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            
            var dict = (JObject)JsonConvert.DeserializeObject(EntertainmentMain.selectedItem);
            // var entParams=dict["params"];
            var prodcut = dict["params"];
            var productId = dict["product_id"];
            var allParams = prodcut["allParams"];
            param = (JArray)allParams["param"];

            product_id = productId.ToString();
            // MessageBox.Show(dict.ToString());

            foreach (var item in param)
            {
                var field = item["field"];

                TextBlock textbox1 = new TextBlock();
                textbox1.Text = field.ToString();
                //textbox1.Height = 35;
                textbox1.Width = 300;
                textbox1.Margin = new Thickness(10, 0, 0, 0);
                textbox1.Foreground = new SolidColorBrush(Colors.Black);
                textbox1.HorizontalAlignment = HorizontalAlignment.Left;
                customPanel.Children.Add(textbox1);

                TextBox textbox = new TextBox();
                textbox.Text = "";
                //textbox.Height = 85;
                textbox.Width = 300;
                textbox.HorizontalAlignment = HorizontalAlignment.Left;
                textbox.Name = field.ToString();
                textbox.BorderBrush = new SolidColorBrush(Constatnt.ConvertStringToColor("#40BDAB"));
                customPanel.Children.Add(textbox);
            }


            Button btn = new Button();
            btn.Content = "Proceed";
            btn.Height = 85;
            btn.Width = 180;
            btn.Foreground = new SolidColorBrush(Colors.Black);
            btn.HorizontalAlignment = HorizontalAlignment.Left;
            btn.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#40BDAB"));
            btn.Click += new RoutedEventHandler(RechargeEnt_Click);
            customPanel.Children.Add(btn);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.ContainsKey("product"))
            {
                string product = NavigationContext.QueryString["product"];
                productText.Text = product;
            }
        }



        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }



        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void RechargeEnt_Click(object sender, RoutedEventArgs e)
        {
            string recStr = "";
            foreach (var item in param)
            {
                var field = item["field"];
                TextBox txtName = (TextBox)this.customPanel.FindName(field.ToString());
                var length = item["length"];
               
                if (txtName != null)
                {
                  /*  int i = int.Parse(length.ToString());
                    if (i == txtName.Text.Length)
                    {
                       
                    }
                    else if (i == null)
                    {

                    }
                    else
                    {
                        MessageBox.Show("Please enter correct values.");
                    }*/
                    txn = txtName.Text;

                    //if()
                    recStr = recStr + "&" + field.ToString() + "="+txn;
                }

            }

          /*  MessageBoxButton buttons = MessageBoxButton.OKCancel;
            // Show message box
            MessageBoxResult result = MessageBox.Show("Operator: " + operatorName.Text + "\nMobile Number: " + textNumber.Text + "\nSub Id: " + textSubsId.Text + "\nAmount:" + textAmount.Text + "\nAre you sure you want to continue?\n", "Mobile Recharge", buttons);
            //  method=authenticate&mobile=9898120212&password=1234&device_id=d80c9122dfcfd25c32438e12ce1a1c9233e84ac5&type=1&device_type=java
            if (result == MessageBoxResult.OK)
            {

                if (isNetOrSms)
                {
                    Task<string> str = MakeWebRequestForStatus();
                }
                else
                {
                    string recharge = "*" + id + "*" + textSubsId.Text + "*" + textNumber.Text + "*" + textAmount.Text;
                    SmsComposeTask smsComposeTask = new SmsComposeTask();
                    smsComposeTask.To = "09223178889";
                    smsComposeTask.Body = recharge;

                    smsComposeTask.Show();

                }






            }*/

            Task<string> res = MakeWebRequest(recStr);
        }

        public async Task<string> MakeWebRequest(string txn)
        {
            string responseString = "";
            Constatnt.SetProgressIndicator(true);
            string reshrgestring="method=vasRecharge&product="+product_id+txn;
            string rechargeString = Constatnt.url + reshrgestring;
            //ListEnter.Items.Clear();
            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
                     {
                         new KeyValuePair<string, string>("method", "getVASProducts"),
                         new KeyValuePair<string, string>("device_type", "android")
                      });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(rechargeString, null);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            var status = dict["status"];
            if (status.ToString().Equals("success"))
            {
                bool isOk = Constatnt.SavePersistent(Constatnt.CURRENT_BALANCE, dict["balance"].ToString());
                MessageBox.Show("Entertainment Pack Subscribed successfully.");
                NavigationService.Navigate(new Uri("/EntertainmentMain.xaml", UriKind.Relative));
                NavigationService.RemoveBackEntry();
            }
            else if (status.ToString().Equals("failure"))
            {
                MessageBox.Show(dict["description"].ToString());
            }
            Constatnt.SetProgressIndicator(false);
            return "hi";
        }



    }
}