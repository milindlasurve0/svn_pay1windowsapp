﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Net.Browser;
using System.IO;
using System.Text;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Controls;
using System.Windows.Navigation;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Maps.Services;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Threading;
using Microsoft.Phone.Notification;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Info;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using SqlLiteWP8.Model;
using SQLite;
using System.Text.RegularExpressions;
//http://www.developer.com/ws/using-the-location-api-in-your-windows-phone-8-applications.html

namespace SqlLiteWP8
{
    public partial class Login : PhoneApplicationPage
    {
        public static Cookie cooki;
        //public static bool isSuccess;
        public static bool isTrue, isSuccess;
        string page;
       public static JObject dictionary;
       string latti,longi;
        IsolatedStorageSettings settings;
        Notification notifyMessages = new Notification();
        // SQLiteAsyncConnection conn;
        DispatcherTimer newTimer;
        SQLiteAsyncConnection conn;
        ProgressRing ring;
        string deviceId;
        public Login()
        {
            InitializeComponent();
            CreateDatabase();
            settings = IsolatedStorageSettings.ApplicationSettings;
            bool isLogin = Constatnt.LoadPersistent<Boolean>("isLogin");

           // getLoc();
            ring = new ProgressRing();
            ring.Visibility = System.Windows.Visibility.Visible;
            this.ContentPanel.Children.Add(ring);
          
           // isTrue = isLogin;
            try
            {
                byte[] id = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");
                deviceId = Convert.ToBase64String(id);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            newTimer = new DispatcherTimer();
          

        }
      /*  async public void getLoc()
        {
            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;

            try
            {
                Geoposition geoposition = await geolocator.GetGeopositionAsync(
                    maximumAge: TimeSpan.FromMinutes(0),
                    timeout: TimeSpan.FromSeconds(1)
                    );
                latti = geoposition.Coordinate.Latitude.ToString("0.00");
                longi = geoposition.Coordinate.Longitude.ToString("0.00");

                MessageBox.Show(geoposition.Coordinate.Latitude.ToString("0.00") + geoposition.Coordinate.Longitude.ToString("0.00"));
                // LongitudeTextBlock.Text = geoposition.Coordinate.Longitude.ToString("0.00");
            }
            catch (Exception ex)
            {
            }
        }*/


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("page"))
            {
                page = NavigationContext.QueryString["page"];

            }
        }


        private async void CreateDatabase()
        {
            conn = new SQLiteAsyncConnection("plans");
            await conn.CreateTableAsync<LocationData>();


        }

        private async void Button_Login(object sender, RoutedEventArgs e)
        {
            Constatnt.SetProgressIndicator(true);
            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;

            try
            {
                Geoposition geoposition = await geolocator.GetGeopositionAsync(
                     maximumAge: TimeSpan.FromMinutes(5),
                     timeout: TimeSpan.FromSeconds(10)
                    );

                latti = geoposition.Coordinate.Latitude.ToString("0.00");
                longi = geoposition.Coordinate.Longitude.ToString("0.00");

                if (user_mobile.Text.Length == 10)
                {
                    if (user_pin.Password.Length < 4 && user_pin.Password.Length > 10)
                    {
                        MessageBox.Show("Enter correct  pin.");
                    }
                    else
                    {
                       
                        InitializeWebRequestClientStackForURI();
                        ReadFromIsolatedStorage();
                    }
                }
                else
                {
                    MessageBox.Show("Enter correct user number.");
                    Constatnt.SetProgressIndicator(false);

                }
            }
            catch (Exception exc)
            {
                Constatnt.SetProgressIndicator(false);
            }

        }


        private void TxtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (user_mobile.Text.Length == 10)
                user_pin.Focus();
        }

        private void TxtPassword_KeyDown(object sender, KeyEventArgs e)
        {


        }


        private void InitializeWebRequestClientStackForURI()
        {
            // Create the client WebRequest creator.
            IWebRequestCreate creator = WebRequestCreator.ClientHttp;

            // Register both http and https.
            //   WebRequest.RegisterPrefix("http://", creator);
            WebRequest.RegisterPrefix("https://", creator);


            string url1 = Constatnt.url + "method=authenticate&mobile=" + user_mobile.Text + "&password=" + user_pin.Password + "&device_id=" + deviceId + "&type=1&device_type=windows8&gcm_reg_id=" + NewMainPage.MPNSString + "&version=" + Environment.OSVersion.Version.ToString() + "&manufacturer=" + DeviceStatus.DeviceManufacturer + "&longitude=" + longi + "&latitude=" + latti;
            // Create a HttpWebRequest.
            HttpWebRequest request = (HttpWebRequest)
                WebRequest.Create(url1);


            //Create the cookie container and add a cookie.
            request.CookieContainer = new CookieContainer();



            request.CookieContainer.Add(new Uri(url1),
              new Cookie("id", "1234"));
            // Send the request.
            IAsyncResult res = request.BeginGetResponse(new AsyncCallback(ReadCallback), request);

        }



       async private void ReadCallback(IAsyncResult asynchronousResult)
        {
           
           try{

                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

                HttpWebResponse response = (HttpWebResponse)
                    request.EndGetResponse(asynchronousResult);

                // Gets the stream associated with the response.
                Stream receiveStream = response.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                // Pipes the stream to a higher level stream reader with the required encoding format. 
                StreamReader readStream = new StreamReader(receiveStream, encode);
                StreamReader reader = new StreamReader(receiveStream);
                string text = reader.ReadToEnd();
                string output = text.Remove(text.Length - 1, 1);
                string output1 = output.Remove(output.Length - 1, 1);
                string output2 = output1.Remove(0, 1);
                string output3 = output2.Remove(0, 1);
                string output4 = output3.Remove(output3.Length - 1, 1);

                var dict = (JObject)JsonConvert.DeserializeObject(output4);
                var status = dict["status"];


                Console.WriteLine("\r\nResponse stream received.");
                Char[] read = new Char[10000];
                // Reads 256 characters at a time.     
                int count = readStream.Read(read, 0, 256);
                Console.WriteLine("HTML...\r\n");
                String str = "";
                while (count > 0)
                {
                    // Dumps the 256 characters on a string and displays the string to the console.
                    str = new String(read, 0, count);
                    Console.Write(str);
                    count = readStream.Read(read, 0, 256);
                }
                Console.WriteLine("");
                // Releases the resources of the response.
                response.Close();
                // Releases the resources of the Stream.
                readStream.Close();

                using (IsolatedStorageFile isf =
                    IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isfs = isf.OpenFile("CookieExCookies",
                        FileMode.OpenOrCreate, FileAccess.Write))
                    {


                        using (StreamWriter sw = new StreamWriter(isfs))
                        {


                            foreach (Cookie cookieValue in response.Cookies)
                            {
                                sw.WriteLine("Cookie: " + cookieValue.ToString());

                                // cook = cookieValue.Value;

                                //callMyMethod(isTrue);
                                if (status.ToString() == "success")
                                {
                                   /* settings["isLogin"] = true;
                                    settings["mobile"] = user_mobile.Text;
                                    */
                                   

                                    var description = dict["description"];
                                    var balance = description["balance"];
                                    Constatnt.mCurrentBalance = balance.ToString();
                                    var shopname = description["shopname"];
                                    Constatnt.SHOPNAME = shopname.ToString();
                                    var passFlag = dict["passFlag"];
                                    cooki = new Cookie(cookieValue.Name, cookieValue.Value);
                                    var add = description["address"].ToString();
                                   dictionary = dict;
                                    if (passFlag.ToString() == "1")
                                    {

                                        Dispatcher.BeginInvoke(async delegate()
                                        {
                                              isTrue = true;



                                             
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection("plans");
            LocationData location = new LocationData();

            location.address = description["address"].ToString();// Constatnt.LoadPersistent<String>("address");
            location.area = description["area_name"].ToString();// Constatnt.LoadPersistent<String>("area_name");
            location.state = description["state_name"].ToString();// Constatnt.LoadPersistent<String>("state_name");
            location.city = description["city_name"].ToString();// Constatnt.LoadPersistent<String>("city_name");
            location.lattitude = description["latitude"].ToString();// Constatnt.LoadPersistent<String>("latitude");
            location.longitude = description["longitude"].ToString();// Constatnt.LoadPersistent<String>("longitude");
            location.zipcode = description["pin"].ToString();// Constatnt.LoadPersistent<String>("pin");

            List<String> lst = new List<string>();
            var query = conn.Table<LocationData>();
            //string val = "value";
            List<LocationData> result = await query.ToListAsync();
            if (result.Count() >= 1)
            {
                
                    // int i= await conn.UpdateAsync(locatonData);
                    string updateQuery="UPDATE LocationData Set address = '"+location.address+"',city = '"+location.city+"',area = '"+location.area+"',state = '"+location.state+"',zipcode = '"+location.zipcode+"',lattitude = '"+location.lattitude+"',longitude = '"+location.longitude+"' WHERE locId = 1";//, locatonData.address, locatonData.city, locatonData.area, locatonData.state, locatonData.zipcode, locatonData.lattitude, locatonData.longitude);// UpdateAsync(location);
                    int i = await conn.ExecuteAsync(updateQuery);

                    List<LocationData> result1 = await query.ToListAsync();
              
            }
            else
            {
                await conn.InsertAsync(location);
            }


                                            settings["cookie"] = cookieValue;
                                            settings["cookieName"] = cookieValue.Name;
                                            settings["cookieValue"] = cookieValue.Value;
                                            settings["merchant"] = shopname.ToString();
                                           // settings["key"] = cooki;
                                            settings[Constatnt.IS_LOGIN] = true;
                                            settings["mobile"] = user_mobile.Text;
                                            settings[Constatnt.CURRENT_BALANCE] = balance.ToString();
                                            settings.Save();
                                            /*  IsolatedStorageSettings.ApplicationSettings.Add("mobile", user_mobile.Text); IsolatedStorageSettings.ApplicationSettings.Save();*/
                                            Constatnt.SetProgressIndicator(false);
                                            NavigationService.Navigate(new Uri("/" + page + ".xaml", UriKind.RelativeOrAbsolute));
                                            this.NavigationService.RemoveBackEntry();
                                         
                                            
                                        });


                                    }
                                    else
                                    {
                                        Dispatcher.BeginInvoke(delegate()
                                        {
                                            Constatnt.SetProgressIndicator(false);
                                            NavigationService.Navigate(new Uri("/resetPassword.xaml", UriKind.RelativeOrAbsolute));
                                            this.NavigationService.RemoveBackEntry();

                                        });
                                    }
                                }
                                else
                                {
                                   
                                    Dispatcher.BeginInvoke(delegate() {
                                        Constatnt.SetProgressIndicator(false);
                                        MessageBox.Show("Login Failed Please try again."); });
                                }

                            }

                            sw.Close();
                           
                            // string ai = sw.ToString();
                        }

                    }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
          

        }

      



        private void ReadFromIsolatedStorage()
        {
            try
            {
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isfs =
                       isf.OpenFile("CookieExCookies", FileMode.OpenOrCreate))
                    {
                        using (StreamReader sr = new StreamReader(isfs))
                        {

                            sr.Close();
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task<string> GetCurrentLocation()
        {
            Geolocator locationFinder = new Geolocator
            {
                DesiredAccuracyInMeters = 50,
                DesiredAccuracy = PositionAccuracy.Default
            };
            String longitude = "";
            String latitude = "";
            try
            {
                Geoposition currentLocation = await locationFinder.GetGeopositionAsync(
                    maximumAge: TimeSpan.FromSeconds(120),
                    timeout: TimeSpan.FromSeconds(10));
                longitude = currentLocation.Coordinate.Longitude.ToString("0.00");
                latitude = currentLocation.Coordinate.Latitude.ToString("0.00");
                MessageBox.Show("Long: " + longitude + "Lat: " + latitude);

            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("And Exception Occured");
            }
            return "Long: " + longitude + "Lat: " + latitude;
        }

     

        /// <summary>
        /// Navigate to page 2 button clicked event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNavigate_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Page2.xaml?NavigatedFrom=Main Page", UriKind.Relative));
        }

        private void user_pin_TextInput_1(object sender, TextCompositionEventArgs e)
        {

        }


        string _enteredPasscode = "";
        string _passwordChar = "*";

        private void PasswordTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //modify new passcode according to entered key
            _enteredPasscode = GetNewPasscode(_enteredPasscode, e);
            ActualPasscode.Text = _enteredPasscode;
            //replace text by *
            user_pin.Password = Regex.Replace(_enteredPasscode, @".", _passwordChar);

            //take cursor to end of string
            //user_pin.SelectionStart = user_pin.Password.Length;
        }


        private string GetNewPasscode(string oldPasscode, KeyEventArgs keyEventArgs)
        {
            string newPasscode = string.Empty;
            switch (keyEventArgs.Key)
            {
                case Key.D0:
                case Key.D1:
                case Key.D2:
                case Key.D3:
                case Key.D4:
                case Key.D5:
                case Key.D6:
                case Key.D7:
                case Key.D8:
                case Key.D9:
                    newPasscode = oldPasscode + (keyEventArgs.PlatformKeyCode - 48);
                    break;
                case Key.Back:
                    if (oldPasscode.Length > 0)
                        newPasscode = oldPasscode.Substring(0, oldPasscode.Length - 1);
                    break;
                default:
                    //others
                    newPasscode = oldPasscode;
                    break;
            }
            return newPasscode;
        }

        private void Button_NewMarchent(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/NewMarchent.xaml?", UriKind.Relative));
        }






    }


}