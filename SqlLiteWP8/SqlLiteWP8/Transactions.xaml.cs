﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Windows.Devices.Input;

namespace SqlLiteWP8
{
    public partial class Transactions : PhoneApplicationPage
    {
        String serviceType = "1";
        public Transactions()
        {
            InitializeComponent();
            
            Task<string> abc = MakeWebRequest(serviceType);
        }/*"", ""));
			listValuePair.add(new BasicNameValuePair("date", date));*/

        private void Home_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Home_Click");
        }
        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }

        public async Task<string> MakeWebRequest(string transServiceType)
        {
            string responseString = "";
            ListTrans.Items.Clear();
            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "lastTransactions"),
                        new KeyValuePair<string, string>("date","2014-03-29"),
                new KeyValuePair<string, string>("date2","2014-03-29"),
                        new KeyValuePair<string, string>("service", transServiceType),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


        
            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            int i = 0;
            foreach (var item in desc)
            {
                foreach (var innerItem in item)
                {
                    var timestamp = innerItem["0"];
                    var time = timestamp["timestamp"];

                    var products = innerItem["products"];
                    var name = products["name"];
                    var vendors_activations = innerItem["vendors_activations"];
                    var product_id = vendors_activations["product_id"];
                    var mobile = vendors_activations["mobile"];
                    var amount = vendors_activations["amount"];
                    var status = vendors_activations["status"];
                    i++;


                    StatusRow statusRow = new StatusRow();
                    statusRow.TextMobile.Text = mobile.ToString();
                    statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                    statusRow.TextDate.Text = time.ToString();
                    if (i % 2 == 0)
                    {
                        statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                    }
                    else
                    {
                         statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                    }
                    if (status.ToString() == "1" || status.ToString() == "0")
                    {
                        statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                    }
                    else
                    {
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                    }
                    statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                    ListTrans.Items.Add(statusRow);



                }

            }

            return responseString;

        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void btnTransMob_Click(object sender, RoutedEventArgs e)
        {
            Task<string> abc = MakeWebRequest("1");
        }

        private void btnTransDth_Click(object sender, RoutedEventArgs e)
        {
            Task<string> abc = MakeWebRequest("2");
        }

        private void btnTransEnter_Click(object sender, RoutedEventArgs e)
        {
            Task<string> abc = MakeWebRequest("3");
        }
    }
}