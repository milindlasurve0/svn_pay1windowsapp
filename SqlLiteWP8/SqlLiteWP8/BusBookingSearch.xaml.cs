﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Input;
using System.IO.IsolatedStorage;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace SqlLiteWP8
{
    public partial class BusBookingSearch : PhoneApplicationPage
    {
        string srdIdNew, desIdNew;
        string apiUrl = "http://192.168.0.6/index.php/apis/receiveWeb/mindsarray/mindsarray/json?method=busBooking&action=availabletrips&source=4502&destination=626&doj=2014-02-28";
        public static string srcId = "";
        public static string destId = "";
        public BusBookingSearch()
        {
            InitializeComponent();
            // this.datePicker.ValueChanged += new EventHandler<DateTimeValueChangedEventArgs>(picker_ValueChanged);
            SrcTextBlock.MouseLeftButtonDown += new MouseButtonEventHandler(SrcTextBlock_MouseLeftButtonDown);
            DestText.MouseLeftButtonDown += new MouseButtonEventHandler(DestTextBlock_MouseLeftButtonDown);
            DateTextBlock.MouseLeftButtonDown += new MouseButtonEventHandler(DateTextBlock_MouseLeftButtonDown);
        }
        void picker_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime date = (DateTime)e.NewDateTime;


        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);



            if (NavigationContext.QueryString.TryGetValue("srcId", out srcId))
            {
                Console.WriteLine("working      " + srcId);
                var sid = IsolatedStorageSettings.ApplicationSettings["srcId"];
                var srcName = IsolatedStorageSettings.ApplicationSettings["srcIdName"];
                srcId = sid.ToString();
                srdIdNew = srcName.ToString();
                SrcTextBlock.Text = srdIdNew;
            }

            if (NavigationContext.QueryString.TryGetValue("destId", out destId))
            {
                Console.WriteLine("working      " + destId);
                try
                {

                    var sid = IsolatedStorageSettings.ApplicationSettings["srcId"];
                    var srcName = IsolatedStorageSettings.ApplicationSettings["srcIdName"];
                    var destName = IsolatedStorageSettings.ApplicationSettings["destIdName"];
                    var did = IsolatedStorageSettings.ApplicationSettings["destId"];
                    srcId = sid.ToString();

                    srdIdNew = srcName.ToString();
                    SrcTextBlock.Text = srdIdNew;
                    destId = did.ToString();
                    desIdNew = destName.ToString();
                    DestText.Text = desIdNew;
                }catch(Exception eee){
                    Console.WriteLine(eee.StackTrace);
                }
               

            }
        }


        private void DateTextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            NavigationService.Navigate(new Uri("/CitySearch.xaml?pageName=srcId", UriKind.Relative));
        }
        private void SrcTextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            NavigationService.Navigate(new Uri("/CitySearch.xaml?pageName=srcId", UriKind.Relative));
        }

        private void DestTextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            NavigationService.Navigate(new Uri("/CitySearch.xaml?pageName=destId", UriKind.Relative));
        }

        private void Home_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Home_Click");
        }


        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void Button_Search_Bus(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BusBookingSearchResult.xaml?", UriKind.Relative));
        }



/*
        public async Task<string> MakeWebRequest(string apiUrl, List<KeyValuePair<string, string>> values)
        {

            // var httpClient = new HttpClient(new HttpClientHandler());

            HttpResponseMessage response = await Login.httpClient.PostAsync(apiUrl, new FormUrlEncodedContent(values));
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();

            var dict = (JObject)JsonConvert.DeserializeObject(responseString);
            var status = dict["status"];
            if (status.ToString() == "success")
            {
                
            }
            else
            {
            }
           
            return responseString;
        }*/
    }
}