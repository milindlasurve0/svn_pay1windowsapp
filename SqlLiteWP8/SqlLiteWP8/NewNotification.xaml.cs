﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Input;
using SqlLiteWP8.Model;
using SQLite;
using System.IO.IsolatedStorage;

namespace SqlLiteWP8
{
    public partial class NewNotification : PhoneApplicationPage
    {
        bool isNetOrSms;
        NotificationListDB notificationList = new NotificationListDB();
        SQLiteAsyncConnection conn;
        public NewNotification()
        {
            InitializeComponent();


            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);

            try
            {
                isNetOrSms = Constatnt.LoadPersistent<Boolean>("isNetOrSms");
            }
            catch (Exception e4)
            {
                Console.WriteLine(e4.Message);
            }
            // Constatnt.loadSideBar(L1,balanceText);


            CreateDatabase();

        }

        private async void CreateDatabase()
        {
            conn = new SQLiteAsyncConnection("plans");
            await conn.CreateTableAsync<NotificationListDB>();



            var query1 = conn.Table<NotificationListDB>().OrderByDescending(x => x.notificationTime);//.ToListAsync();//.Where(x =>x.notificationID == 1);
            // var col = conn.QueryAsync<Plans>("SELECT * FROM Plans WHERE planOpertorID='2' && planCircleID='BR'");


            // string updateQuery = "SELECT * FROM NotificationListDB ORDER BY notificationTime";//Set address = '" + location.address + "',city = '" + location.city + "',area = '" + location.area + "',state = '" + location.state + "',zipcode = '" + location.zipcode + "',lattitude = '" + location.lattitude + "',longitude = '" + location.longitude + "' WHERE locId = 1";//, locatonData.address, locatonData.city, locatonData.area, locatonData.state, locatonData.zipcode, locatonData.lattitude, locatonData.longitude);// UpdateAsync(location);
            //var k = await conn.ExecuteAsync(updateQuery);
            Console.WriteLine("xfggf");

            var result1 = await query1.ToListAsync();
           
            if (isNetOrSms)
            {
                TextMerchant.Text = Constatnt.SHOPNAME;
                Task<string> sas = MakeWebRequest("");

            }
            else
            {
                var query = conn.Table<NotificationListDB>();//.Where(x =>x.n == op_id);
                // var col = conn.QueryAsync<Plans>("SELECT * FROM Plans WHERE planOpertorID='2' && planCircleID='BR'");
                Console.WriteLine("xfggf");

                var result = await query.ToListAsync();
                List<PlanUserControl>[] a = new List<PlanUserControl>[25];
                List<String> ptype = new List<String>();
                foreach (var item in result)
                {
                    NotificationItem row = new NotificationItem();
                    row.notif.Text = item.notificationData;
                    listBoxNotification.Items.Add(row);
                }
                IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                settings[Constatnt.NOTIFICATION_COUNT] = 0;
                settings.Save();
            }
           

        }

        public async Task<string> MakeWebRequest(string date)
        {

            //listBox1.Items.Clear();
            string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "pullNotifications"),
                      
                        new KeyValuePair<string, string>("device_type", "windows8")
    });

                Cookie cookie = Constatnt.LoadPersistent<Cookie>("cookie");
                cookie.Name = Constatnt.LoadPersistent<String>("cookieName");

                cookie.Value = Constatnt.LoadPersistent<String>("cookieValue");
                //Login.cooki = new Cookie(cookie.Name, cookie.Value);

                //Login.cooki = new Cookie(cookie.Name, cookie.Value);


                cookieContainer.Add(baseAddress, new Cookie(cookie.Name, cookie.Value));
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            //string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

            var dict = (JObject)JsonConvert.DeserializeObject(output4);

            var desc = dict["notifications"];
            int i = 0;
             
            foreach (var item in desc)
             {
                 string id = item["id"].ToString();
                 string msg = item["msg"].ToString();
                 string created = item["created"].ToString();
                 notificationList.notificationMobileID = id;
                 notificationList.notificationTime = created;
                 notificationList.notificationData = msg;
                 try
                 {
                     await conn.InsertAsync(notificationList);
                 }catch(SQLiteException exc){
                     continue;
                 }
             }

          /*  notificationList.notificationMobileID = "9769597418";
            notificationList.notificationTime = "created";
            notificationList.notificationData = "msg";
            await conn.InsertAsync(notificationList);*/



            
            var query = conn.Table<NotificationListDB>().OrderByDescending(x =>x.notificationTime);//.ToListAsync();//.Where(x =>x.notificationID == 1);
               // var col = conn.QueryAsync<Plans>("SELECT * FROM Plans WHERE planOpertorID='2' && planCircleID='BR'");


           // string updateQuery = "SELECT * FROM NotificationListDB ORDER BY notificationTime";//Set address = '" + location.address + "',city = '" + location.city + "',area = '" + location.area + "',state = '" + location.state + "',zipcode = '" + location.zipcode + "',lattitude = '" + location.lattitude + "',longitude = '" + location.longitude + "' WHERE locId = 1";//, locatonData.address, locatonData.city, locatonData.area, locatonData.state, locatonData.zipcode, locatonData.lattitude, locatonData.longitude);// UpdateAsync(location);
            //var k = await conn.ExecuteAsync(updateQuery);
                Console.WriteLine("xfggf");
               
                var result = await query.ToListAsync();
                List<PlanUserControl>[] a = new List<PlanUserControl>[25];
                List<String> ptype = new List<String>();
                foreach (var item in result)
                {
                    NotificationItem row = new NotificationItem();
                    row.notif.Text = item.notificationData;
                    row.time.Text = item.notificationTime;
                    listBoxNotification.Items.Add(row);
                }
                IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                settings[Constatnt.NOTIFICATION_COUNT] = 0;
                settings.Save();
            return responseString;

        }

        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
            Constatnt.handleSidebar(index, this.NavigationService, "NewMainPage");

        }

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }

    }
}