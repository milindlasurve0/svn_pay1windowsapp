﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SqlLiteWP8
{
    public partial class DiscountStructure : PhoneApplicationPage
    {
        String serviceType = "1";
        public DiscountStructure()
        {
            InitializeComponent();
            Task<string> abc = MakeWebRequest(serviceType);
        }/*"", ""));
			listValuePair.add(new BasicNameValuePair("date", date));*/

        public async Task<string> MakeWebRequest(string transServiceType)
        {
            string responseString = "";
            ListDisc.Items.Clear();
            var baseAddress = new Uri("http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?");

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "getCommissions"),
                        new KeyValuePair<string, string>("date","2014-02-25"),
                        new KeyValuePair<string, string>("service", transServiceType),
                        new KeyValuePair<string, string>("device_type", "")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

        

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            var item1=desc[0];
            var R=item1["R"];
            int i = 0;
            foreach (var item in R)
            {
               foreach (var innerItem in item)
                {
                    var prodName = innerItem["prodName"];
                    var prodPercent = innerItem["prodPercent"];
                  
                    i++;
                   

                    string color = "";

                    if (i % 2 == 0)
                    {
                        var color1 = new Color() { R = 0xE2, G = 0xF1, B = 0xDE };
                        var brush = new SolidColorBrush(color1);
                        color = brush.ToString();
                        color = "Blue";

                    }
                    else
                    {
                        color = "Red";
                        //lbi1.Foreground = new SolidColorBrush(Color.FromArgb(255, 45, 23, 45));
                    }


                    DiscountRow discRow = new DiscountRow();
                    if (i % 2 == 0)
                    {
                        discRow.LayoutRoot.Background = new SolidColorBrush(Colors.Green);
                    }
                    else
                    {
                        discRow.LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(1, 0, 200, 0));
                    }
                    discRow.Product.Text = prodName.ToString();
                    discRow.DiscountRate.Text = prodPercent.ToString()+"%";

                    ListDisc.Items.Add(discRow);
                    i++;
                }

            }

            return responseString;

        }
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Home_Click");
        }
        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }
       
        private void btnTransMob_Click(object sender, RoutedEventArgs e)
        {
            Task<string> abc = MakeWebRequest("1");
        }

        private void btnTransDth_Click(object sender, RoutedEventArgs e)
        {
            Task<string> abc = MakeWebRequest("2");
        }

        private void btnTransEnter_Click(object sender, RoutedEventArgs e)
        {
            Task<string> abc = MakeWebRequest("3");
        }
    }
}