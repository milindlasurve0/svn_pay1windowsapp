﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace SqlLiteWP8
{
    public partial class ReversalMain : PhoneApplicationPage
    {
        RevStatusList revStatusList = new RevStatusList();
        RevStatusList revRequestList = new RevStatusList();
        public ReversalMain()
        {
            InitializeComponent();
            
            Task<string> abc = MakeWebRequestForStatus("1");
            Task<string> abc1 = MakeWebRequest("1");
           
        }
        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Home_Click(object sender, RoutedEventArgs e)
        {

            MessageBox.Show("Home_Click");
        }


        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }


        public async Task<string> MakeWebRequestForStatus(string service)
        {

            string responseString = "";

            var baseAddress = new Uri("http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?");

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "mobileTransactions"),
                        new KeyValuePair<string, string>("mobile",mobileNumber.Text),
                        new KeyValuePair<string, string>("service", service),
                        new KeyValuePair<string, string>("device_type", "")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


            RevStatusList mov = new RevStatusList();




            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            int i = 0;
            foreach (var item in desc)
            {
                foreach (var innerItem in item)
                {
                    var timestamp = innerItem["0"];
                    var time = timestamp["timestamp"];

                    var products = innerItem["products"];
                    var name = products["name"];
                    var vendors_activations = innerItem["vendors_activations"];
                    var product_id = vendors_activations["product_id"];
                    var mobile = vendors_activations["mobile"];
                    var amount = vendors_activations["amount"];
                    var status = vendors_activations["status"];
                    i++;
                    Uri ImageUrl = null;
                    
                    if (product_id.ToString() == Constatnt.ID_OPERATOR_AIRTEL)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);


                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_TATA_SKY)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);

                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_SUN)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);

                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_DISHTV)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);
                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_AIRTEL)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);
                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_BIG)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);
                    }

                    string color = "";

                    if (i % 2 == 0)
                    {
                        var color1 = new Color() { R = 0xE2, G = 0xF1, B = 0xDE };
                        var brush = new SolidColorBrush(color1);
                        color = brush.ToString();
                        color = "Blue";

                    }
                    else
                    {
                        color = "Red";
                        //lbi1.Foreground = new SolidColorBrush(Color.FromArgb(255, 45, 23, 45));
                    }
                    mov.Add(new RevStatus { RevMobile = mobile.ToString(), RevRechargeStatus = status.ToString(), RevAmount = amount.ToString(), RevTime = time.ToString(), color = color, RevImageUrl = ImageUrl });


                    //mov.Add(new StatusMobile { Mobile = mobile.ToString(), RechargeStatus = status.ToString(), Amount = amount.ToString(), Time = time.ToString(), color = color, ImageUrl = ImageUrl });
                }

            }


           //  listBox1.ItemsSource = mov;
            listBox2.ItemsSource = mov;

            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output2);



            return responseString;

        }










        public async Task<string> MakeWebRequest(string service)
        {

            string responseString = "";

            var baseAddress = new Uri("http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?");

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "reversalTransactions"),
                        new KeyValuePair<string, string>("date","2014-02-10"),
                        new KeyValuePair<string, string>("service", service),
                        new KeyValuePair<string, string>("device_type", "")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


            RevStatusList mov = new RevStatusList();




            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            int i = 0;
            foreach (var item in desc)
            {
                foreach (var innerItem in item)
                {
                    var timestamp = innerItem["0"];
                    var time = timestamp["timestamp"];

                    var products = innerItem["products"];
                    var name = products["name"];
                    var vendors_activations = innerItem["vendors_activations"];
                    var product_id = vendors_activations["product_id"];
                    var mobile = vendors_activations["mobile"];
                    var amount = vendors_activations["amount"];
                    var status = vendors_activations["status"];
                    i++;
                    Uri ImageUrl = null;

                    if (product_id.ToString() == Constatnt.ID_OPERATOR_AIRTEL)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);


                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_TATA_SKY)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);

                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_SUN)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);

                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_DISHTV)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);
                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_AIRTEL)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);
                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_BIG)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);
                    }

                    string color = "";

                    if (i % 2 == 0)
                    {
                        var color1 = new Color() { R = 0xE2, G = 0xF1, B = 0xDE };
                        var brush = new SolidColorBrush(color1);
                        color = brush.ToString();
                        color = "Blue";

                    }
                    else
                    {
                        color = "Red";
                        //lbi1.Foreground = new SolidColorBrush(Color.FromArgb(255, 45, 23, 45));

                       
                    }
                    mov.Add(new RevStatus{ RevMobile = mobile.ToString(), RevRechargeStatus = status.ToString(), RevAmount = amount.ToString(), RevTime = time.ToString(), color = color, RevImageUrl = ImageUrl });


                    //mov.Add(new StatusMobile { Mobile = mobile.ToString(), RechargeStatus = status.ToString(), Amount = amount.ToString(), Time = time.ToString(), color = color, ImageUrl = ImageUrl });
                }

            }


            listBox1.ItemsSource = mov;
            //listBox2.ItemsSource = mov;

            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output2);



            return responseString;

        }



        private void Enter_Status_Click(object sender, RoutedEventArgs e)
        {
            revStatusList.Clear();
            listBox1.ItemsSource = null;
          
            Task<string> abc = MakeWebRequest("3");
        }

        private void Dth_Status_Click(object sender, RoutedEventArgs e)
        {
            revStatusList.Clear();
            try
            {
                this.listBox1.ItemsSource = null;
               
               Task<string> abc = MakeWebRequest("2");
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.StackTrace);
            }
        }

        private void Mobile_Status_Click(object sender, RoutedEventArgs e)
        {
            revStatusList.Clear();
            listBox1.ItemsSource = null;
           
            Task<string> abc = MakeWebRequest("1");
        }

        private void Mobile_Request_Click(object sender, RoutedEventArgs e)
        {
            revRequestList.Clear();
           
            listBox2.ItemsSource = null;
            int len=mobileNumber.Text.Length;
            if (len == 10)
            {
                
               Task<string> abc1 = MakeWebRequestForStatus("1");
            }
            else
            {
                MessageBox.Show("Please Enter Correct Mobile Number");
            }
        }

        private void Dth_Request_Click(object sender, RoutedEventArgs e)
        {
            revRequestList.Clear();
            
            listBox2.ItemsSource = null;
            int len = mobileNumber.Text.Length;
            if (len == 10)
            {
                
                Task<string> abc1 = MakeWebRequestForStatus("2");
            }
            else
            {
                MessageBox.Show("Please Enter Correct Mobile Number");
            }
        }

        private void Enter_Request_Click(object sender, RoutedEventArgs e)
        {
            revRequestList.Clear();
            
            listBox2.ItemsSource = null;
            int len = mobileNumber.Text.Length;
            if (len == 10)
            {
              Task<string> abc1 = MakeWebRequestForStatus("3");
            }
            else
            {
                MessageBox.Show("Please Enter Correct Mobile Number");
            }
        }

       
    }

    public class RevStatusList : List<RevStatus>
    {
        public RevStatusList()
        {


        }
    }

    public class RevStatus
    {
        public string RevTime { get; set; }
        public string RevAmount { get; set; }
        public string RevRechargeStatus { get; set; }
        public string RevMobile { get; set; }
        public string color { get; set; }
        public Uri RevImageUrl { get; set; }
    }

}