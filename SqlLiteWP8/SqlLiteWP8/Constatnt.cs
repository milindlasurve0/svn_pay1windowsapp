﻿using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using SQLite;
using SqlLiteWP8.Model;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Threading;
using Windows.Devices.Geolocation;

namespace SqlLiteWP8
{
    public static class UIThread
    {
        private static readonly Dispatcher Dispatcher;
       
        static UIThread()
        {
            // Store a reference to the current Dispatcher once per application
            Dispatcher = Deployment.Current.Dispatcher;
        }


       





        /// <summary>
        ///   Invokes the given action on the UI thread - if the current thread is the UI thread this will just invoke the action directly on
        ///   the current thread so it can be safely called without the calling method being aware of which thread it is on.
        /// </summary>
        public static void Invoke(Action action)
        {
            if (Dispatcher.CheckAccess())
                action.Invoke();
            else
                Dispatcher.BeginInvoke(action);
        }
    }



    class Constatnt
    {

        //public static string url = "http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?";
        //public static string url = "http://dev.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?";
        //public static string url = "http://192.168.0.11:9091/apis/receiveWeb/mindsarray/mindsarray/json?";
        public static string url = "https://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?";
        public static ListBox l2;
        public static string SHOPNAME = "Merchant";

        public static string latti;
        public static string longi;

        public static string srcId="";
        public static string destid="";
        public static int LOGIN = 100;
        public static String DATE = "date";
        public static String ENTERTAINMENT = "entertainment";
        public static String NETWORK = "Network";
        public static String SMS = "sms";
        public static String PLAN_JSON = "plan_json";
        public static int PLAN_REQUEST = 200;
        public static String IS_RESET = "isReset";
        public static String OPERATOR_ID = "operator_id";
        public static String OPERATOR_LOGO = "operator_logo";
        public static String OPERATOR_NAME = "operator_name";
        public static String IS_STATUS_TAB = "status_tab";
        public static String REQUEST_FOR = "requestFor";
        public static String REQUEST_FOR_TAB = "requestFor";
        public static String NOTIFICATION_COUNT = "count";
        public static String IS_LOGIN = "isLogin";
        public static String CURRENT_BALANCE = "balance";

        public static String PRODUCT_NAME = "productName";
        public static String PRODUCT_ID = "productId";
        public static String PRODUCT_DETAILS = "productDetails";

        public static int MOBILE_TAB = 1;
        public static int DTH_TAB = 2;
        public static int ENTERTAINMENT_TAB = 3;
        public static int STATUS_TAB = 1;
        public static int REQUEST_TAB = 2;
        public static int RECHARGE_TAB = 0;
        public static String IS_REQUEST_TAB = "request_tab";




        public static int OPERATOR_AIRCEL = 1;
        public static int OPERATOR_AIRTEL = 2;
        public static int OPERATOR_BSNL = 3;
        public static int OPERATOR_IDEA = 4;
        public static int OPERATOR_LOOP = 5;
        public static int OPERATOR_MTS = 6;
        public static int OPERATOR_RELIANCE_CDMA = 7;
        public static int OPERATOR_RELIANCE_GSM = 8;
        public static int OPERATOR_DOCOMO = 9;
        public static int OPERATOR_TATA_INDICOM = 10;
        public static int OPERATOR_UNINOR = 11;
        public static int OPERATOR_VIDEOCON = 12;
        public static int OPERATOR_VODAFONE = 15;
        public static int OPERATOR_MTNL = 30;

        public static int OPERATOR_DTH_DISHTV = 18;
        public static int OPERATOR_DTH_TATA_SKY = 20;
        public static int OPERATOR_DTH_SUN = 19;
        public static int OPERATOR_DTH_BIG = 17;
        public static int OPERATOR_DTH_AIRTEL = 16;
        public static int OPERATOR_DTH_VIDEOCON = 21;

   /*     public static int OPERATOR_BILL_DOCOMO = 36;
        public static int OPERATOR_BILL_LOOP = 37;
        public static int OPERATOR_BILL_CELLONE = 38;
        public static int OPERATOR_BILL_IDEA = 39;
        public static int OPERATOR_BILL_TATATELESERVICE = 40;
        public static int OPERATOR_BILL_VODAFONE = 41;
        */
       
        

        public static String ID_OPERATOR_AIRCEL = "1";
        public static String ID_OPERATOR_AIRTEL = "2";
        public static String ID_OPERATOR_BSNL = "3";
        public static String ID_OPERATOR_IDEA = "4";
        public static String ID_OPERATOR_LOOP = "5";
        public static String ID_OPERATOR_MTS = "6";
        public static String ID_OPERATOR_RELIANCE_CDMA = "7";
        public static String ID_OPERATOR_RELIANCE_GSM = "8";
        public static String ID_OPERATOR_DOCOMO = "9";
        public static String ID_OPERATOR_TATA_INDICOM = "10";
        public static String ID_OPERATOR_UNINOR = "11";
        public static String ID_OPERATOR_VIDEOCON = "12";
        public static String ID_OPERATOR_VODAFONE = "15";
        public static String ID_OPERATOR_MTNL = "30";
        


        public static String ID_OPERATOR_DTH_DISHTV = "18";
        public static String ID_OPERATOR_DTH_TATA_SKY = "20";
        public static String ID_OPERATOR_DTH_SUN = "19";
        public static String ID_OPERATOR_DTH_BIG = "17";
        public static String ID_OPERATOR_DTH_AIRTEL = "16";
        public static String ID_OPERATOR_DTH_VIDEOCON = "21";

        public static String OPERATOR_BILL_DOCOMO = "36";
        public static String OPERATOR_BILL_LOOP = "37";
        public static String OPERATOR_BILL_CELLONE = "38";
        public static String OPERATOR_BILL_IDEA = "39";
        public static String OPERATOR_BILL_TATATELESERVICE = "40";
        public static String OPERATOR_BILL_VODAFONE = "41";
        public static String OPERATOR_BILL_AIRTEL = "42";
        public static String OPERATOR_BILL_RELAINCE = "43";


        public static String IMG_URL_AIRCEL = "Images/m_airtel.png";
        public static String IMG_URL_AIRTEL = "Images/m_aircel.png";
        public static String IMG_URL_BSNL = "Images/m_bsnl.png";
        public static String IMG_URL_IDEA = "Images/m_idea.png";
        public static String IMG_URL_LOOP = "Images/m_loop.png";
        public static String IMG_URL_MTS = "Images/m_mts.png";
        public static String IMG_URL_RELIANCE_CDMA = "Images/m_reliance_cdma.png";
        public static String IMG_URL_RELIANCE_GSM = "Images/m_reliance.png";
        public static String IMG_URL_DOCOMO = "Images/m_docomo.png";
        public static String IMG_URL_TATA_INDICOM = "Images/m_indicom.png";
        public static String IMG_URL_UNINOR = "Images/m_uninor.png";
        public static String IMG_URL_VIDEOCON = "Images/m_mts.png";
        public static String IMG_URL_VODAFONE = "Images/m_vodafone.png";
        public static String IMG_URL_MTNL = "Images/m_mtnl.png";

        public static String IMG_URL_DTH_DISHTV = "Images/d_dishtv.png";
        public static String IMG_URL_DTH_TATA_SKY = "Images/d_tatasky.png";
        public static String IMG_URL_DTH_SUN = "Images/d_sundirect.png";
        public static String IMG_URL_DTH_BIG = "Images/d_bigtv.png";
        public static String IMG_URL_DTH_AIRTEL = "Images/m_airtel.png";
        public static String IMG_URL_DTH_VIDEOCON = "Images/d_videocon.png";


        public static int RECHARGE_MOBILE = 1;
        public static int RECHARGE_DTH = 2;
        public static int RECHARGE_ENTERTAINMENT = 3;

        public static String mCurrentBalance="";
        public static String LoginDescription = "Desc";
        private Object dTHMain;
        private static NavigationService navServ;


        public static bool ClearPersistent(string key)
        {
            if (null == key)
                return false;

            var store = IsolatedStorageSettings.ApplicationSettings;
            if (store.Contains(key))
                store.Remove(key);
            store.Save();
            return true;
        }

        public static bool SavePersistent(string key, object value)
        {
            if (null == value)
                return false;

            var store = IsolatedStorageSettings.ApplicationSettings;
            if (store.Contains(key))
                store[key] = value;
            else
                store.Add(key, value);

            store.Save();
            return true;
        }

        public static T LoadPersistent<T>(string key)
        {
            var store = IsolatedStorageSettings.ApplicationSettings;
            if (!store.Contains(key))
                return default(T);

            return (T)store[key];
        }




        public static void SetProgressIndicator(bool value)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
            SystemTray.ProgressIndicator.IsIndeterminate = value;
            SystemTray.ProgressIndicator.IsVisible = value;
        }

        public Constatnt(Object dTHMain)
        {
            // TODO: Complete member initialization
            this.dTHMain = dTHMain;
        }

        public static List<SideBarItem> LoginItems()
        {

            List<SideBarItem> items=new List<SideBarItem>();
            items.Add(new SideBarItem("Login","/Images/logout.png"));
            items.Add(new SideBarItem("Notifications", "/Images/notif.png"));
            items.Add(new SideBarItem("Contact Details","/Images/location.png"));
            items.Add(new SideBarItem("Live Support","/Images/live_support.png"));
            items.Add(new SideBarItem("Change Pin","/change_pin.png"));
            items.Add(new SideBarItem("Use SMS","/Images/connectivity.png"));
            return items;
        }

        public static GeoCoordinate ConvertGeocoordinate(Geocoordinate geocoordinate)
        {
            return new GeoCoordinate
                (
                geocoordinate.Latitude,
                geocoordinate.Longitude,
                geocoordinate.Altitude ?? Double.NaN,
                geocoordinate.Accuracy,
                geocoordinate.AltitudeAccuracy ?? Double.NaN,
                geocoordinate.Speed ?? Double.NaN,
                geocoordinate.Heading ?? Double.NaN
                );
        }



        public static Color ConvertStringToColor(String hex)
        {
            //remove the # at the front
            hex = hex.Replace("#", "");

            byte a = 255;
            byte r = 255;
            byte g = 255;
            byte b = 255;

            int start = 0;

            //handle ARGB strings (8 characters long)
            if (hex.Length == 8)
            {
                a = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                start = 2;
            }

            //convert RGB characters to bytes
            r = byte.Parse(hex.Substring(start, 2), System.Globalization.NumberStyles.HexNumber);
            g = byte.Parse(hex.Substring(start + 2, 2), System.Globalization.NumberStyles.HexNumber);
            b = byte.Parse(hex.Substring(start + 4, 2), System.Globalization.NumberStyles.HexNumber);

            return Color.FromArgb(a, r, g, b);
        }




          public static List<SideBarItem> LogOutItems()
        {

           
            List<SideBarItem> items=new List<SideBarItem>();

            items.Add(new SideBarItem("Notifications", "/Images/notif.png"));
            items.Add(new SideBarItem("Contact Details","/Images/location.png"));
            items.Add(new SideBarItem("Chat Support","/Images/live_support.png"));
            items.Add(new SideBarItem("Change Pin","/Images/change_pin.png"));
            items.Add(new SideBarItem("Use SMS","/Images/connectivity.png"));
               items.Add(new SideBarItem("LogOut","/Images/logout.png"));
            return items;
        }

          public static Uri getStatusIcon(String product_id)
          {
              Uri ImageUrl = null;

              if (product_id == "0")
              {
                  ImageUrl = new Uri(@"Images/sent.png", UriKind.Relative);


              }
              else if (product_id == "1")
              {
                  ImageUrl = new Uri(@"Images/sent.png", UriKind.Relative);

              }
              else if (product_id == "2")
              {
                  ImageUrl = new Uri(@"Images/not_sent.png", UriKind.Relative);

              }
              else if (product_id == "3")
              {
                  ImageUrl = new Uri(@"Images/not_sent.png", UriKind.Relative);
              }
              else if (product_id == "4")
              {
                  ImageUrl = new Uri(@"Images/hourglass.png", UriKind.Relative);
              }
              else if (product_id == "5")
              {
                  ImageUrl = new Uri(@"Images/double_tick.png", UriKind.Relative);
              } else if (product_id == "6")
              {
                  ImageUrl = new Uri(@"Images/resend.png", UriKind.Relative);
              }
              
              return ImageUrl;
          }

          
          public static void handleButtonClick(Button btnMob, Button btnDth, Button btnEnt, Button btnComplaint, Button btnBill, Button btnReports,NavigationService navigation)
          {
              Constatnt.navServ = navigation;
              btnMob.Click += Common_MouseHover;
              btnDth.Click += Common_MouseHover;
              btnEnt.Click += Common_MouseHover;
              btnComplaint.Click += Common_MouseHover;
              btnBill.Click += Common_MouseHover;
              btnReports.Click += Common_MouseHover;

        }
          public static void Common_MouseHover(object sender, RoutedEventArgs e)
          {
              Button btn = sender as Button;
              string na = btn.Name;
              bool isNetOrSms = NewMainPage.isNetOrSms;

           


              if (na.Equals("btnMob"))
              {
                  if (Login.isTrue)
                  {
                      Constatnt.navServ.Navigate(new Uri("/MobileMain.xaml", UriKind.Relative));
                  }
                  else
                  {
                      if (isNetOrSms)
                      {
                          Constatnt.navServ.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                      }
                      else
                      {
                          MessageBoxResult result =
                         MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                                 "Settings", MessageBoxButton.OKCancel);

                          if (result == MessageBoxResult.OK)
                          {
                              Constatnt.navServ.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                          }
                          if (result == MessageBoxResult.Cancel)
                          {
                              Constatnt.navServ.Navigate(new Uri("/MobileMain.xaml?", UriKind.Relative));
                          }
                      }

                      /* MessageBox.Show("Please login First.");
                       NavigationService.Navigate(new Uri("/Login.xaml?page=EntertainmentMain", UriKind.Relative));*/
                  }
              }
              else if (na.Equals("btnDth"))
              {
                  if (Login.isTrue)
                  {
                      Constatnt.navServ.Navigate(new Uri("/DTHMain.xaml", UriKind.Relative));
                  }
                  else
                  {
                      if (isNetOrSms)
                      {
                          Constatnt.navServ.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                      }
                      else
                      {
                          MessageBoxResult result =
                         MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                                 "Settings", MessageBoxButton.OKCancel);

                          if (result == MessageBoxResult.OK)
                          {
                              Constatnt.navServ.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                          }
                          if (result == MessageBoxResult.Cancel)
                          {
                              Constatnt.navServ.Navigate(new Uri("/DTHMain.xaml?", UriKind.Relative));
                          }
                      }

                      /* MessageBox.Show("Please login First.");
                       NavigationService.Navigate(new Uri("/Login.xaml?page=EntertainmentMain", UriKind.Relative));*/
                  }
              }
              else if (na.Equals("btnComplaint"))
              {
                  if (Login.isTrue)
                  {
                      Constatnt.navServ.Navigate(new Uri("/ReversalNewMain.xaml", UriKind.Relative));
                  }
                  else
                  {
                      if (isNetOrSms)
                      {
                          Constatnt.navServ.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                      }
                      else
                      {
                          MessageBoxResult result =
                         MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                                 "Settings", MessageBoxButton.OKCancel);

                          if (result == MessageBoxResult.OK)
                          {
                              Constatnt.navServ.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                          }
                          if (result == MessageBoxResult.Cancel)
                          {
                              Constatnt.navServ.Navigate(new Uri("/ReversalNewMain.xaml?", UriKind.Relative));
                          }
                      }

                      /* MessageBox.Show("Please login First.");
                       NavigationService.Navigate(new Uri("/Login.xaml?page=EntertainmentMain", UriKind.Relative));*/
                  }
              }
              else if (na.Equals("btnEnt"))
              {
                  if (Login.isTrue)
                  {
                      Constatnt.navServ.Navigate(new Uri("/EntertainmentMain.xaml", UriKind.Relative));
                  }
                  else
                  {
                      if (isNetOrSms)
                      {
                          Constatnt.navServ.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                      }
                      else
                      {
                          MessageBoxResult result =
                         MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                                 "Settings", MessageBoxButton.OKCancel);

                          if (result == MessageBoxResult.OK)
                          {
                              Constatnt.navServ.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                          }
                          if (result == MessageBoxResult.Cancel)
                          {
                              Constatnt.navServ.Navigate(new Uri("/EntertainmentMain.xaml?", UriKind.Relative));
                          }
                      }

                      /* MessageBox.Show("Please login First.");
                       NavigationService.Navigate(new Uri("/Login.xaml?page=EntertainmentMain", UriKind.Relative));*/
                  }
              }
              else if (na.Equals("btnBill"))
              {
                  if (Login.isTrue)
                  {
                      Constatnt.navServ.Navigate(new Uri("/BillPaymentMain.xaml", UriKind.Relative));
                  }
                  else
                  {
                      if (isNetOrSms)
                      {
                          Constatnt.navServ.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                      }
                      else
                      {
                          MessageBoxResult result =
                         MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                                 "Settings", MessageBoxButton.OKCancel);

                          if (result == MessageBoxResult.OK)
                          {
                              Constatnt.navServ.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                          }
                          if (result == MessageBoxResult.Cancel)
                          {
                              Constatnt.navServ.Navigate(new Uri("/BillPaymentMain.xaml?", UriKind.Relative));
                          }
                      }

                      /* MessageBox.Show("Please login First.");
                       NavigationService.Navigate(new Uri("/Login.xaml?page=EntertainmentMain", UriKind.Relative));*/
                  }
              }
              else if (na.Equals("btnReports"))
              {
                  if (Login.isTrue)
                  {
                      Constatnt.navServ.Navigate(new Uri("/ReportMain.xaml", UriKind.Relative));
                  }
                  else
                  {
                      if (isNetOrSms)
                      {
                          Constatnt.navServ.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                      }
                      else
                      {
                          MessageBoxResult result =
                         MessageBox.Show("Internet setting is not enabled. \nWould you like to change the Settings?",
                                 "Settings", MessageBoxButton.OKCancel);

                          if (result == MessageBoxResult.OK)
                          {
                              Constatnt.navServ.Navigate(new Uri("/SettingsPage.xaml?", UriKind.Relative));
                          }
                          if (result == MessageBoxResult.Cancel)
                          {
                              Constatnt.navServ.Navigate(new Uri("/ReportMain.xaml?", UriKind.Relative));
                          }
                      }

                      /* MessageBox.Show("Please login First.");
                       NavigationService.Navigate(new Uri("/Login.xaml?page=EntertainmentMain", UriKind.Relative));*/
                  }
              }
              
              //MessageBox.Show(na);
          }


          public static Uri getOperatorUri(string product_id)
          {
              Uri ImageUrl=null;


              if (product_id == Constatnt.ID_OPERATOR_DTH_AIRTEL)
              {
                  ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);


              }
              else if (product_id == Constatnt.ID_OPERATOR_DTH_TATA_SKY)
              {
                  ImageUrl = new Uri(@"Images/d_tatasky.png", UriKind.Relative);

              }
              else if (product_id == Constatnt.ID_OPERATOR_DTH_SUN)
              {
                  ImageUrl = new Uri(@"Images/d_sundirect.png", UriKind.Relative);

              }
              else if (product_id == Constatnt.ID_OPERATOR_DTH_DISHTV)
              {
                  ImageUrl = new Uri(@"Images/d_dishtv.png", UriKind.Relative);
              }
              else if (product_id == Constatnt.ID_OPERATOR_DTH_VIDEOCON)
              {
                  ImageUrl = new Uri(@"Images/d_videocon.png", UriKind.Relative);
              }
              else if (product_id == Constatnt.ID_OPERATOR_DTH_BIG)
              {
                  ImageUrl = new Uri(@"Images/d_bigtv.png", UriKind.Relative);
              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_AIRTEL)
              {
                  ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);


              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_AIRCEL)
              {
                  ImageUrl = new Uri(@"Images/m_aircel.png", UriKind.Relative);

              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_LOOP)
              {
                  ImageUrl = new Uri(@"Images/m_loop.png", UriKind.Relative);

              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_BSNL)
              {
                  ImageUrl = new Uri(@"Images/m_bsnl.png", UriKind.Relative);
              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_IDEA)
              {
                  ImageUrl = new Uri(@"Images/m_idea.png", UriKind.Relative);
              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_MTS)
              {
                  ImageUrl = new Uri(@"Images/m_mts.png", UriKind.Relative);
              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_UNINOR)
              {
                  ImageUrl = new Uri(@"Images/m_loop.png", UriKind.Relative);

              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_VODAFONE)
              {
                  ImageUrl = new Uri(@"Images/m_vodafone.png", UriKind.Relative);
              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_VIDEOCON)
              {
                  ImageUrl = new Uri(@"Images/m_videocon.png", UriKind.Relative);
              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_DOCOMO || product_id.ToString() == "27")
              {
                  ImageUrl = new Uri(@"Images/m_docomo.png", UriKind.Relative);
              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_TATA_INDICOM)
              {
                  ImageUrl = new Uri(@"Images/m_indicom.png", UriKind.Relative);
              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_RELIANCE_CDMA)
              {
                  ImageUrl = new Uri(@"Images/m_reliance_cdma.png", UriKind.Relative);
              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_RELIANCE_GSM)
              {
                  ImageUrl = new Uri(@"Images/m_reliance.png", UriKind.Relative);
              }
              else if (product_id.ToString() == Constatnt.ID_OPERATOR_MTNL)
              {
                  ImageUrl = new Uri(@"Images/m_mtnl.png", UriKind.Relative);
              }
            return ImageUrl;

          }

          public static bool _isSettingsOpen = false;
          public static void handleSidebar(int index,NavigationService navSer,string page)
          {
              if (Login.isTrue)
              {
                  // DTHMain instance = new DTHMain();
                  //btn.Clicked += instance.StartClick;
                  // l2.SelectionChanged+=instance.listBox1_SelectedIndexChanged();
                  switch (index)
                  {
                      case 5:
                          navSer.Navigate(new Uri("/LogoutMain.xaml", UriKind.RelativeOrAbsolute));
                          
                          break;
                     
                      case 2:
                          navSer.Navigate(new Uri("/SocketCheck.xaml", UriKind.RelativeOrAbsolute));
                          
                          break;
                      case 3:
                          navSer.Navigate(new Uri("/resetPassword.xaml", UriKind.Relative));
                          
                          break;
                      case 1:
                          navSer.Navigate(new Uri("/LocationAddress.xaml", UriKind.RelativeOrAbsolute));
                          
                          break;
                      case 4:
                          navSer.Navigate(new Uri("/SettingsPage.xaml", UriKind.Relative));
                          
                          break;
                      case 0:
                          navSer.Navigate(new Uri("/NewNotification.xaml", UriKind.Relative));
                          
                          break;
                      default:
                          navSer.Navigate(new Uri("/NewMainPage.xaml", UriKind.Relative));
                          
                          break;
                  }
              }
              else
              {
                  switch (index)
                  {

                      case 0:
                          Constatnt.getLoc();
                          navSer.Navigate(new Uri("/Login.xaml?page="+page, UriKind.Relative));
                          

                          break;
                     
                      case 3:
                          if (Login.isTrue)
                          {
                              navSer.Navigate(new Uri("/SocketCheck.xaml", UriKind.RelativeOrAbsolute));
                              
                          }
                          else
                          {
                              MessageBox.Show("Please login First");
                              navSer.Navigate(new Uri("/Login.xaml?page="+page, UriKind.Relative));
                              
                          }
                          break;
                      case 4:
                          if (Login.isTrue)
                          {
                              navSer.Navigate(new Uri("/resetPassword.xaml", UriKind.Relative));
                              
                          }
                          else
                          {
                              MessageBox.Show("Please login First");
                              navSer.Navigate(new Uri("/Login.xaml?page=" + page, UriKind.Relative));
                              
                          }
                          
                          break;
                     
                      case 2:
                          if (Login.isTrue)
                          {
                              navSer.Navigate(new Uri("/LocationAddress.xaml", UriKind.RelativeOrAbsolute));
                              
                          }
                          else
                          {
                              MessageBox.Show("Please login First");
                              navSer.Navigate(new Uri("/Login.xaml?page=" + page, UriKind.Relative));
                              
                          }

                          break;


                      case 1:
                          if (Login.isTrue)
                          {
                              navSer.Navigate(new Uri("/NewNotification.xaml", UriKind.RelativeOrAbsolute));
                              
                          }
                          else
                          {
                              MessageBox.Show("Please login First");
                              navSer.Navigate(new Uri("/Login.xaml?page=" + page, UriKind.Relative));
                              
                          }

                          break;


                      case 5:
                          if (Login.isTrue)
                          {
                              navSer.Navigate(new Uri("/SettingsPage.xaml", UriKind.Relative));
                              
                          }
                          else
                          {
                              /*MessageBox.Show("Please login First");
                              navSer.Navigate(new Uri("/Login.xaml?page=" + page, UriKind.Relative));*/
                              navSer.Navigate(new Uri("/SettingsPage.xaml", UriKind.Relative));
                              
                          }
                          break;
                      default:
                          navSer.Navigate(new Uri("/NewMainPage.xaml", UriKind.Relative));
                          
                          break;

                  }
              }

             

          }

          public static void hideControls(Border b1,Border b2,TextBlock balText)
          {
              bool isLogin = Constatnt.LoadPersistent<Boolean>(Constatnt.IS_LOGIN);
              if (isLogin)
              {
              }
              else
              {
                  b1.Visibility = Visibility.Collapsed;
                  b2.Visibility = Visibility.Collapsed;
                  balText.Visibility = Visibility.Collapsed;
              }
          }


          public static void loadSideBar(ListBox L1, TextBlock balanceText, TextBlock merchantText, TextBlock notificationCount, Border notiGrid, Border b1, Border b2, TextBlock balText)
          {
              SQLiteAsyncConnection conn = new SQLiteAsyncConnection("plans");
              //await conn.CreateTableAsync<LocationData>();
             
              int count = Constatnt.LoadPersistent<int>(Constatnt.NOTIFICATION_COUNT);
              //notificationCount.Text = count + "";
              //int count = Constatnt.LoadPersistent<int>(Constatnt.NOTIFICATION_COUNT);
            l2 = L1;
            l2.Height = 400;
              bool isLogin = Constatnt.LoadPersistent<Boolean>(Constatnt.IS_LOGIN);
              Constatnt.mCurrentBalance = Constatnt.LoadPersistent<String>(Constatnt.CURRENT_BALANCE);
           // balanceText.Text =Constatnt.mCurrentBalance;
            if (isLogin)
            {
                //setAddressData();
                if (count != 0)
                {
                    notificationCount.Text = "  "+count;
                }
                else
                {
                    notificationCount.Visibility = Visibility.Collapsed;
                    notiGrid.Visibility = Visibility.Collapsed;
                }
                decimal d = 0;
                try
                {
                    d = Decimal.Parse(Constatnt.mCurrentBalance);
                }
                catch (Exception e)
                {
                    d = 0;
                }
                balanceText.Text = "Rs. " + d.ToString("0.00");
                
                //merchantText.Text = Constatnt.SHOPNAME;
                merchantText.Text = Constatnt.SHOPNAME;
                foreach (var items in Constatnt.LogOutItems())
                {
                    SideBarUserControl sideBar = new SideBarUserControl();
                    sideBar.SideBarTextBlock.Text = items.Name.ToString();
                    try
                    {
                        string uria = items.ImageUrl;
                        Uri uri = new Uri(items.ImageUrl, UriKind.RelativeOrAbsolute);
                        ImageSource imgSource = new BitmapImage(uri);
                        sideBar.sideBarIcons.Source = imgSource;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.Message);
                    }
                    L1.Items.Add(sideBar);

                }
               
            }


            else
            {
                b1.Visibility = Visibility.Collapsed;
                b2.Visibility = Visibility.Collapsed;
                balText.Visibility = Visibility.Collapsed;
                notificationCount.Visibility = Visibility.Collapsed;
                notiGrid.Visibility = Visibility.Collapsed;
                merchantText.Text = Constatnt.SHOPNAME;
                foreach (var items in Constatnt.LoginItems())
                {
                    SideBarUserControl sideBar = new SideBarUserControl();
                    sideBar.SideBarTextBlock.Text = items.Name.ToString();
                    try
                    {
                        string uria = items.ImageUrl;
                        Uri uri = new Uri(items.ImageUrl, UriKind.RelativeOrAbsolute);
                        ImageSource imgSource = new BitmapImage(uri);
                        sideBar.sideBarIcons.Source = imgSource;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.Message);
                    }
                    L1.Items.Add(sideBar);

                }
            }
}

        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = l2.SelectedIndex;
            MessageBox.Show("" + index);
            /* SideBarUserControl s = L1.SelectedValue as SideBarUserControl;
             try
             {
                 MessageBox.Show(s.SideBarTextBlock.Text);
             }catch(Exception e1){
                 System.Diagnostics.Debug.WriteLine(e1.StackTrace);
             }*/
        }


        




        public static string getTime()
        {
            DateTime unixEpoch = new DateTime(1970, 1, 1);
            DateTime currentDate = DateTime.Now;
            long totalMiliSecond = (currentDate.Ticks - unixEpoch.Ticks) / 10000;
            return totalMiliSecond.ToString();
        }

        public static string getDeviceId()
        {
            // get the unique device id for the publisher per device
            Byte[] DeviceArrayID = (Byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");
            string UniqueDeviceID = Convert.ToBase64String(DeviceArrayID);
            return UniqueDeviceID;

        }

        internal static string CalculateSHA1(string text)
        {
            SHA1Managed s = new SHA1Managed();
            UTF8Encoding enc = new UTF8Encoding();
            s.ComputeHash(enc.GetBytes(text.ToCharArray()));
            System.Diagnostics.Debug.WriteLine("Original Text {0}, Access {1}", text, Convert.ToBase64String(s.Hash));
            string orginal = BitConverter.ToString(s.Hash).Replace("-", "");
            return orginal;
        }

        public static bool checkInternetConnection()
        {
            return (Microsoft.Phone.Net.NetworkInformation.NetworkInterface.NetworkInterfaceType !=
Microsoft.Phone.Net.NetworkInformation.NetworkInterfaceType.None);
        }


        async public static void getLoc()
        {
            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;

            try
            {
                Geoposition geoposition = await geolocator.GetGeopositionAsync(
                    maximumAge: TimeSpan.FromMinutes(0),
                    timeout: TimeSpan.FromSeconds(1)
                    );
                latti = geoposition.Coordinate.Latitude.ToString("0.00");
                longi = geoposition.Coordinate.Longitude.ToString("0.00");

                //MessageBox.Show(geoposition.Coordinate.Latitude.ToString("0.00") + geoposition.Coordinate.Longitude.ToString("0.00"));
                // LongitudeTextBlock.Text = geoposition.Coordinate.Longitude.ToString("0.00");
            }
            catch (Exception ex)
            {
            }
        }


        
    }
}
