﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class TransactionNew : PhoneApplicationPage
    {
        public TransactionNew()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            
             DateTime dat = DateTime.Now;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            page = 0;
           
           
                   Task<string> abc1 = MakeWebRequest("1", year + "-" + month + "-" + day);
                   Task<string> abc2 = MakeWebRequest("2", year + "-" + month + "-" + day);
                   Task<string> abc3 = MakeWebRequest("3", year + "-" + month + "-" + day);
                   Task<string> abc4 = MakeWebRequest("4", year + "-" + month + "-" + day);
                   Task<string> abc5 = MakeWebRequest("5", year + "-" + month + "-" + day);


           
        }

        private void Mobile_Recharge_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            ListMobileTrans.Items.Clear();
            page = 0;
            Task<string> abc3 = MakeWebRequest("1", year + "-" + month + "-" + day);
        }

        private void DTH_Recharge_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            ListDthTrans.Items.Clear();
            page = 0;
            Task<string> abc3 = MakeWebRequest("2", year + "-" + month + "-" + day);
        }

        private void Bill_Payment_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            ListBillTrans.Items.Clear();
            page = 0;
            Task<string> abc3 = MakeWebRequest("4", year + "-" + month + "-" + day);
        }

        private void Entertainment_Recharge_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            ListEntTrans.Items.Clear();
            page = 0;
            Task<string> abc3 = MakeWebRequest("3", year + "-" + month + "-" + day);
        }

        private void Pay1_Recharge_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            ListPay1Trans.Items.Clear();
            page = 0;
            Task<string> abc3 = MakeWebRequest("3", year + "-" + month + "-" + day);
        }

        private void DatePicker_ValueChanged(
       object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;


            switch (main_pivot.SelectedIndex)
            {
                case 0:
                   Task<string> abc = MakeWebRequest("1", year + "-" + month + "-" + day);
                    break;

                case 1:
                    Task<string> abc1 = MakeWebRequest("2", year + "-" + month + "-" + day);
                    break;
                case 2:
                    Task<string> abc2 = MakeWebRequest("4", year + "-" + month + "-" + day);
                    break;

                case 3:                   
                    Task<string> abc3 = MakeWebRequest("3", year + "-" + month + "-" + day);
                    break;
            }
            //Task<string> abc3 = MakeWebRequest(year + "-" + month + "-" + day);
            //string day=
            //MessageBox.Show(da.ToString());
        }

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }


        int page = 0;
        public async Task<string> MakeWebRequest(string transServiceType,string date)
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";
            /*ListMobileTrans.Items.Clear();
            ListDthTrans.Items.Clear();
            ListEntTrans.Items.Clear();*/
            Button bt = new Button();
            bt.Content = "Add More";
            bt.Foreground = new SolidColorBrush(Colors.Red);
            // listBox1.Items.Remove(bt);
            //listBox1.Items.RemoveAt(listBox1.Items.Count);// (bt);

            if (transServiceType.Equals("1"))
            {
                if(ListMobileTrans.Items.Count!=0)
                ListMobileTrans.Items.RemoveAt(ListMobileTrans.Items.Count-1);// (bt);
            }
            else if (transServiceType.Equals("2"))
            {
                if (ListDthTrans.Items.Count != 0)
                    ListDthTrans.Items.RemoveAt(ListDthTrans.Items.Count - 1);// (bt);
            }
            else if (transServiceType.Equals("3"))
            {
                if (ListEntTrans.Items.Count != 0)
                    ListEntTrans.Items.RemoveAt(ListEntTrans.Items.Count - 1);// (bt);
            }
            else if (transServiceType.Equals("4"))
            {
                if (ListBillTrans.Items.Count != 0)
                    ListBillTrans.Items.RemoveAt(ListBillTrans.Items.Count - 1);// (bt);
            }
            else if (transServiceType.Equals("5"))
            {
                if (ListPay1Trans.Items.Count != 0)
                    ListPay1Trans.Items.RemoveAt(ListPay1Trans.Items.Count - 1);// (bt);
            }
            

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
            {
                        new KeyValuePair<string, string>("method", "lastTransactions"),
                        new KeyValuePair<string, string>("date",date),
                        new KeyValuePair<string, string>("date2",date),
                        new KeyValuePair<string, string>("limit","10"),
                        new KeyValuePair<string, string>("page", ""+page++),
                        new KeyValuePair<string, string>("service", transServiceType),
                        new KeyValuePair<string, string>("device_type", "windows8")
            });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            
            int i = 0;
            foreach (var item in desc)
            {
                foreach (var innerItem in item)
                {
                    var timestamp = innerItem["0"];
                    var time = timestamp["timestamp"];

                    var products = innerItem["products"];
                    var name = products["name"];
                    var vendors_activations = innerItem["vendors_activations"];
                    var product_id = vendors_activations["product_id"];
                    var mobile = vendors_activations["mobile"];
                    var amount = vendors_activations["amount"];
                    var status = vendors_activations["status"];
                    i++;

                    decimal d = 0;
                    try
                    {
                        d = Decimal.Parse(amount.ToString());
                    }
                    catch (Exception e)
                    {
                    }
                    StatusRow statusRow = new StatusRow();
                    statusRow.TextMobile.Text = mobile.ToString();
                    statusRow.TextAmt.Text = "Rs. " + d.ToString("0.00");
                    statusRow.TextDate.Text = time.ToString();
                    if (i % 2 == 0)
                    {
                        statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                    }
                    else
                    {
                         statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                    }
                    if (status.ToString() == "1" || status.ToString() == "0")
                    {
                        statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                    }
                    else
                    {
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                    }
                    statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));

                    if (transServiceType.Equals("1"))
                    {
                        ListMobileTrans.Items.Add(statusRow);
                        if (ListMobileTrans.Items.Count % 10 == 0 && ListMobileTrans.Items.Count != 0)
                        {

                            bt.Click += (s, e) =>
                            {
                                Task<string> abc3 = MakeWebRequest(transServiceType, date);
                            };
                            ListMobileTrans.Items.Add(bt);
                        }
                        else
                        {
                        }

                       
                    }
                    else if (transServiceType.Equals("2"))
                    {
                        ListDthTrans.Items.Add(statusRow);
                        if (ListDthTrans.Items.Count % 10 == 0 && ListDthTrans.Items.Count != 0)
                        {

                            bt.Click += (s, e) =>
                            {
                                Task<string> abc3 = MakeWebRequest(transServiceType, date);
                            };
                            ListDthTrans.Items.Add(bt);
                        }
                        else
                        {
                        }
                    }else if (transServiceType.Equals("3"))
                    {
                        ListEntTrans.Items.Add(statusRow);
                        if (ListEntTrans.Items.Count % 10 == 0 && ListEntTrans.Items.Count != 0)
                        {

                            bt.Click += (s, e) =>
                            {
                                Task<string> abc3 = MakeWebRequest(transServiceType, date);
                            };
                            ListEntTrans.Items.Add(bt);
                        }
                        else
                        {
                        }

                    }
                    else if (transServiceType.Equals("4"))
                    {
                        ListBillTrans.Items.Add(statusRow);
                        if (ListBillTrans.Items.Count % 10 == 0 && ListBillTrans.Items.Count != 0)
                        {

                            bt.Click += (s, e) =>
                            {
                                Task<string> abc3 = MakeWebRequest(transServiceType, date);
                            };
                            ListBillTrans.Items.Add(bt);
                        }
                        else
                        {
                        }
                       
                    }
                    else if (transServiceType.Equals("5"))
                    {
                        ListPay1Trans.Items.Add(statusRow);
                        if (ListPay1Trans.Items.Count % 10 == 0 && ListPay1Trans.Items.Count != 0)
                        {

                            bt.Click += (s, e) =>
                            {
                                Task<string> abc3 = MakeWebRequest(transServiceType, date);
                            };
                            ListPay1Trans.Items.Add(bt);
                        }
                        else
                        {
                        }
                        
                    }



                }

            }

            if (transServiceType.Equals("1"))
            {
                if (ListMobileTrans.Items.Count == 0)
                {
                    noMobGrid.Visibility = Visibility.Visible;
                }
            }
            else if (transServiceType.Equals("2"))
            {
                if (ListDthTrans.Items.Count == 0)
                {
                    noDthGrid.Visibility = Visibility.Visible;
                }
            }
            else if (transServiceType.Equals("3"))
            {
                if (ListEntTrans.Items.Count == 0)
                {
                    noEntGrid.Visibility = Visibility.Visible;
                }
            }
            else if (transServiceType.Equals("4"))
            {
                if (ListBillTrans.Items.Count == 0)
                {
                    noBillGrid.Visibility = Visibility.Visible;
                }
            }
            else if (transServiceType.Equals("5"))
            {
                if (ListPay1Trans.Items.Count == 0)
                {
                    noPay1Grid.Visibility = Visibility.Visible;
                }
            }
            Constatnt.SetProgressIndicator(false);
            return responseString;

        }



        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }

        private void Pivot_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
          /* DateTime dat = DateTime.Now;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            page = 0;
            ListMobileTrans.Items.Clear();
            ListDthTrans.Items.Clear();
            ListEntTrans.Items.Clear();
            ListBillTrans.Items.Clear();
            ListPay1Trans.Items.Clear();
            switch (((Pivot)sender).SelectedIndex)
            {
                case 0:
                   Task<string> abc = MakeWebRequest("1", year + "-" + month + "-" + day);
                    break;

                case 1:
                    Task<string> abc1 = MakeWebRequest("2", year + "-" + month + "-" + day);
                    break;
                case 2:
                    Task<string> abc2 = MakeWebRequest("4", year + "-" + month + "-" + day);
                    break;

                case 3:                   
                    Task<string> abc3 = MakeWebRequest("3", year + "-" + month + "-" + day);
                    break;
                case 4:
                    Task<string> abc4 = MakeWebRequest("5", year + "-" + month + "-" + day);
                    break;
            }*/
        }

        






    }
}