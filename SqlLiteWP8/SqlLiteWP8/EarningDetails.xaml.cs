﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class EarningDetails : PhoneApplicationPage
    {
        int totalAmt;
        double totalIncome;
        EarningDetailRow detailRow;
        public EarningDetails()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid,border1, border2, balText);
            
        }
        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("date"))
            {
               string date = NavigationContext.QueryString["date"];
               string replacedString = Regex.Replace(date, @"-", "");
              /* var charsToRemove = new string[] {"-"};
               string str="";
               foreach (var c in charsToRemove)
               {
                   str = str.Replace(c, string.Empty);
               }*/
               Task<string> abc = MakeWebRequest(replacedString);
            }
        }


        public async Task<string> MakeWebRequest(String date)
        {
            string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "saleReport"),
                        new KeyValuePair<string, string>("date",date+"-"+date),
                        
                        new KeyValuePair<string, string>("device_type", "")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


         

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            string status = dict["status"].ToString();
            if (status.Equals("success"))
            {
                ListEarn.Items.Clear();
                var desnInner = desc[0];
                var descSubInner = desnInner["1"];
                var data = descSubInner["data"];
                int i = 0;
                foreach (var item in data)
                {
                    var getZero=item["0"];
                    var counts = getZero["counts"];
                    var amount = getZero["amount"];
                    var income = getZero["income"];
                    var products = item["products"];
                    var name = products["name"];
                    var id = products["id"];

                    detailRow = new EarningDetailRow();
                    detailRow.TextAmount.Text = amount.ToString();
                    detailRow.TextCount.Text = counts.ToString();
                    detailRow.TextIncome.Text = income.ToString();
                    detailRow.TextProdName.Text = name.ToString();
                    int amt = Int32.Parse(amount.ToString());
                    double totalInc = double.Parse(income.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                    totalAmt = totalAmt + amt;
                    totalIncome = totalIncome+totalInc;
                    if (i % 2 == 0)
                    {
                        detailRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#dbe0fe")); 
                    }
                    else
                    {
                        detailRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                    }
                  /*  earningRow.Earning.Text = income.ToString();
                    earningRow.EarningDate.Text = earnDate.ToString();
                    earningRow.EarningSale.Text = amount.ToString();
                    */
                   ListEarn.Items.Add(detailRow);
                    i++;
                }
              /*  detailRow.TextAmount.Text = totalAmt.ToString();
                detailRow.TextCount.Text = i.ToString();
                detailRow.TextIncome.Text = totalIncome.ToString();
                detailRow.TextProdName.Text = "Total";
                detailRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#dbe0fe"));
                ListEarn.Items.Add(detailRow);*/
            }
            else
            {
                MessageBox.Show("Data Not Recieved");
            }



            return responseString;

        }


        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

       
    }
}