﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Maps;
using System.Device.Location;
using Microsoft.Phone.Maps.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using Microsoft.Phone.Maps.Toolkit;
using Windows.Devices.Geolocation;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class AddLocation : PhoneApplicationPage
    {
        const int MIN_ZOOM_LEVEL = 1;
        const int MAX_ZOOM_LEVEL = 20;
        const int MIN_ZOOMLEVEL_FOR_LANDMARKS = 16;
        GeoCoordinate geo;
        string longitude, latiitude;
        GeoCoordinate currentLocation = null;
        MapLayer locationLayer = null;
        public AddLocation()
        {
            InitializeComponent();
           // Constatnt.loadSideBar(L1, balanceText);


          /*  sampleMapFull.Center = new GeoCoordinate(19.1802370, 72.8554150);
            sampleMapFull.ZoomLevel = 10;
            MapLayer layer0 = new MapLayer();
            Pushpin pushpin0 = new Pushpin();
            // sampleMapFull.map
            pushpin0.GeoCoordinate = new GeoCoordinate(19.180237, 72.8554150);
            MapOverlay overlay0 = new MapOverlay();
            overlay0.Content = pushpin0;
            overlay0.GeoCoordinate = new GeoCoordinate(19.180237, 72.8554150);
            layer0.Add(overlay0);
            sampleMapFull.Layers.Add(layer0);
            // GetLocation();

            */

            // Add the layer with the pins in to the map


        }

        async private void GetLocation()
        {
            try
            {
                var geolocator = new Geolocator();

                Geoposition position = await geolocator.GetGeopositionAsync();

                Geocoordinate coordinate = position.Coordinate;
                MapLayer layer0 = new MapLayer();
                Pushpin pushpin2 = new Pushpin();
                pushpin2.GeoCoordinate = new GeoCoordinate(coordinate.Latitude, coordinate.Longitude);
                MapOverlay overlay2 = new MapOverlay();
                overlay2.Content = pushpin2;
                layer0.Add(overlay2);
                sampleMapFull.Layers.Add(layer0);
                MessageBox.Show("Latitude = " + coordinate.Latitude + " Longitude = " + coordinate.Longitude);



            }
            catch (Exception ee)
            {
                Console.WriteLine(ee.StackTrace);
            }
        }

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }

        private void sampleMapFull_Loaded(object sender, RoutedEventArgs e)
        {
            MapsSettings.ApplicationContext.ApplicationId = "44aefe8a-fff1-40b9-8a2e-76c835339fdc";
            MapsSettings.ApplicationContext.AuthenticationToken = "hrN1FC4q733LtXFkLPv4kg";
        }



        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
           
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }



        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
            Constatnt.handleSidebar(index, this.NavigationService, "NewMainPage");

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("latiitude"))
            {
                latiitude = NavigationContext.QueryString["latiitude"];
                if (NavigationContext.QueryString.ContainsKey("longitude"))
                {
                     longitude = NavigationContext.QueryString["longitude"];
                    //ShowMyLocationOnTheMapByCordinate(Convert.ToDouble(latiitude),Convert.ToDouble(longitude));
                   // string longitude = NavigationContext.QueryString["longitude"];
                     sampleMapFull.Center = new GeoCoordinate(Convert.ToDouble(latiitude), Convert.ToDouble(longitude));
                    sampleMapFull.ZoomLevel = 13;
                    MapLayer layer0 = new MapLayer();
                    Pushpin pushpin0 = new Pushpin();
                    // sampleMapFull.map
                    pushpin0.GeoCoordinate = new GeoCoordinate(Convert.ToDouble(latiitude), Convert.ToDouble(longitude));
                    MapOverlay overlay0 = new MapOverlay();

                    Ellipse myCircle = new Ellipse();
                    myCircle.Fill = new SolidColorBrush(Colors.Blue);
                    myCircle.Height = 20;
                    myCircle.Width = 20;
                    myCircle.Opacity = 50;


                    overlay0.Content = myCircle;
                    overlay0.GeoCoordinate = new GeoCoordinate(Convert.ToDouble(latiitude), Convert.ToDouble(longitude));
                    layer0.Add(overlay0);
                    sampleMapFull.Layers.Add(layer0);
                }
            }


        }

        private async void ShowMyLocationOnTheMapByCordinate(double lat, double longi)
        {
            
            Geolocator gl = new Geolocator();
            GeoCoordinate myGeoCoordinate = new GeoCoordinate();
            myGeoCoordinate.Latitude = lat;
            myGeoCoordinate.Longitude = longi;

            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Colors.Blue);
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;

            MapOverlay myLocationOverlay = new MapOverlay();
            myLocationOverlay.Content = myCircle;
            myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
            myLocationOverlay.GeoCoordinate = myGeoCoordinate;

            MapLayer myLocationLayer = new MapLayer();
            myLocationLayer.Add(myLocationOverlay);

            sampleMapFull.Layers.Add(myLocationLayer);

        }




        private void ShowLocation()
        {
            // Create a small circle to mark the current location.
            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Colors.Blue);
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;

            // Create a MapOverlay to contain the circle.
            MapOverlay myLocationOverlay = new MapOverlay();
            myLocationOverlay.Content = myCircle;
            myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
            myLocationOverlay.GeoCoordinate = currentLocation;

            // Create a MapLayer to contain the MapOverlay.
            locationLayer = new MapLayer();
            locationLayer.Add(myLocationOverlay);

            // Add the MapLayer to the Map.
            sampleMapFull.Layers.Add(locationLayer);

        }






        private void CenterMapOnLocation()
        {
            sampleMapFull.Center = currentLocation;
        }

        private void saveLocatin_Click(object sender, RoutedEventArgs e)
        {
              NavigationService.Navigate(new Uri("/LocationAddress.xaml?latiitude="+latiitude+"&longitude="+longitude,UriKind.Relative));
              NavigationService.RemoveBackEntry();
        }

        private void sampleMapFull_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            sampleMapFull.Layers.Clear();
            Point p = e.GetPosition(this.sampleMapFull);
            geo = new GeoCoordinate();
            geo = sampleMapFull.ConvertViewportPointToGeoCoordinate(p);// ViewportPointToLocation(p);
            //sampleMapFull.ZoomLevel = 10;
            sampleMapFull.Center = geo;


            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Colors.Blue);
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;


            MapOverlay myLocationOverlay = new MapOverlay();
            myLocationOverlay.Content = myCircle;
            myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
            myLocationOverlay.GeoCoordinate = geo;
            latiitude = geo.Latitude.ToString();
            longitude = geo.Longitude.ToString();


            MapLayer myLocationLayer = new MapLayer();
            myLocationLayer.Add(myLocationOverlay);

            sampleMapFull.Layers.Add(myLocationLayer);



        }


        private void sampleMap_Loaded(object sender, RoutedEventArgs e)
        {
            MapsSettings.ApplicationContext.ApplicationId = "44aefe8a-fff1-40b9-8a2e-76c835339fdc";
            MapsSettings.ApplicationContext.AuthenticationToken = "hrN1FC4q733LtXFkLPv4kg";
        }
    }
}