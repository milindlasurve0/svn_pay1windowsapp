﻿#pragma checksum "D:\Pay1 Windows 02-04-2014\Old Windows App Backup\SqlLiteWP8\SqlLiteWP8\BillPayment.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "DAD8C2E5DD1D6A04FA2AF8838B4CE4FA"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace SqlLiteWP8 {
    
    
    public partial class BillPayment : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid Container;
        
        internal System.Windows.VisualStateGroup SettingsStateGroup;
        
        internal System.Windows.VisualState SettingsClosedState;
        
        internal System.Windows.VisualState SettingsOpenState;
        
        internal System.Windows.Controls.Grid SettingsPane;
        
        internal System.Windows.Controls.ListBox L1;
        
        internal System.Windows.Controls.TextBlock TextMerchant;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.TextBlock balanceText;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Controls.Image operator_logo;
        
        internal System.Windows.Controls.TextBlock operatorName;
        
        internal System.Windows.Controls.TextBox textBillMobNumber;
        
        internal System.Windows.Controls.TextBox textBillMobAmount;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/SqlLiteWP8;component/BillPayment.xaml", System.UriKind.Relative));
            this.Container = ((System.Windows.Controls.Grid)(this.FindName("Container")));
            this.SettingsStateGroup = ((System.Windows.VisualStateGroup)(this.FindName("SettingsStateGroup")));
            this.SettingsClosedState = ((System.Windows.VisualState)(this.FindName("SettingsClosedState")));
            this.SettingsOpenState = ((System.Windows.VisualState)(this.FindName("SettingsOpenState")));
            this.SettingsPane = ((System.Windows.Controls.Grid)(this.FindName("SettingsPane")));
            this.L1 = ((System.Windows.Controls.ListBox)(this.FindName("L1")));
            this.TextMerchant = ((System.Windows.Controls.TextBlock)(this.FindName("TextMerchant")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.balanceText = ((System.Windows.Controls.TextBlock)(this.FindName("balanceText")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.operator_logo = ((System.Windows.Controls.Image)(this.FindName("operator_logo")));
            this.operatorName = ((System.Windows.Controls.TextBlock)(this.FindName("operatorName")));
            this.textBillMobNumber = ((System.Windows.Controls.TextBox)(this.FindName("textBillMobNumber")));
            this.textBillMobAmount = ((System.Windows.Controls.TextBox)(this.FindName("textBillMobAmount")));
        }
    }
}

