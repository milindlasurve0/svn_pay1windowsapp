﻿#pragma checksum "D:\Pay1 Windows 02-04-2014\Old Windows App Backup\SqlLiteWP8\SqlLiteWP8\AccountHistory.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "6974D4B779D98C3638CEE1D40BDAD6CF"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace SqlLiteWP8 {
    
    
    public partial class AccountHistory : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid Container;
        
        internal System.Windows.VisualStateGroup SettingsStateGroup;
        
        internal System.Windows.VisualState SettingsClosedState;
        
        internal System.Windows.VisualState SettingsOpenState;
        
        internal System.Windows.Controls.Grid SettingsPane;
        
        internal System.Windows.Controls.Button btnMob;
        
        internal System.Windows.Controls.Button btnEnt;
        
        internal System.Windows.Controls.Button btnDth;
        
        internal System.Windows.Controls.Button btnComplaint;
        
        internal System.Windows.Controls.Button btnBill;
        
        internal System.Windows.Controls.Button btnReports;
        
        internal System.Windows.Controls.ListBox L1;
        
        internal System.Windows.Controls.TextBlock TextMerchant;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.TextBlock balText;
        
        internal System.Windows.Controls.TextBlock balanceText;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Controls.ListBox listBoxHistory;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/SqlLiteWP8;component/AccountHistory.xaml", System.UriKind.Relative));
            this.Container = ((System.Windows.Controls.Grid)(this.FindName("Container")));
            this.SettingsStateGroup = ((System.Windows.VisualStateGroup)(this.FindName("SettingsStateGroup")));
            this.SettingsClosedState = ((System.Windows.VisualState)(this.FindName("SettingsClosedState")));
            this.SettingsOpenState = ((System.Windows.VisualState)(this.FindName("SettingsOpenState")));
            this.SettingsPane = ((System.Windows.Controls.Grid)(this.FindName("SettingsPane")));
            this.btnMob = ((System.Windows.Controls.Button)(this.FindName("btnMob")));
            this.btnEnt = ((System.Windows.Controls.Button)(this.FindName("btnEnt")));
            this.btnDth = ((System.Windows.Controls.Button)(this.FindName("btnDth")));
            this.btnComplaint = ((System.Windows.Controls.Button)(this.FindName("btnComplaint")));
            this.btnBill = ((System.Windows.Controls.Button)(this.FindName("btnBill")));
            this.btnReports = ((System.Windows.Controls.Button)(this.FindName("btnReports")));
            this.L1 = ((System.Windows.Controls.ListBox)(this.FindName("L1")));
            this.TextMerchant = ((System.Windows.Controls.TextBlock)(this.FindName("TextMerchant")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.balText = ((System.Windows.Controls.TextBlock)(this.FindName("balText")));
            this.balanceText = ((System.Windows.Controls.TextBlock)(this.FindName("balanceText")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.listBoxHistory = ((System.Windows.Controls.ListBox)(this.FindName("listBoxHistory")));
        }
    }
}

