﻿#pragma checksum "D:\Pay1 Windows 02-04-2014\Old Windows App Backup\SqlLiteWP8\SqlLiteWP8\StatusRow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "6E338E58889FD3DF0CE19E77BCB18672"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace SqlLiteWP8 {
    
    
    public partial class StatusRow : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Image operatorIcon;
        
        internal System.Windows.Controls.TextBlock TextMobile;
        
        internal System.Windows.Controls.TextBlock TextDate;
        
        internal System.Windows.Controls.TextBlock TextAmt;
        
        internal System.Windows.Controls.Image statusIcon;
        
        internal System.Windows.Controls.Image ComplaintIcon;
        
        internal System.Windows.Controls.TextBlock index;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/SqlLiteWP8;component/StatusRow.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.operatorIcon = ((System.Windows.Controls.Image)(this.FindName("operatorIcon")));
            this.TextMobile = ((System.Windows.Controls.TextBlock)(this.FindName("TextMobile")));
            this.TextDate = ((System.Windows.Controls.TextBlock)(this.FindName("TextDate")));
            this.TextAmt = ((System.Windows.Controls.TextBlock)(this.FindName("TextAmt")));
            this.statusIcon = ((System.Windows.Controls.Image)(this.FindName("statusIcon")));
            this.ComplaintIcon = ((System.Windows.Controls.Image)(this.FindName("ComplaintIcon")));
            this.index = ((System.Windows.Controls.TextBlock)(this.FindName("index")));
        }
    }
}

