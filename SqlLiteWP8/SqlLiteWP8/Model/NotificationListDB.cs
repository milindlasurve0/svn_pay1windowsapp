﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlLiteWP8.Model
{
    class NotificationListDB
    {
        [PrimaryKey, AutoIncrement]
        public int notificationID { get; set; }
        public string notificationData { get; set; }

        [Unique]
        public string notificationMobileID { get; set; }
        public string notificationType { get; set; }
        public string notificationTime { get; set; }
    }
}
