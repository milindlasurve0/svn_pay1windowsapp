﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlLiteWP8.Model
{
    class ChatData
    {
        [PrimaryKey, AutoIncrement]
        public int chatId { get; set; }

        public string chatBody { get; set; }
        public string chatTime { get; set; }
        public bool chatPosition { get; set; }
        
    }
}
