﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlLiteWP8.Model
{
    class LocationData
    {
        [PrimaryKey, AutoIncrement]
        public int locId { get; set; }

        public string address { get; set; }
        public string city { get; set; }
        public string area { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string lattitude { get; set; }
        public string longitude { get; set; }
      
    }
}
