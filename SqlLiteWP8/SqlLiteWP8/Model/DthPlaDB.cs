﻿using SQLite;
using System;

namespace SqlLiteWP8.Model
{
    public class DthPlanDB
    {

        [PrimaryKey, AutoIncrement]
        public int dthPlanID { get; set; }

        public int dthPlanAmount { get; set; }
        public string dthPlanOpertorID { get; set; }
        public string dthPlanOperatorName { get; set; }
        public string dthPlanCircleID { get; set; }
        public string dthPlanCircleName { get; set; }
        public string dthPlanType { get; set; }
        public string dthPlanValidity { get; set; }
        public string dthPlanDescription { get; set; }
        public string dthPlanUptateTime { get; set; }

    }
}
