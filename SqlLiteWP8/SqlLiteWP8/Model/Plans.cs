﻿using SQLite;
using System;

namespace SqlLiteWP8.Model
{
    public class Plans
    {
       
        [PrimaryKey, AutoIncrement]
        public int planID { get; set; }

        public int planAmount { get; set; }
        public string planOpertorID { get; set; }
        public string planOperatorName { get; set; }
        public string planCircleID { get; set; }
        public string planCircleName { get; set; }
        public string planType { get; set; }
        public string planValidity { get; set; }
        public string planDescription { get; set; }
        public DateTime planUptateTime { get; set; }

    }
}
