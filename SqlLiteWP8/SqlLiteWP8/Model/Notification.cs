﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlLiteWP8.Model
{
    class Notification
    {
        [PrimaryKey, AutoIncrement]
        public string notificationID { get; set; }
        public string notificationData { get; set; }

        public string notificationType { get; set; }
        public string notificationTime { get; set; }
    }
}
