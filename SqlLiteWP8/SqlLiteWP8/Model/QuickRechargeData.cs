﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlLiteWP8.Model
{
    class QuickRechargeData
    {
        [PrimaryKey, AutoIncrement]
        public int op_code { get; set; }

        public string op_count { get; set; }
        public string rec_type { get; set; }
        public string op_id { get; set; }
        public string op_name { get; set; }
        public string op_imageUrl { get; set; }

    }
}
