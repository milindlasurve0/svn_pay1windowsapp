﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SQLite;
using SqlLiteWP8.Model;

namespace PhoneApp1
{
    public partial class MyUserControl : UserControl
    {
        public event EventHandler EventForPageNavigation;
        SQLiteAsyncConnection conn;
        public MyUserControl()
        {
            InitializeComponent();
            CreateDatabase();
        }

        private async void CreateDatabase()
        {
            conn = new SQLiteAsyncConnection("plans");
            await conn.CreateTableAsync<QuickRechargeData>();


        }
        private void btnNavigate_Click(object sender, System.Windows.RoutedEventArgs e)
        {
           // MethodToNavigateToPage(new Uri("/MainPage.xaml?", UriKind.Relative));
            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/NewQuickRecharge.xaml?op_id=1", UriKind.Relative));
        }
    }
}
