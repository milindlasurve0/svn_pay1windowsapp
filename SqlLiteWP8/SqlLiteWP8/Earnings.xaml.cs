﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class Earnings : PhoneApplicationPage
    {
        bool isNetOrSms;

        string NextWeekDate, PreWeekDate, crwk;
        public Earnings()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);

            try
            {
                isNetOrSms = Constatnt.LoadPersistent<Boolean>("isNetOrSms");
            }
            catch (Exception e4)
            {
                Console.WriteLine(e4.Message);
            }
            // Constatnt.loadSideBar(L1,balanceText);
            if (isNetOrSms)
            {
                TextMerchant.Text = Constatnt.SHOPNAME;
                Task<string> abc = MakeWebRequest("");


            }
            else
            {
            }
            
        }

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        public async Task<string> MakeWebRequest(String date)
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "earnings"),
                        new KeyValuePair<string, string>("date",date),
                        
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);




            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            string status = dict["status"].ToString();
            if (status.Equals("success"))
            {
                ListEarn.Items.Clear();
                var desnInner = desc[0];
                var prevWeek = dict["prevWeek"];
                var nextWeek = dict["nextWeek"];
                var currWeek = dict["currWeek"];

                var dat1 = (JArray)JsonConvert.DeserializeObject(prevWeek.ToString());
                PreWeekDate = dat1[0].ToString();

                var dat2 = (JArray)JsonConvert.DeserializeObject(nextWeek.ToString());
                NextWeekDate = dat2[0].ToString();
                var dat3 = (JArray)JsonConvert.DeserializeObject(currWeek.ToString());
                crwk = dat3[0].ToString();
                TextCurrentWeek.Text = crwk;
                int i = 0;
                foreach (var item in desnInner)
                {
                    var descSubInner = item[0];
                    // var descIn=descSubInner[0];
                    var amount = descSubInner["amount"];
                    var income = descSubInner["income"];
                    var earnDate = descSubInner["date"];


                    EarningRow earningRow = new EarningRow();
                    if (i % 2 == 0)
                    {
                        earningRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                    }
                    else
                    {
                        earningRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                    }

                    decimal d = 0, d1 = 0;
                    try
                    {
                        d = Decimal.Parse(income.ToString());
                        d1 = Decimal.Parse(amount.ToString());
                    }
                    catch (Exception e)
                    {
                    }
                    earningRow.Earning.Text = d.ToString("0.00");
                    earningRow.EarningDate.Text = earnDate.ToString();
                    earningRow.EarningSale.Text = d1.ToString("0.00");

                    ListEarn.Items.Add(earningRow);
                    i++;
                }

            }
            else
            {
                MessageBox.Show("Data Not Recieved");
            }

            Constatnt.SetProgressIndicator(false);

            return responseString;

        }


        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void Button_Next_Click(object sender, RoutedEventArgs e)
        {

            if (NextWeekDate.Equals(""))
            {
                MessageBox.Show("Can not proceed further");
            }
            else
            {
                Task<string> abc = MakeWebRequest(NextWeekDate);
            }
        }

        private void Button_Pre_Click(object sender, RoutedEventArgs e)
        {

            if (PreWeekDate.Equals(""))
            {
                MessageBox.Show("Can not proceed further");
            }
            else
            {

                Task<string> abc = MakeWebRequest(PreWeekDate);
            }
        }

        private void ListEarn_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            EarningRow row = ListEarn.SelectedItem as EarningRow;
            string dateEarn = row.EarningDate.Text;
            NavigationService.Navigate(new Uri("/EarningDetails.xaml?date=" + dateEarn, UriKind.Relative));
        }
    }
}