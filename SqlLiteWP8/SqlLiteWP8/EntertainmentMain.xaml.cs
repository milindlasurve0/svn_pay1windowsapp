﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.Windows.Controls.Primitives;

namespace SqlLiteWP8
{
    public partial class EntertainmentMain : PhoneApplicationPage
    {
        public static string shortDesc, longDesc, ImageSouce;
        public static EnterList enter = new EnterList();
        string prodsDetails;
        Popup p;
        List<string> prodList = new List<string>();
        public static string selectedItem;
        public EntertainmentMain()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            
            Task<string> abc = MakeWebRequest();
            Task<string> statusRequest = MakeWebStatusRequest("");
            Task<string> request = MakeWebRequestRequest();
        }

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        public async Task<string> MakeWebRequest()
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";
            EntMainList.Items.Clear();
            var baseAddress = new Uri(Constatnt.url);
            Button bt = new Button();
            bt.Content = "Add More";
            bt.Foreground = new SolidColorBrush(Colors.Red);
            //listBox1.Items.Remove(bt);
            //EntStatusList.Items.RemoveAt(EntStatusList.Items.Count);// (bt);
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "getVASProducts"),
                       
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            var item1 = desc[0];
            var R = item1["R"];
            int i = 0;
            foreach (var item in desc)
            {

                var prods = item["prods"];
                var name = prods["name"];
                prodsDetails = prods.ToString();
                var shortDesc = prods["shortDesc"];
                var longDesc = prods["longDesc"];
                var validity = prods["validity"];
                var price = prods["price"];
                var image = prods["image"];
                var prodcut = prods["params"];
                prodList.Add(prodsDetails);


                EntRows rows = new EntRows();
                if (i % 2 == 0)
                {
                    rows.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                }
                else
                {
                    rows.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                }

                rows.prodDetailBtn.Click += new RoutedEventHandler(AddButton_Click);


                rows.prodDetailBtn.Name = i + "";

                rows.TextProduct.Text = name.ToString();
                EntMainList.Items.Add(rows);// = enter;
                /* EntertainmentControl entctrl = new EntertainmentControl();
                 entctrl.ProductText.Text = name.ToString();
                 EntMainList.Items.Add(entctrl);// = enter;
                 /*  enter.Add(new Enter { EntName = name.ToString(), ShortDesc = shortDesc.ToString(), LongDesc = longDesc.ToString(), TagValue = i.ToString(), Validity = validity.ToString(), Price = price.ToString(), Image = image.ToString() });
                   // EntertainmentControl discRow = new EntertainmentControl();
                   // discRow.Entertain = enter[i];
                   //  discRow.ProductText.Text = name.ToString();
                   //discRow.DiscountRate.Text = prodPercent.ToString();

                   // ListEnter.Items.Add(discRow);
                   i++;

                   //this.Entertain.Tag = i;//new Enter { EntName = name.ToString() ,ShortDesc=shortDesc.ToString(),LongDesc=longDesc.ToString(),Validity=validity.ToString(),Price=price.ToString(),Image=image.ToString()};
               }*/
                i++;
            }
            Constatnt.SetProgressIndicator(false);
            return responseString;

        }


        private void panel1_MouseLeave(object sender, System.EventArgs e)
        {
            Image button = (Image)sender;

            string rowIndex = button.Name;


            int index = Convert.ToInt32(rowIndex);
            StatusRow row = (StatusRow)EntReqestList.Items[index - 1];
            string complaintId = row.index.Text;
            MessageBox.Show("Are you sure you want to complaint for " + row.TextMobile.Text + "  number.");
            Task<string> complaint = MakeWebRequestForComplaint(complaintId, index);


        }

        public async Task<string> MakeWebRequestForComplaint(string complaintId, int index)
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "reversal"),
                        //new KeyValuePair<string, string>("date","2014-02-10"),
                        new KeyValuePair<string, string>("id", complaintId),
                        
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);







            var dict = (JObject)JsonConvert.DeserializeObject(output4);
           
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            string status1 = dict["status"].ToString();

            if (status1.Equals("success"))
            {
                var desc = dict["description"];
              /*  MessageBox.Show(desc.ToString());
                StatusRow row1 = new StatusRow();
                // button.Source = new BitmapImage(Constatnt.getStatusIcon("4"));
                StatusRow row = (StatusRow)EntReqestList.Items[index];
                row.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon("4"));
                row.statusIcon.Visibility = Visibility.Collapsed;
                EntReqestList.UpdateLayout();
                */
                MessageBox.Show("Your complaint registered succesfully.");
                StatusRow row1 = new StatusRow();
                // button.Source = new BitmapImage(Constatnt.getStatusIcon("4"));
                StatusRow row = (StatusRow)EntReqestList.Items[index - 1];
                row.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon("4"));
                row.statusIcon.Visibility = Visibility.Collapsed;
                EntReqestList.UpdateLayout();
            }
            else
            {
                var desc = dict["description"];
                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=EntertainmentMain", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }


            Constatnt.SetProgressIndicator(false);
            return responseString;

        }





        private void EntMainList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            prodsDetails = prodList[EntMainList.SelectedIndex].ToString();
            EntRows data = (sender as ListBox).SelectedItem as EntRows;
            string productName = data.TextProduct.Text;
            selectedItem = prodsDetails;
            NavigationService.Navigate(new Uri("/EnterRecharge.xaml?product="+productName, UriKind.Relative));
            /*  var clickedUIElement = sender as ListBox;
              if (null == clickedUIElement) { return; }
              EntRows selectedItemData = clickedUIElement.DataContext as EntRows;
              selectedItemData.prodDetailBtn.Click +=new RoutedEventHandler(AddButton_Click);*/

        }


        void AddButton_Click(object sender, RoutedEventArgs e)
        {
            LayoutRoot.IsHitTestVisible = false;
            
            LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#000000"));
            var button = (Button)sender;
            EntMainList.SelectedIndex = 0;
            string nam = button.Name;

            // int.TryParse(nam, out int value);
            prodsDetails = prodList[int.Parse(nam)].ToString();

            var dict = (JObject)JsonConvert.DeserializeObject(prodsDetails);
            shortDesc = dict["shortDesc"].ToString();
            longDesc = dict["longDesc"].ToString();
            //ImageSouce=
            //MessageBoxResult m = MessageBox.Show(shortDesc, longDesc, MessageBoxButton.OKCancel);
            // NavigationService.Navigate(new Uri("/EntProduct.xaml?", UriKind.Relative));
            if (int.Parse(nam) == 0)
            {
                ImageSouce = "Images/e_love.png";
            }
            else if (int.Parse(nam) == 1)
            {
                ImageSouce = "Images/e_fun.png";
            }
            else if (int.Parse(nam) == 2)
            {
                ImageSouce = "Images/e_ditto.png";
            }

            p = new Popup();
            var content = new EntProduct();//TextBox { Text = "hello world!" };
            p.Child = content;
            p.VerticalOffset = 200;
            p.HorizontalOffset = 50;
            p.IsOpen = true;
            content.OkBtn.Click += new RoutedEventHandler(ClosePopupClick);
            //EntRows lstItem = (EntRows)EntMainList.Items[1];
            //var company = ((EntRows)button.DataContext).Tag;

            /* var clickedUIElement = sender as Button;
             if (null == clickedUIElement) { return; }

             EntRows selectedItemData = EntMainList.DataContext as EntRows;
            // string tit = company.TextProduct.Text;
             MessageBox.Show("iovyhuio    " + EntMainList.SelectedIndex);
           //  EntRows rows = new EntRows();
            // var i=rows.prodDetailBtn.GetValue;
            // MessageBox.Show("DSFhsdh");
           /*  var clickedUIElement = sender as Button;
             if (null == clickedUIElement) { return; }
             EntRows selectedItemData = clickedUIElement.DataContext as EntRows;
             if (null != selectedItemData)
             {
                 MessageBox.Show(""+selectedItemData.TextProduct.Text);
                 //NavigationService.Navigate("/page3.xaml", selectedItemData);
             }
            /* Button button = sender as Button;
             EntRows dataObject = button.SelectedItem as EntRows;*/
            // List<EntRows> myDataObjects = (List<EntRows>)EntMainList.ItemsSource;
            // get the index
            // int index = myDataObjects.IndexOf(dataObject);
            //MessageBox.Show(dataObject.TextProduct.Text);
        }

        private void ClosePopupClick(object sender, RoutedEventArgs e)
        {
            LayoutRoot.IsHitTestVisible = true;
            if (this.p.IsOpen)
            {
                this.p.IsOpen = false;
                //e.Cancel = true;
            }

        }


        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }



        private void DatePicker_ValueChanged(
                 object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            EntStatusList.Items.Clear();
            Task<string> abc3 = MakeWebStatusRequest(year + "-" + month + "-" + day);
            //string day=
            //MessageBox.Show(da.ToString());
        }

        private void btnClick(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            int i = int.Parse(btn.Tag.ToString());
            MessageBox.Show(enter[i].EntName + "\n" + enter[i].ShortDesc + "\n" + enter[i].LongDesc + "\n" + enter[i].Validity + "\n" + enter[i].Price + "\n");

        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void MainListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //
            prodsDetails = prodList[EntMainList.SelectedIndex].ToString();
            selectedItem = prodsDetails;
            NavigationService.Navigate(new Uri("/EnterRecharge.xaml?", UriKind.Relative));
        }


        public async Task<string> MakeWebStatusRequest(string date)
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";
            Button bt = new Button();
            bt.Content = "Add More";
            bt.Foreground = new SolidColorBrush(Colors.Red);
            // listBox1.Items.Remove(bt);
            if (EntStatusList.Items.Count > 0)
            EntStatusList.Items.RemoveAt(EntStatusList.Items.Count-1);// (bt);
            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "reversalTransactions"),
                        new KeyValuePair<string, string>("date",date),
                        new KeyValuePair<string, string>("service", "3"),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);







            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];

            int i = 0;
            foreach (var item in desc)
            {
                foreach (var innerItem in item)
                {
                    var timestamp = innerItem["0"];
                    var time = timestamp["timestamp"];

                    var products = innerItem["products"];
                    var name = products["name"];
                    var vendors_activations = innerItem["vendors_activations"];
                    var product_id = vendors_activations["product_id"];
                    var mobile = vendors_activations["mobile"];
                    var amount = vendors_activations["amount"];
                    var status = vendors_activations["status"];
                    i++;
                    Uri ImageUrl = null;


                    StatusRow statusRow = new StatusRow();
                    statusRow.TextMobile.Text = mobile.ToString();
                    statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                    statusRow.TextDate.Text = time.ToString();
                    if (i % 2 == 0)
                    {
                        statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE")); 
                    }
                    else
                    {
                        statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                    }

                    /*if (status.ToString() == "1" || status.ToString() == "0")
                    {
                        statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                    }
                    else
                    {
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                    }*/

                    if (status.ToString() == "1" || status.ToString() == "0")
                    {
                        statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        statusRow.statusIcon.MouseLeave += new MouseEventHandler(panel1_MouseLeave);
                        //statusRow.statusIcon.MouseLeftButtonDown += new MouseButtonEventHandler(panel1_MouseLeave);
                        //statusRow.statusIcon.Name = id.ToString();
                        // statusRow.statusIcon.MouseLeave += new System.EventHandler(this.panel1_MouseLeave);
                    }
                    else
                    {
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                    }


                    statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                    EntStatusList.Items.Add(statusRow);
                }

            }


            // EntStatusList.ItemsSource = mov;
            //listBox2.ItemsSource = mov;

            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output2);

            if (EntStatusList.Items.Count % 10 == 0 && EntStatusList.Items.Count != 0)
            {

                bt.Click += (s, e) =>
                {
                    Task<string> abc3 = MakeWebStatusRequest(date);
                };
                EntStatusList.Items.Add(bt);
            }
            else
            {
            }
            if (EntStatusList.Items.Count == 0)
            {
                noDataStatusGrid.Visibility = Visibility.Visible;
            }
            Constatnt.SetProgressIndicator(false);
            return responseString;

        }

        public async Task<string> MakeWebRequestRequest()
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "lastten"),
                        //new KeyValuePair<string, string>("date","2014-02-10"),
                        new KeyValuePair<string, string>("service", "3"),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);







            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            int i = 0;
            foreach (var item in desc)
            {
                foreach (var innerItem in item)
                {
                    var timestamp = innerItem["0"];
                    var time = timestamp["timestamp"];

                    var products = innerItem["products"];
                    var name = products["name"];
                    var vendors_activations = innerItem["vendors_activations"];
                    var product_id = vendors_activations["product_id"];
                    var mobile = vendors_activations["mobile"];
                    var amount = vendors_activations["amount"];
                    var status = vendors_activations["status"];
                    var id = vendors_activations["id"];
                    i++;


                    StatusRow statusRow = new StatusRow();
                    statusRow.TextMobile.Text = mobile.ToString();
                    statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                    statusRow.TextDate.Text = time.ToString();
                    statusRow.index.Text = id.ToString();

                    if (i % 2 == 0)
                    {
                        statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                    }
                    else
                    {
                        statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                    }
                    if (status.ToString() == "1" || status.ToString() == "0")
                    {
                        statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        statusRow.statusIcon.MouseLeave += new MouseEventHandler(panel1_MouseLeave);
                        //statusRow.statusIcon.MouseLeftButtonDown += new MouseButtonEventHandler(panel1_MouseLeave);
                        statusRow.statusIcon.Name = i + "";// d.ToString();
                        statusRow.statusIcon.Tag = i;
                    }
                    else
                    {
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                    }
                    statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                    EntReqestList.Items.Add(statusRow);
                }

            }


            // listBox1.ItemsSource = mov;
            //EntReqestList.ItemsSource = mov;

            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output2);

            if (EntReqestList.Items.Count == 0)
            {
                noDataRequestGrid.Visibility = Visibility.Visible;
            }
            Constatnt.SetProgressIndicator(false);
            return responseString;

        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string searchNumber = searchText.Text;

            if (searchNumber.Trim().Length == 10)
            {
                Task<string> abc = MakeWebRequestSearch(searchNumber);
            }
            else
            {
                MessageBox.Show("Please enter correct Mobile number.");
            }

        }

        private async Task<string> MakeWebRequestSearch(string searchNumber)
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";
            EntReqestList.Items.Clear();

            var baseAddress = new Uri(Constatnt.url);
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "mobileTransactions"),
                        new KeyValuePair<string, string>("mobile",searchNumber),
                        new KeyValuePair<string, string>("service", "3"),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);







            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status1 = dict["status"].ToString();
            var desc = dict["description"];
            if (status1.Equals("success"))
            {

                // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
                int i = 0;
                foreach (var item in desc)
                {
                    foreach (var innerItem in item)
                    {
                        var timestamp = innerItem["0"];
                        var time = timestamp["timestamp"];

                        var products = innerItem["products"];
                        var name = products["name"];
                        var vendors_activations = innerItem["vendors_activations"];
                        var product_id = vendors_activations["product_id"];
                        var mobile = vendors_activations["mobile"];
                        var amount = vendors_activations["amount"];
                        var status = vendors_activations["status"];
                        i++;
                        StatusRow statusRow = new StatusRow();
                        statusRow.TextMobile.Text = mobile.ToString();
                        statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                        statusRow.TextDate.Text = time.ToString();
                        if (i % 2 == 0)
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE")); 
                        }
                        else
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                        }
                        if (status.ToString() == "1" || status.ToString() == "0")
                        {
                            statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        else
                        {
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                        EntReqestList.Items.Add(statusRow);


                    }

                }
            }
            else
            {
                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=EntertainmentMain", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }
            Constatnt.SetProgressIndicator(false);
            return responseString;

        }











    }

    public class EnterList : List<Enter>
    {
        public EnterList()
        {


        }
    }

    public class Enter
    {
        public string EntName { get; set; }
        public string ShortDesc { get; set; }
        public string LongDesc { get; set; }
        public string Validity { get; set; }
        public string Price { get; set; }
        public string Image { get; set; }
        public string TagValue { get; set; }

    }

}