﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SqlLiteWP8.Resources;
using SqlLiteWP8.ViewModels;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Tasks;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace SqlLiteWP8
{

    public partial class QRCH : PhoneApplicationPage
    {
        bool isNetOrSms = true;
        int selectedService;
        List<ImageData> dataSource;
        // Constructor
        public QRCH()
        {

            InitializeComponent();
            try
            {
                isNetOrSms = (Boolean)IsolatedStorageSettings.ApplicationSettings["isNetOrSms"];
            }
            catch (Exception e4)
            {
                Console.WriteLine(e4.Message);
            }

            dataSource = new List<ImageData>() 
    { 
    new ImageData(){Name = "Idea", ImagePath="/Images/m_idea.png",productId=Constatnt.ID_OPERATOR_IDEA,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "Airtel", ImagePath="/Images/m_airtel.png",productId=Constatnt.ID_OPERATOR_AIRTEL,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "Aircel", ImagePath="/Images/m_aircel.png",productId=Constatnt.ID_OPERATOR_AIRCEL,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "BSNL", ImagePath="/Images/m_bsnl.png",productId=Constatnt.ID_OPERATOR_BSNL,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "Loop", ImagePath="/Images/m_loop.png",productId=Constatnt.ID_OPERATOR_LOOP,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "Uninor", ImagePath="/Images/m_uninor.png",productId=Constatnt.ID_OPERATOR_UNINOR,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "Reliance CDMA", ImagePath="/Images/m_reliance_cdma.png",productId=Constatnt.ID_OPERATOR_RELIANCE_CDMA,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "Reliance GSM", ImagePath="/Images/m_reliance.png",productId=Constatnt.ID_OPERATOR_RELIANCE_GSM,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "Docomo", ImagePath="/Images/m_docomo.png",productId=Constatnt.ID_OPERATOR_DOCOMO,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "Tata Indicom", ImagePath="/Images/m_indicom.png",productId=Constatnt.ID_OPERATOR_TATA_INDICOM,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "MTS", ImagePath="/Images/m_mts.png",productId=Constatnt.ID_OPERATOR_MTS,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "MTNL", ImagePath="/Images/m_mtnl.png",productId=Constatnt.ID_OPERATOR_MTNL,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "Vodafone", ImagePath="/Images/m_vodafone.png",productId=Constatnt.ID_OPERATOR_VODAFONE,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "Videocon", ImagePath="/Images/m_videocon.png",productId=Constatnt.ID_OPERATOR_VIDEOCON,DthOrMobile=Constatnt.RECHARGE_MOBILE},
    new ImageData(){Name = "Dish TV", ImagePath="/Images/d_dishtv.png",productId=Constatnt.ID_OPERATOR_DTH_DISHTV,DthOrMobile=Constatnt.RECHARGE_DTH},
    new ImageData(){Name = "Airtel Digitel", ImagePath="/Images/m_airtel.png",productId=Constatnt.ID_OPERATOR_DTH_AIRTEL,DthOrMobile=Constatnt.RECHARGE_DTH},
    new ImageData(){Name = "Videocon", ImagePath="/Images/d_videocon.png",productId=Constatnt.ID_OPERATOR_DTH_VIDEOCON,DthOrMobile=Constatnt.RECHARGE_DTH},
    new ImageData(){Name = "Sun TV", ImagePath="/Images/d_sundirect.png",productId=Constatnt.ID_OPERATOR_DTH_SUN,DthOrMobile=Constatnt.RECHARGE_DTH},
    new ImageData(){Name = "Big TV", ImagePath="/Images/d_bigtv.png",productId=Constatnt.ID_OPERATOR_DTH_BIG,DthOrMobile=Constatnt.RECHARGE_DTH},
   
    };

            this.MainLongLiefstSelector.ItemsSource = dataSource;
            // Set the data context of the LongListSelector control to the sample data
            DataContext = App.ViewModel;

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }



        private bool _isSettingsOpen = false;
        private void ApplicationBarIconButton_OnClick(object sender, EventArgs e)
        {
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }

        private void Home_Click(object sender, RoutedEventArgs e)
        {
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }


        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void MainLongLiefstSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            var selected = MainLongLiefstSelector.SelectedValue as ImageData;
            string se = selected.productId;
            rchCode.Text = "*" + selected.productId + "*";
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }

        }

        private void Quick_Recharge_Click(object sender, RoutedEventArgs e)
        {
            if (isNetOrSms)
            {
                string[] result = rchCode.Text.Split('*');
                int len = result.Count();
                string flLeg = result[1];

                if (len == 4 && result[2].Length == 10)
                {
                    MakeWebRequestForStatus(rchCode.Text);
                }
                else if (len == 5 && result[2].Length <= 12 && result[3].Length == 10)
                {
                    MakeWebRequestForStatus(rchCode.Text);
                }
                else
                {
                    MessageBox.Show("Please enter correct code");
                }

            }
            else
            {
                SmsComposeTask smsComposeTask = new SmsComposeTask();
                smsComposeTask.To = "09223178889";
                smsComposeTask.Body = rchCode.Text;

                smsComposeTask.Show();

            }
        }


        public async Task<string> MakeWebRequestForStatus(String recCode)
        {

            string[] result = recCode.Split('*');
            int len = result.Count();
            string method = "";
            string special = "";

            if (recCode.Contains("#"))
            {
                special = "1";
            }
            else
            {
                special = "0";
            }
            string id;
            string mobNo;
            string amount;
            string subId;


            string flLeg = result[1];
            if (len == 4)
            {
                id = result[1];
                mobNo = result[2];
                if (special == "1")
                {
                    amount = result[3].Remove(result[3].Length - 1);
                }
                else
                {
                    amount = result[3];
                }
                subId = result[2];
                method = "mobRecharge";
            }
            else if (len == 5)
            {
                id = result[1];
                mobNo = result[2];
                amount = result[4];
                subId = result[3];
                method = "dthRecharge";
            }
            else
            {
                method = "vasRecharge";
            }
            string responseString = "";
            return responseString;
            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
       new KeyValuePair<string, string>("method", method),
                        new KeyValuePair<string, string>("mobileNumber", mobNo),
                        new KeyValuePair<string, string>("operator", id),
                        new KeyValuePair<string, string>("subId", subId),
                        new KeyValuePair<string, string>("amount", amount),
                        new KeyValuePair<string, string>("type", "flexi"),
                        new KeyValuePair<string, string>("circle", ""),
                        new KeyValuePair<string, string>("special", special),
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    return responseString;
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

            RevStatusList mov = new RevStatusList();

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status = dict["status"].ToString();
            if (status.Equals("success"))
            {
                MessageBox.Show("Recharge Request Sent Successfully");
            }
            else if (status.Equals("failure"))
            {
                MessageBox.Show(dict["description"].ToString());
            }
            return responseString;
        }


    }

    public class ImageData
    {
        public string ImagePath { get; set; }
        public string productId { get; set; }
        public string Name { get; set; }
        public int DthOrMobile { get; set; }
    }


}