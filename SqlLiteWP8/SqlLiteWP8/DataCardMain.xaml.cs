﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;

namespace SqlLiteWP8
{
    public partial class DataCardMain : PhoneApplicationPage
    {
        public DataCardMain()
        {
            InitializeComponent();



            Task<string> abc = MakeWebRequestForStatus();
            Task<string> abc1 = MakeWebRequest();
        }
        /*private void mob00(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MobileMain.xaml", UriKind.Relative));


        }*/

        private void mobAirtel(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_AIRTEL, UriKind.Relative));
        }

        private void mobAircel(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_AIRCEL, UriKind.Relative));
        }

        private void mobBSNL(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_BSNL, UriKind.Relative));
        }

        private void mobIdea(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_IDEA, UriKind.Relative));
        }

        private void mobLoop(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_LOOP, UriKind.Relative));
        }

        private void mobMTNL(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_MTNL, UriKind.Relative));
        }

        private void mobMTS(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_MTS, UriKind.Relative));
        }

        private void mobVideocon(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_VIDEOCON, UriKind.Relative));
        }

        private void mobIndicom(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_TATA_INDICOM, UriKind.Relative));
        }

        private void mobDOCOMO(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_DOCOMO, UriKind.Relative));
        }


        private void mobUninor(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_UNINOR, UriKind.Relative));
        }

        private void mobRelianceCDMA(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_RELIANCE_CDMA, UriKind.Relative));
        }


        private void mobRelianceGSM(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_RELIANCE_GSM, UriKind.Relative));
        }

        private void mobVodafone(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataCardRecharge.xaml?id=" + Constatnt.ID_OPERATOR_VODAFONE, UriKind.Relative));
        }


        private void Home_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Home_Click");
        }


        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }





        public async Task<string> MakeWebRequest()
        {

            string responseString = "";

            var baseAddress = new Uri("http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?");

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "reversalTransactions"),
                        new KeyValuePair<string, string>("date","2014-02-10"),
                        new KeyValuePair<string, string>("service", "1"),
                        new KeyValuePair<string, string>("device_type", "")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


            StatusMobileList mov = new StatusMobileList();




            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            int i = 0;
            foreach (var item in desc)
            {
                foreach (var innerItem in item)
                {
                    var timestamp = innerItem["0"];
                    var time = timestamp["timestamp"];

                    var products = innerItem["products"];
                    var name = products["name"];
                    var vendors_activations = innerItem["vendors_activations"];
                    var product_id = vendors_activations["product_id"];
                    var mobile = vendors_activations["mobile"];
                    var amount = vendors_activations["amount"];
                    var status = vendors_activations["status"];
                    i++;
                    Uri ImageUrl = null;

                    if (product_id.ToString() == Constatnt.ID_OPERATOR_AIRTEL)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);


                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_TATA_SKY)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);

                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_SUN)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);

                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_DISHTV)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);
                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_AIRTEL)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);
                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_BIG)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);
                    }

                    string color = "";

                    if (i % 2 == 0)
                    {
                        var color1 = new Color() { R = 0xE2, G = 0xF1, B = 0xDE };
                        var brush = new SolidColorBrush(color1);
                        color = brush.ToString();
                        color = "Blue";

                    }
                    else
                    {
                        color = "Red";
                        //lbi1.Foreground = new SolidColorBrush(Color.FromArgb(255, 45, 23, 45));
                    }
                    mov.Add(new StatusMobile { Mobile = mobile.ToString(), RechargeStatus = status.ToString(), Amount = amount.ToString(), Time = time.ToString(), color = color, ImageUrl = ImageUrl });


                    //mov.Add(new StatusMobile { Mobile = mobile.ToString(), RechargeStatus = status.ToString(), Amount = amount.ToString(), Time = time.ToString(), color = color, ImageUrl = ImageUrl });
                }

            }


            listBox1.ItemsSource = mov;
            //listBox2.ItemsSource = mov;

            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output2);



            return responseString;

        }



        public async Task<string> MakeWebRequestForStatus()
        {

            string responseString = "";

            var baseAddress = new Uri("http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?");

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "lastten"),
                        //new KeyValuePair<string, string>("date","2014-02-10"),
                        new KeyValuePair<string, string>("service", "1"),
                        new KeyValuePair<string, string>("device_type", "")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


            StatusMobileList mov = new StatusMobileList();




            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            int i = 0;
            foreach (var item in desc)
            {
                foreach (var innerItem in item)
                {
                    var timestamp = innerItem["0"];
                    var time = timestamp["timestamp"];

                    var products = innerItem["products"];
                    var name = products["name"];
                    var vendors_activations = innerItem["vendors_activations"];
                    var product_id = vendors_activations["product_id"];
                    var mobile = vendors_activations["mobile"];
                    var amount = vendors_activations["amount"];
                    var status = vendors_activations["status"];
                    i++;
                    Uri ImageUrl = null;

                    if (product_id.ToString() == Constatnt.ID_OPERATOR_AIRTEL)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);


                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_TATA_SKY)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);

                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_SUN)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);

                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_DISHTV)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);
                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_AIRTEL)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);
                    }
                    else if (product_id.ToString() == Constatnt.ID_OPERATOR_DTH_BIG)
                    {
                        ImageUrl = new Uri(@"Images/m_airtel.png", UriKind.Relative);
                    }

                    string color = "";

                    if (i % 2 == 0)
                    {
                        var color1 = new Color() { R = 0xE2, G = 0xF1, B = 0xDE };
                        var brush = new SolidColorBrush(color1);
                        color = brush.ToString();
                        color = "Blue";

                    }
                    else
                    {
                        color = "Red";
                        //lbi1.Foreground = new SolidColorBrush(Color.FromArgb(255, 45, 23, 45));
                    }
                    mov.Add(new StatusMobile { MobileReq = mobile.ToString(), RechargeStatusReq = status.ToString(), AmountReq = amount.ToString(), TimeReq = time.ToString(), color = color, ImageUrl = ImageUrl });


                    //mov.Add(new StatusMobile { Mobile = mobile.ToString(), RechargeStatus = status.ToString(), Amount = amount.ToString(), Time = time.ToString(), color = color, ImageUrl = ImageUrl });
                }

            }


            // listBox1.ItemsSource = mov;
            listBox2.ItemsSource = mov;

            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output2);



            return responseString;

        }


        public class StatusMobileList : List<StatusMobile>
        {
            public StatusMobileList()
            {


            }
        }

        public class StatusMobile
        {
            public string Time { get; set; }
            public string Amount { get; set; }
            public string RechargeStatus { get; set; }
            public string Mobile { get; set; }
            public string color { get; set; }
            public Uri ImageUrl { get; set; }


            public string TimeReq { get; set; }
            public string AmountReq { get; set; }
            public string RechargeStatusReq { get; set; }
            public string MobileReq { get; set; }

        }



    }
}

