﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

namespace SqlLiteWP8
{
    public partial class Pay1_Wallet : PhoneApplicationPage
    {
        bool isNetOrSms;

        public Pay1_Wallet()
        {

            InitializeComponent();

            Constatnt.SetProgressIndicator(true);
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);

            try
            {
                isNetOrSms = Constatnt.LoadPersistent<Boolean>("isNetOrSms");
            }
            catch (Exception e4)
            {
                Console.WriteLine(e4.Message);
            }
            // Constatnt.loadSideBar(L1,balanceText);
            if (isNetOrSms)
            {
                TextMerchant.Text = Constatnt.SHOPNAME;
                Task<string> abc = MakeWebRequestForStatus();
                Task<string> abc1 = MakeWebRequest("");


            }
            else
            {
            }


        }


        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // Constatnt.loadSideBar(L1, balanceText);
        }


        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }
        

        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml?page=MobileMain", UriKind.Relative));
            //NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }


        private void panel1_MouseLeave(object sender, System.EventArgs e)
        {
            Image button = (Image)sender;

            string rowIndex = button.Name;

            Constatnt.SetProgressIndicator(true);
            int index = Convert.ToInt32(rowIndex);
            StatusRow row = (StatusRow)listBox2.Items[index - 1];
            string complaintId = row.index.Text;
            MessageBoxResult m = MessageBox.Show("Are you sure you want to complaint for " + row.TextMobile.Text + "  number.", "Complaint", MessageBoxButton.OKCancel);
            //MessageBox.Show("Are you sure you want to complaint for " + row.TextMobile.Text + "  number.");
            if (m == MessageBoxResult.OK)
            {
                Task<string> complaint = MakeWebRequestForComplaint(complaintId, index);
            }
            else
            {
                Constatnt.SetProgressIndicator(false);
            }


        }

        public async Task<string> MakeWebRequestForComplaint(string complaintId, int index)
        {

            string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "reversal"),
                        //new KeyValuePair<string, string>("date","2014-02-10"),
                        new KeyValuePair<string, string>("id", complaintId),
                        
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);







            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            string status1 = dict["status"].ToString();

            if (status1.Equals("success"))
            {
                MessageBox.Show("Your complaint registered succesfully.");
                StatusRow row1 = new StatusRow();
                // button.Source = new BitmapImage(Constatnt.getStatusIcon("4"));
                StatusRow row = (StatusRow)listBox2.Items[index - 1];
                row.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("4"));
                row.ComplaintIcon.Visibility = Visibility.Collapsed;
                listBox2.UpdateLayout();
            }
            else
            {
                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=MobileMain", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }

            Constatnt.SetProgressIndicator(false);

            return responseString;

        }







        int page = 0;
        public async Task<string> MakeWebRequest(string date)
        {
            Constatnt.SetProgressIndicator(true);
            
            string responseString = "";


            Button bt = new Button();
            bt.Content = "Add More";
            bt.Foreground = new SolidColorBrush(Colors.Red);
            // listBox1.Items.Remove(bt);
            if (listBox1.Items.Count > 0)
            listBox1.Items.RemoveAt(listBox1.Items.Count-1);// (bt);
            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "reversalTransactions"),
                        new KeyValuePair<string, string>("date",date),
                        new KeyValuePair<string, string>("service", "5"),
                        new KeyValuePair<string, string>("limit","10"),
                        new KeyValuePair<string, string>("page", ""+page++),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });

                Cookie cookie = Constatnt.LoadPersistent<Cookie>("cookie");
                cookie.Name = Constatnt.LoadPersistent<String>("cookieName");

                cookie.Value = Constatnt.LoadPersistent<String>("cookieValue");
                //Login.cooki = new Cookie(cookie.Name, cookie.Value);

                //Login.cooki = new Cookie(cookie.Name, cookie.Value);


                cookieContainer.Add(baseAddress, new Cookie(cookie.Name, cookie.Value));
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            string status1 = dict["status"].ToString();

            if (status1.Equals("success"))
            {
                // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
                int i = 0;
                foreach (var item in desc)
                {
                    foreach (var innerItem in item)
                    {
                        var timestamp = innerItem["0"];
                        var time = timestamp["timestamp"];

                        var products = innerItem["products"];
                        var name = products["name"];
                        var vendors_activations = innerItem["vendors_activations"];
                        var product_id = vendors_activations["product_id"];
                        var mobile = vendors_activations["mobile"];
                        var amount = vendors_activations["amount"];
                        var status = vendors_activations["status"];
                        var id = vendors_activations["id"];
                        i++;

                        StatusRow statusRow = new StatusRow();
                        statusRow.TextMobile.Text = mobile.ToString();
                        statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                        statusRow.TextDate.Text = time.ToString();
                        // statusRow.index.Text = id.ToString();

                        if (i % 2 == 0)
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                        }
                        else
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                        }
                        if (status.ToString() == "1" || status.ToString() == "0")
                        {
                            statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                            statusRow.statusIcon.MouseLeave += new MouseEventHandler(panel1_MouseLeave);
                            //statusRow.statusIcon.MouseLeftButtonDown += new MouseButtonEventHandler(panel1_MouseLeave);
                            statusRow.statusIcon.Name = i + "";// d.ToString();
                            statusRow.statusIcon.Tag = i;

                        }
                        else
                        {
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                        listBox1.Items.Add(statusRow);

                        StatusRow r = (StatusRow)listBox1.Items[0];
                        //mov.Add(new StatusMobile { Mobile = mobile.ToString(), RechargeStatus = status.ToString(), Amount = amount.ToString(), Time = time.ToString(), color = color, ImageUrl = ImageUrl });
                    }

                }
            }
            else
            {
                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=MobileMain", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }


            if (listBox1.Items.Count % 10 == 0 && listBox1.Items.Count != 0)
            {
               
                bt.Click += (s, e) =>
                {
                    Task<string> abc3 = MakeWebRequest(date);
                };
                listBox1.Items.Add(bt);
            }
            else
            {
            }

            if (listBox1.Items.Count == 0)
            {
                noDataStatusGrid.Visibility = Visibility.Visible;
            }
            Constatnt.SetProgressIndicator(false);
            return responseString;

        }



        public async Task<string> MakeWebRequestForStatus()
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";

            //var baseAddress = new Uri("http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?");
            var baseAddress = new Uri(Constatnt.url);
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "lastten"),
                        //new KeyValuePair<string, string>("date",date),
                        new KeyValuePair<string, string>("service", "5"),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status1 = dict["status"].ToString();
            var desc = dict["description"];
            if (status1.Equals("success"))
            {
                // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
                int i = 0;
                foreach (var item in desc)
                {
                    foreach (var innerItem in item)
                    {
                        var timestamp = innerItem["0"];
                        var time = timestamp["timestamp"];

                        var products = innerItem["products"];
                        var name = products["name"];
                        var vendors_activations = innerItem["vendors_activations"];
                        var product_id = vendors_activations["product_id"];
                        var mobile = vendors_activations["mobile"];
                        var amount = vendors_activations["amount"];
                        var status = vendors_activations["status"];
                        var id = vendors_activations["id"];
                        i++;
                        StatusRow statusRow = new StatusRow();
                        statusRow.TextMobile.Text = mobile.ToString();
                        statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                        statusRow.TextDate.Text = time.ToString();
                        statusRow.index.Text = id.ToString();

                        if (i % 2 == 0)
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                        }
                        else
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                        }
                        if (status.ToString() == "1" || status.ToString() == "0")
                        {
                            statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                            statusRow.statusIcon.MouseLeave += new MouseEventHandler(panel1_MouseLeave);
                            //statusRow.statusIcon.MouseLeftButtonDown += new MouseButtonEventHandler(panel1_MouseLeave);
                            statusRow.statusIcon.Name = i + "";// d.ToString();
                            statusRow.statusIcon.Tag = i;
                        }
                        else
                        {
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                        listBox2.Items.Add(statusRow);

                        /*  mov.Add(new StatusMobile { MobileReq = mobile.ToString(), RechargeStatusReq = status.ToString(), AmountReq = amount.ToString(), TimeReq = time.ToString(), color = color, ImageUrl = ImageUrl });*/


                        //mov.Add(new StatusMobile { Mobile = mobile.ToString(), RechargeStatus = status.ToString(), Amount = amount.ToString(), Time = time.ToString(), color = color, ImageUrl = ImageUrl });
                    }

                }

            }
            else
            {
                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=MobileMain", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }
            // listBox1.ItemsSource = mov;
            // listBox2.ItemsSource = mov;

            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output2);
            if (listBox2.Items.Count == 0)
            {
                noDataRequestGrid.Visibility = Visibility.Visible;
            }
            Constatnt.SetProgressIndicator(false);

            return responseString;

        }

        private void DatePicker_ValueChanged(
       object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            listBox1.Items.Clear();
            Task<string> abc3 = MakeWebRequest(year + "-" + month + "-" + day);
            //string day=
            //MessageBox.Show(da.ToString());
        }



        public async Task<string> MakeWebRequestSearch(string searchNumber)
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";


            var baseAddress = new Uri(Constatnt.url);
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "mobileTransactions"),
                        new KeyValuePair<string, string>("mobile",searchNumber),
                        new KeyValuePair<string, string>("service", "5"),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


         




            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status1 = dict["status"].ToString();
            var desc = dict["description"];
            if (status1.Equals("success"))
            {
                listBox2.Items.Clear();
                // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
                int i = 0;
                foreach (var item in desc)
                {
                    foreach (var innerItem in item)
                    {
                        var timestamp = innerItem["0"];
                        var time = timestamp["timestamp"];

                        var products = innerItem["products"];
                        var name = products["name"];
                        var vendors_activations = innerItem["vendors_activations"];
                        var product_id = vendors_activations["product_id"];
                        var mobile = vendors_activations["mobile"];
                        var amount = vendors_activations["amount"];
                        var status = vendors_activations["status"];
                        i++;
                        StatusRow statusRow = new StatusRow();
                        statusRow.TextMobile.Text = mobile.ToString();
                        statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                        statusRow.TextDate.Text = time.ToString();
                        if (i % 2 == 0)
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                        }
                        else
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                        }
                        if (status.ToString() == "1" || status.ToString() == "0")
                        {
                            statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        else
                        {
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                        listBox2.Items.Add(statusRow);

                        /*  mov.Add(new StatusMobile { MobileReq = mobile.ToString(), RechargeStatusReq = status.ToString(), AmountReq = amount.ToString(), TimeReq = time.ToString(), color = color, ImageUrl = ImageUrl });*/


                        //mov.Add(new StatusMobile { Mobile = mobile.ToString(), RechargeStatus = status.ToString(), Amount = amount.ToString(), Time = time.ToString(), color = color, ImageUrl = ImageUrl });
                    }

                }
            }
            else
            {

                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=MobileMain", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }
            if (listBox1.Items.Count == 0)
            {
                noDataRequestGrid.Visibility = Visibility.Visible;
            }
            Constatnt.SetProgressIndicator(false);
            return responseString;

        }



        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string searchNumber = searchText.Text;
            Task<string> abc = MakeWebRequestSearch(searchNumber);

        }





        

     

        public bool reset { get; set; }

        private void rechargeWallet(object sender, RoutedEventArgs e)
        {

            if (textNumber.Text.Length != 10)
            {
                MessageBox.Show("Please Enter Correct Number");
            }
            else
            {
                if (textAmount.Text.Length==0)
                {
                    MessageBox.Show("Please Enter Correct Amount");
                }
                else
                {



                    MessageBoxButton buttons = MessageBoxButton.OKCancel;
                    // Show message box
                    MessageBoxResult result = MessageBox.Show("Mobile Number: " + textNumber.Text + "\nAmount:" + textAmount.Text + "\nAre you sure you want to continue?\n", "Pay1 Wallet", buttons);
                    //  method=authenticate&mobile=9898120212&password=1234&device_id=d80c9122dfcfd25c32438e12ce1a1c9233e84ac5&type=1&device_type=java
                    if (result == MessageBoxResult.OK)
                    {

                        byte[] myDeviceID = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");

                        string DeviceIDAsString = Convert.ToBase64String(myDeviceID);
                        var values = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("method", "mobRecharge"),
                        new KeyValuePair<string, string>("mobileNumber", textNumber.Text),
                        
                        new KeyValuePair<string, string>("subId", textNumber.Text),
                        new KeyValuePair<string, string>("amount", textAmount.Text),
                        new KeyValuePair<string, string>("type", "flexi"),
                        new KeyValuePair<string, string>("circle", ""),
                        new KeyValuePair<string, string>("special", "0"),
                         //new KeyValuePair<string, string>("timestamp", Constatnt.getTime()),
                          //new KeyValuePair<string, string>("hash_code",Constatnt.CalculateSHA1(Constatnt.getDeviceId()+textNumber.Text+textAmount.Text+Constatnt.getTime()) ),
                      // new KeyValuePair<string, string>("device_type", "windows"),
		 
                    };

                       Task<string> aaa=  MakeWebRequestForWallet();
                        /*if (isNetOrSms)
                        {
                            MakeWebRequestForWallet();
                            /* string winPhoneGeekTweetsUrl = @"http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?method=mobRecharge&mobileNumber=8422057400&operator=2&subId=9637260589& amount=10&type=flexi&circle=&special=0&timestamp=1392725591312&profile_id=18&hash_code=e6b9d662626e45234c188d702a6c8ef57aa62521& device_type=android";

                            WebClient webClient = new WebClient();
                             webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webClient_DownloadStringCompleted1);
                             webClient.DownloadStringAsync(new Uri(winPhoneGeekTweetsUrl));*/
                      /*  }
                        else
                        {
                            string recharge = "*" + id + "*" + textNumber.Text + "*" + textAmount.Text;
                            SmsComposeTask smsComposeTask = new SmsComposeTask();
                            smsComposeTask.To = "09223178889";
                            smsComposeTask.Body = recharge;

                            smsComposeTask.Show();*/

//                        }

                    }
                }

            }
        }

        private void TxtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (textNumber.Text.Length == 10)
                textAmount.Focus();
        }


        public async Task<string> MakeWebRequestForWallet()
        {
            string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
       new KeyValuePair<string, string>("method", "pay1Wallet"),
                        new KeyValuePair<string, string>("mobileNumber", textNumber.Text),
                        
                        new KeyValuePair<string, string>("subId", textNumber.Text),
                        new KeyValuePair<string, string>("amount", textAmount.Text),
                        new KeyValuePair<string, string>("type", "flexi"),
                        new KeyValuePair<string, string>("circle", ""),
                        new KeyValuePair<string, string>("special", "0"),
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


            RevStatusList mov = new RevStatusList();




            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status = dict["status"].ToString();
            if (status.Equals("success"))
            {
                string bal = dict["balance"].ToString();
                bool isOk = Constatnt.SavePersistent(Constatnt.CURRENT_BALANCE, bal);
                MessageBox.Show("Recharge Request Sent Successfully.\nTransaction Id: " + dict["description"].ToString());
                NavigationService.Navigate(new Uri("/MobileMain.xaml", UriKind.Relative));
                NavigationService.RemoveBackEntry();
            }
            else if (status.Equals("failure"))
            {
                MessageBox.Show(dict["description"].ToString());
            }
            return output4;
        }






    }
}


