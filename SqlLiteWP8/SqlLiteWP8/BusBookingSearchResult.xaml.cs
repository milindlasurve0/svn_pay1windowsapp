﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;

namespace SqlLiteWP8
{
    public partial class BusBookingSearchResult : PhoneApplicationPage
    {
        BusList busList;
        private ProgressIndicator _progressIndicator;
        public BusBookingSearchResult()
        {
            InitializeComponent();
            //busList = new BusList();
            _progressIndicator = new ProgressIndicator
            {
                IsIndeterminate = true,
                Text = "Loading...",
                
                IsVisible = true,
            };
            
            SystemTray.SetIsVisible(this, true);
            SystemTray.SetProgressIndicator(this, _progressIndicator);
            SystemTray.SetOpacity(this, 2);

            var values = new List<KeyValuePair<string, string>>
            {
                 new KeyValuePair<string, string>("method", "busBooking"),
                        new KeyValuePair<string, string>("action","availabletrips"),
                        new KeyValuePair<string, string>("source",BusBookingSearch.srcId),
                        new KeyValuePair<string, string>("destination", BusBookingSearch.destId),
                        new KeyValuePair<string, string>("doj", "2014-02-22")

            };
            Task<string> abc = MakeWebRequest("http://192.168.0.23/index.php/apis/receiveWeb/mindsarray/mindsarray/json", values);
        }


        public async Task<string> MakeWebRequest(string apiUrl, List<KeyValuePair<string, string>> values)
        {

             var httpClient = new HttpClient(new HttpClientHandler());

            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, new FormUrlEncodedContent(values));
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();

            var dict = (JObject)JsonConvert.DeserializeObject(responseString);
            var status = dict["status"];
            if (status.ToString() == "success")
            {
                var allsources = dict["availableTrips"];
                int i = 0;
                foreach (var item in allsources)
                {
                    var ac = item["AC"];
                    var arrivalTime = item["arrivalTime"];
                    var travels = item["travels"];
                    var travelDuration = item["travelDuration"];
                    var departureTime=item["departureTime"];
                    var seater = item["seater"];
                    var sleeper = item["sleeper"];
                    var nonAC = item["nonAC"];
                    var id = item["id"];
                    var fares =item["fares"];
                    string busFare = "";
                    for (int k = 0; k < fares.Count(); k++)
                    {
                        busFare=fares[k].ToString();
                    }
                    var availableSeats = item["availableSeats"];
                    string time = arrivalTime.ToString();
                  
                    List_row row = new List_row();
                    row.Travels.Text = travels.ToString();
                    row.seatAvailable.Text = availableSeats.ToString()+" seats";
                    row.busDuration.Text = travelDuration.ToString();
                    row.busFares.Text = "Rs. "+busFare;
                    if (ac.ToString() == "true")
                    {
                        row.busType.Text = "AC";
                    }

                    row.busTime.Text = departureTime.ToString()+"-"+arrivalTime.ToString();
                    row.busId.Text = id.ToString();
                    
                    
                    L1.Items.Add(row);
                    
                }
                
              
            }
            else if (status.ToString() == "failed")
            {
                //var status = dict["status"];
                MessageBox.Show(dict["description"].ToString());
            }
            _progressIndicator.IsVisible = false;
           // SystemTray.SetIsVisible(this, false);
            return responseString;
        }

        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                List_row row = new List_row();
                var txt = L1.SelectedItem as List_row;
                string busId = txt.busId.Text;
                NavigationService.Navigate(new Uri("/BusBookingDetails.xaml?busId=" + busId, UriKind.Relative));
            }catch(Exception exc){
                Console.WriteLine(exc.StackTrace);
            }
        }
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Home_Click");
        }


        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void L1_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            string text = (L1.SelectedItem as ListBoxItem).Content.ToString();
            Console.WriteLine(text);
        }
    }


    public class BusList : List<BusDetails>
    {
        public BusList()
        {


        }
    }
    public class BusDetails
    {
       

       // public string ac { get; set; }
        //public string time { get; set; }
        public string travels { get; set; }
        //public string travelDuration { get; set; }
        //public string fares {get; set; }
     //   public string seater { get; set; }
       // public string sleeper { get; set; }
        //public string nonAC { get; set; }
     //   public string id { get; set; }
        public string availableSeats { get; set; }
       

    }
}