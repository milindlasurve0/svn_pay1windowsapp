﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Shell;

namespace SqlLiteWP8
{
    public partial class BillPaymentMain : PhoneApplicationPage
    {
        bool isNetOrSms;
        public BillPaymentMain()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            
            isNetOrSms = Constatnt.LoadPersistent<Boolean>("isNetOrSms");
            if (isNetOrSms)
            {
            Task<string> abc = MakeWebRequest("");
            Task<string> abc1 = MakeWebRequestRequest();
           // SetProgressIndicator(true);
            }else{
            }
        }
        /*private void mob00(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MobileMain.xaml", UriKind.Relative));


        }*/

        

        public void SetProgressIndicator(bool value)
        {
            SystemTray.ProgressIndicator.IsIndeterminate = value;
            SystemTray.ProgressIndicator.IsVisible = value;
        }
        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }


        private void mobBSNL(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BillPayment.xaml?id=" + Constatnt.OPERATOR_BILL_CELLONE, UriKind.Relative));
        }

        private void mobIdea(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BillPayment.xaml?id=" + Constatnt.OPERATOR_BILL_IDEA, UriKind.Relative));
        }

        private void mobLoop(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BillPayment.xaml?id=" + Constatnt.OPERATOR_BILL_LOOP, UriKind.Relative));
        }

        private void mobBillReliance(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BillPayment.xaml?id=" + Constatnt.OPERATOR_BILL_RELAINCE, UriKind.Relative));
        }

        private void mobBillAirtel(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BillPayment.xaml?id=" + Constatnt.OPERATOR_BILL_AIRTEL, UriKind.Relative));
        }

        private void mobIndicom(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BillPayment.xaml?id=" + Constatnt.OPERATOR_BILL_TATATELESERVICE, UriKind.Relative));
        }

        private void mobDOCOMO(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BillPayment.xaml?id=" + Constatnt.OPERATOR_BILL_DOCOMO, UriKind.Relative));
        }

        private void mobVodafone(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BillPayment.xaml?id=" + Constatnt.OPERATOR_BILL_VODAFONE, UriKind.Relative));
        }

        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        int page = 0;
        public async Task<string> MakeWebRequest(string date)
        {

            string responseString = "";
            var baseAddress = new Uri(Constatnt.url);
            Button bt = new Button();
            bt.Content = "Add More";
            bt.Foreground = new SolidColorBrush(Colors.Red);
            //listBox1.Items.Remove(bt);
            if(listBox1.Items.Count>0)
            listBox1.Items.RemoveAt(listBox1.Items.Count-1);// (bt);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "reversalTransactions"),
                        new KeyValuePair<string, string>("date",date),
                        new KeyValuePair<string, string>("service", "4"),
                        new KeyValuePair<string, string>("limit","10"),
                        new KeyValuePair<string, string>("page", ""+page++),
                        new KeyValuePair<string, string>("device_type", "")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


            StatusMobileList mov = new StatusMobileList();




            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            int i = 0;
            foreach (var item in desc)
            {
                foreach (var innerItem in item)
                {
                    var timestamp = innerItem["0"];
                    var time = timestamp["timestamp"];

                    var products = innerItem["products"];
                    var name = products["name"];
                    var vendors_activations = innerItem["vendors_activations"];
                    var product_id = vendors_activations["product_id"];
                    var mobile = vendors_activations["mobile"];
                    var amount = vendors_activations["amount"];
                    var status = vendors_activations["status"];
                    i++;


                    StatusRow statusRow = new StatusRow();
                    statusRow.TextMobile.Text = mobile.ToString();
                    statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                    statusRow.TextDate.Text = time.ToString();
                    if (i % 2 == 0)
                    {
                        statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                    }
                    else
                    {
                        statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                    }
                    if (status.ToString() == "1" || status.ToString() == "0")
                    {
                        statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                    }
                    else
                    {
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                    }
                    statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                    listBox1.Items.Add(statusRow);
                   
                }

            }

            if (listBox1.Items.Count % 10 == 0 && listBox1.Items.Count != 0)
            {
               
                bt.Click += (s, e) =>
                {
                    Task<string> abc3 = MakeWebRequest(date);
                };
                listBox1.Items.Add(bt);
            }
            else
            {
            }
            if (listBox1.Items.Count == 0)
            {
                noDataStatusGrid.Visibility = Visibility.Visible;
            }
            SetProgressIndicator(false);
            return responseString;

        }


        public async Task<string> MakeWebRequestRequest()
        {

            string responseString = "";
            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "lastten"),
                       // new KeyValuePair<string, string>("date","2014-02-25"),
                        new KeyValuePair<string, string>("service", "4"),
                        new KeyValuePair<string, string>("device_type", "")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


            StatusMobileList mov = new StatusMobileList();




            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            int i = 0;
            foreach (var item in desc)
            {
                foreach (var innerItem in item)
                {
                    var timestamp = innerItem["0"];
                    var time = timestamp["timestamp"];

                    var products = innerItem["products"];
                    var name = products["name"];
                    var vendors_activations = innerItem["vendors_activations"];
                    var product_id = vendors_activations["product_id"];
                    var mobile = vendors_activations["mobile"];
                    var amount = vendors_activations["amount"];
                    var status = vendors_activations["status"];
                    var id = vendors_activations["id"];
                    i++;


                    StatusRow statusRow = new StatusRow();
                    statusRow.TextMobile.Text = mobile.ToString();
                    statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                    statusRow.index.Text = id.ToString();

                    if (i % 2 == 0)
                    {
                        statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                    }
                    else
                    {
                        statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                    }
                    if (status.ToString() == "1" || status.ToString() == "0")
                    {
                        statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        statusRow.statusIcon.MouseLeave += new MouseEventHandler(panel1_MouseLeave);
                        //statusRow.statusIcon.MouseLeftButtonDown += new MouseButtonEventHandler(panel1_MouseLeave);
                        statusRow.statusIcon.Name = i + "";// d.ToString();
                        statusRow.statusIcon.Tag = i;
                    }
                    else
                    {
                        statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                    }
                    statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                    listBox2.Items.Add(statusRow);
                   
                }

            }
            if (listBox2.Items.Count == 0)
            {
                noDataRequestGrid.Visibility = Visibility.Visible;
            }
            SetProgressIndicator(false);
            return responseString;

        }


        private void panel1_MouseLeave(object sender, System.EventArgs e)
        {
            Image button = (Image)sender;

            string rowIndex = button.Name;


            int index = Convert.ToInt32(rowIndex);
            StatusRow row = (StatusRow)listBox2.Items[index-1];
            string complaintId = row.index.Text;
            MessageBox.Show("Are you sure you want to complaint for " + row.TextMobile.Text + "  number.");
            Task<string> complaint = MakeWebRequestForComplaint(complaintId, index);


        }

        public async Task<string> MakeWebRequestForComplaint(string complaintId, int index)
        {

            string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "reversal"),
                        //new KeyValuePair<string, string>("date","2014-02-10"),
                        new KeyValuePair<string, string>("id", complaintId),
                        
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);







            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            string status1 = dict["status"].ToString();

            if (status1.Equals("success"))
            {
                StatusRow row1 = new StatusRow();
                // button.Source = new BitmapImage(Constatnt.getStatusIcon("4"));
                StatusRow row = (StatusRow)listBox2.Items[index-1];
                row.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon("4"));
                row.statusIcon.Visibility = Visibility.Collapsed;
                listBox2.UpdateLayout();
            }
            else
            {
                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=MobileMain", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }



            return responseString;

        }









        public class StatusMobileList : List<StatusMobile>
        {
            public StatusMobileList()
            {


            }
        }

        public class StatusMobile
        {
            public string Time { get; set; }
            public string Amount { get; set; }
            public string RechargeStatus { get; set; }
            public string Mobile { get; set; }
            public string color { get; set; }
            public Uri ImageUrl { get; set; }
        }

       


        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService,"BIllPaymentMain");
            /* SideBarUserControl s = L1.SelectedValue as SideBarUserControl;
             try
             {
                 MessageBox.Show(s.SideBarTextBlock.Text);
             }catch(Exception e1){
                 System.Diagnostics.Debug.WriteLine(e1.StackTrace);
             }*/
        }

        private void DatePicker_ValueChanged(
         object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            Task<string> abc3 = MakeWebRequest(year + "-" + month + "-" + day);
            //string day=
            //MessageBox.Show(da.ToString());
        }



        public async Task<string> MakeWebRequestSearch(string searchNumber)
        {
            SetProgressIndicator(true);
            string responseString = "";

            //var baseAddress = new Uri("http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?");
            var baseAddress = new Uri(Constatnt.url);
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "mobileTransactions"),
                        new KeyValuePair<string, string>("mobile",searchNumber),
                        new KeyValuePair<string, string>("service", "4"),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);







            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status1 = dict["status"].ToString();
            var desc = dict["description"];
            if (status1.Equals("success"))
            {

                // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
                int i = 0;
                foreach (var item in desc)
                {
                    foreach (var innerItem in item)
                    {
                        var timestamp = innerItem["0"];
                        var time = timestamp["timestamp"];

                        var products = innerItem["products"];
                        var name = products["name"];
                        var vendors_activations = innerItem["vendors_activations"];
                        var product_id = vendors_activations["product_id"];
                        var mobile = vendors_activations["mobile"];
                        var amount = vendors_activations["amount"];
                        var status = vendors_activations["status"];
                        i++;
                        StatusRow statusRow = new StatusRow();
                        statusRow.TextMobile.Text = mobile.ToString();
                        statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                        statusRow.TextDate.Text = time.ToString();
                        if (i % 2 == 0)
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                        }
                        else
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                        }
                        if (status.ToString() == "1" || status.ToString() == "0")
                        {
                            statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        else
                        {
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                        listBox2.Items.Add(statusRow);


                    }

                }
            }
            else
            {
                MessageBox.Show(desc.ToString());
            }

            if (listBox2.Items.Count == 0)
            {
                noDataRequestGrid.Visibility = Visibility.Visible;
            }
            return responseString;

        }



        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string searchNumber = searchText.Text;
            Task<string> abc = MakeWebRequestSearch(searchNumber);

        }








    }
}


