﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.Net.Http;

namespace SqlLiteWP8
{
    public partial class AccountHistory : PhoneApplicationPage
    {
        bool isNetOrSms;
        string historyDate;
        int page = 0;
        public AccountHistory()
        {
            
            InitializeComponent();
            SystemTray.ProgressIndicator = new ProgressIndicator();
            Constatnt.SetProgressIndicator(true);
            //Constatnt.hideControls(border1, border2, balText);
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            /*btn1.Click += Constatnt.Common_MouseHover;
            btn2.Click += Constatnt.Common_MouseHover;*/
            
            try
            {
                isNetOrSms = Constatnt.LoadPersistent<Boolean>("isNetOrSms");
            }
            catch (Exception e4)
            {
                Console.WriteLine(e4.Message);
            }
            // Constatnt.loadSideBar(L1,balanceText);
            if (isNetOrSms)
            {
                TextMerchant.Text = Constatnt.SHOPNAME;
                DateTime thisDay = DateTime.Today;

                int day = thisDay.Day;
                int month = thisDay.Month;
                string mon = "";
                if (month < 10)
                {
                    mon = "0" + month;
                }
                else
                {
                    mon = month.ToString();
                }
                int year = thisDay.Year;
                Task<string> abc3 = MakeWebRequest(day.ToString() + mon + year.ToString());


            }
            else
            {
            }
        }

       /* private void Common_MouseHover(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            string na = btn.Name;
            MessageBox.Show(na);
        }*/

        
        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void DatePicker_ValueChanged(
      object sender, DateTimeValueChangedEventArgs e)
        {
            page = 0;
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            string mon = "";
            if (month < 10)
            {
                mon = "0" + month;
            }
            else
            {
                mon = month.ToString();
            }
            int year = dat.Year;
            listBoxHistory.Items.Clear();
            Task<string> abc3 = MakeWebRequest(day.ToString() + mon+ year.ToString());
            //string day=
            //MessageBox.Show(da.ToString());
        }


        public async Task<string> MakeWebRequest(string date)
        {
            historyDate = date;
            Constatnt.SetProgressIndicator(true);
            //listBoxHistory.Items.Clear();
            string responseString = "";
            Button bt = new Button();
            bt.Content = "Add More";
            bt.Foreground = new SolidColorBrush(Colors.Red);
            // listBox1.Items.Remove(bt);
            if (listBoxHistory.Items.Count > 0)
            listBoxHistory.Items.RemoveAt(listBoxHistory.Items.Count-1);// (bt);
            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "ledgerBalance"),
                        new KeyValuePair<string, string>("date",date+"-"+date),
                new KeyValuePair<string, string>("limit","10"),
                        new KeyValuePair<string, string>("page", ""+page++),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });

                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            string status1 = dict["status"].ToString();

            if (status1.Equals("success"))
            {
                // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
                int i = 0;
                foreach (var item in desc)
                {
                    var trans = item["transactions"];
                    foreach (var innerItem in trans)
                    {
                        var transactions = innerItem["transactions"];
                        string confirm_flag = transactions["confirm_flag"].ToString();
                        string name = transactions["name"].ToString();
                        string refid = transactions["refid"].ToString();
                        string credit = transactions["credit"].ToString();
                        string debit = transactions["debit"].ToString();
                        string timestamp =  transactions["timestamp"].ToString();
                        string time = timestamp.Substring(11,5);
                        string type = transactions["type"].ToString();
                        var opening_closing = innerItem["opening_closing"];
                        string opening = opening_closing["opening"].ToString();
                        string closing = opening_closing["closing"].ToString();

                        i++;

                        HistoryRow historyRow = new HistoryRow();
                        historyRow.TextClosing.Text = closing;
                        historyRow.TextOpening.Text = opening;
                        historyRow.TextTransaction.Text = debit+"Dr";
                        historyRow.TextDate.Text = time;

                        if (i % 2 == 0)
                        {
                            historyRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                        }
                        else
                        {
                            historyRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                        }


                        if (name == "Recharge")
                        {
                            historyRow.TextParticular.Visibility = Visibility.Collapsed;
                            historyRow.operatorIcon.Visibility = Visibility.Visible;
                            historyRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(refid.ToString()));
                            if (confirm_flag.Equals("0"))
                            {
                                historyRow.LayoutRoot.Background = new SolidColorBrush(Colors.Red);
                            }
                        }
                        else
                        {
                            historyRow.operatorIcon.Visibility = Visibility.Collapsed;
                            historyRow.TextParticular.Visibility = Visibility.Visible;
                            historyRow.TextParticular.Text = name;

                            if (type.Equals("21"))
                            {
                                historyRow.LayoutRoot.Background = new SolidColorBrush(Colors.Red);
                            }
                            if (type.Equals("21"))
                            {
                                historyRow.LayoutRoot.Background = new SolidColorBrush(Colors.Red);
                            }
                            if (type.Equals("2")&&confirm_flag.Equals("1"))
                            {
                                historyRow.LayoutRoot.Background = new SolidColorBrush(Colors.Red);
                            }
                            
                        }
                        listBoxHistory.Items.Add(historyRow);
                        
                    }

                }
                if (listBoxHistory.Items.Count % 10 == 0 && listBoxHistory.Items.Count!=0)
                {
                   
                    bt.Click += (s, e) =>
                    {
                        Task<string> abc3 = MakeWebRequest(historyDate);
                    };
                    listBoxHistory.Items.Add(bt);
                }
                else
                {
                }
            }
            else
            {
                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=AccountHistory", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }
            Constatnt.SetProgressIndicator(false);
            return responseString;

        }

        private void btn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("sfgfdg");
        }



    }
}