﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Windows.Input;
using Microsoft.Phone.Tasks;

namespace SqlLiteWP8
{
    public partial class RechargeDTH : PhoneApplicationPage
    {
        string id = "";
        string op_id = "";
        bool isNetOrSms;
        public RechargeDTH()
        {
            InitializeComponent();
            try
            {
                isNetOrSms = Constatnt.LoadPersistent<Boolean>("isNetOrSms");//)IsolatedStorageSettings.ApplicationSettings["isNetOrSms"];
            }
            catch (Exception e4)
            {
                Console.WriteLine(e4.Message);
            }
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            
        }

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }


        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void TxtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (textNumber.Text.Length == 10)
                textAmount.Focus();
        }


        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);



            if (NavigationContext.QueryString.ContainsKey("amt") && NavigationContext.QueryString.ContainsKey("validity") && NavigationContext.QueryString.ContainsKey("desc"))
            {
                string amt = NavigationContext.QueryString["amt"];
                string validity = NavigationContext.QueryString["validity"];
                string desc = NavigationContext.QueryString["desc"];
                //planDesc.Text = desc;
                textAmount.Text = amt;
            }

            BitmapImage btm = null;





            if (NavigationContext.QueryString.TryGetValue("id", out id))


                if (id == Constatnt.ID_OPERATOR_DTH_VIDEOCON)
                {
                    btm = new BitmapImage(new Uri(@"Images/d_videocon.png", UriKind.Relative));
                    operatorName.Text = "Videocon D2h";
                    op_id = "6";

                }
                else if (id == Constatnt.ID_OPERATOR_DTH_TATA_SKY)
                {
                    btm = new BitmapImage(new Uri(@"Images/d_tatasky.png", UriKind.Relative));
                    operatorName.Text = "Tata Sky";
                    op_id = "5";

                }
                else if (id == Constatnt.ID_OPERATOR_DTH_SUN)
                {
                    btm = new BitmapImage(new Uri(@"Images/d_sundirect.png", UriKind.Relative));
                    operatorName.Text = "Sun Direct";
                    op_id = "4";

                }
                else if (id == Constatnt.ID_OPERATOR_DTH_DISHTV)
                {
                    btm = new BitmapImage(new Uri(@"Images/d_dishtv.png", UriKind.Relative));
                    operatorName.Text = "Dish Tv";
                    op_id = "3";
                }
                else if (id == Constatnt.ID_OPERATOR_DTH_AIRTEL)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_airtel.png", UriKind.Relative));
                    operatorName.Text = "Airtel";
                    op_id = "1";
                }
                else if (id == Constatnt.ID_OPERATOR_DTH_BIG)
                {
                    btm = new BitmapImage(new Uri(@"Images/d_bigtv.png", UriKind.Relative));
                    operatorName.Text = "Big Tv";
                    op_id = "2";
                }

            operator_logo.Source = btm;
        }

        private void rechargeNow(object sender, RoutedEventArgs e)
        {
            if (textNumber.Text.Length != 10)
            {
                MessageBox.Show("Please Enter correct Number");
            }
            else if (textAmount.Text.Length == 0)
            {
                MessageBox.Show("Please Enter correct Amount");
            }
            else
            {
                if (int.Parse(textAmount.Text) < 10)
                {
                    MessageBox.Show("Please Enter correct Amount");
                }
                else
                {



                    MessageBoxButton buttons = MessageBoxButton.OKCancel;
                    // Show message box
                    MessageBoxResult result = MessageBox.Show("Operator: " + operatorName.Text + "\nMobile Number: " + textNumber.Text + "\nSub Id: " + textSubsId.Text + "\nAmount:" + textAmount.Text + "\nAre you sure you want to continue?\n", "Mobile Recharge", buttons);
                    //  method=authenticate&mobile=9898120212&password=1234&device_id=d80c9122dfcfd25c32438e12ce1a1c9233e84ac5&type=1&device_type=java
                    if (result == MessageBoxResult.OK)
                    {

                        if (isNetOrSms)
                        {
                            Task<string> str = MakeWebRequestForStatus();
                        }
                        else
                        {
                            string recharge = "*" + id + "*" + textSubsId.Text + "*" + textNumber.Text + "*" + textAmount.Text;
                            SmsComposeTask smsComposeTask = new SmsComposeTask();
                            smsComposeTask.To = "09223178889";
                            smsComposeTask.Body = recharge;

                            smsComposeTask.Show();

                        }






                    }
                }

            }
        }


        /*    public async System.Threading.Tasks.Task<string> MakeWebRequest(List<KeyValuePair<string, string>> values)
            {

                 var httpClient = new HttpClient(new HttpClientHandler());
                HttpResponseMessage response = await httpClient.PostAsync(Constatnt.url, new FormUrlEncodedContent(values));
                response.EnsureSuccessStatusCode();
                var responseString = await response.Content.ReadAsStringAsync();


                string output = responseString.Remove(responseString.Length - 1, 1);
                string output1 = output.Remove(output.Length - 1, 1);
                string output2 = output1.Remove(0, 1);
                RootObjectDthRecharge[] root = JsonConvert.DeserializeObject<RootObjectDthRecharge[]>(output2);


                string status1 = root[0].status;
                if (status1 == "success")
                {
                    MessageBox.Show("Recharge Request Sent Succesfully");
                    Constatnt.mCurrentBalance = root[0].balance;
                    ///NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(root[0].description);
                }



                return responseString;

            }*/


        public async Task<string> MakeWebRequestForStatus()
        {
            byte[] myDeviceID = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");

            string DeviceIDAsString = Convert.ToBase64String(myDeviceID);
            string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
       new KeyValuePair<string, string>("method", "dthRecharge"),
                        new KeyValuePair<string, string>("mobileNumber", textNumber.Text),
                        new KeyValuePair<string, string>("operator",(int.Parse(id)-15)+""),
                        new KeyValuePair<string, string>("subId", textSubsId.Text),
                        new KeyValuePair<string, string>("amount", textAmount.Text),
                        new KeyValuePair<string, string>("type", "flexi"),
                        new KeyValuePair<string, string>("circle", ""),
                        new KeyValuePair<string, string>("special", "0"),
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


            RevStatusList mov = new RevStatusList();




            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status = dict["status"].ToString();
            if (status.Equals("success"))
            {
                bool isOk = Constatnt.SavePersistent(Constatnt.CURRENT_BALANCE, dict["balance"].ToString());
                MessageBox.Show("Recharge Request Sent Successfully.\nTransaction Id: " + dict["description"].ToString());
                NavigationService.Navigate(new Uri("/DTHMain.xaml", UriKind.Relative));
                NavigationService.RemoveBackEntry();
            }
            else if (status.Equals("failure"))
            {
                MessageBox.Show(dict["description"].ToString());
            }
            return output4;
        }

        private void Button_Check_Dth_Plans(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DthPlans.xaml?op_id=" + id, UriKind.Relative));
        }
    }


    public class RootObjectDthRecharge
    {
        public string status { get; set; }
        public string balance { get; set; }
        public string description { get; set; }
    }
}