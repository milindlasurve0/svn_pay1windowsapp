﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class Discount : PhoneApplicationPage
    {
        public Discount()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
           
            Task<string> abc = MakeWebRequest("1");
            Task<string> abc1 = MakeWebRequest("2");
           Task<string> abc2 = MakeWebRequest("3");
        }

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        public async Task<string> MakeWebRequest(string transServiceType)
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";
            ListDisc.Items.Clear();
            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "getCommissions"),
                       
                        new KeyValuePair<string, string>("service", transServiceType),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

         

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            var item1 = desc[0];
            var R = item1["R"];
            int i = 0;
            foreach (var item in R)
            {
                foreach (var innerItem in item)
                {
                    var prodName = innerItem["prodName"];
                    var prodPercent = innerItem["prodPercent"];

                  


                    string color = "";

                 

                    DiscountRow discRow = new DiscountRow();
                    if (i % 2 == 0)
                    {
                        discRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                    }
                    else
                    {
                        discRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                     
                    }
                    discRow.Product.Text = prodName.ToString();
                    discRow.DiscountRate.Text = prodPercent.ToString()+"%";

                    if (transServiceType.Equals("1"))
                    {
                        ListDisc.Items.Add(discRow);
                    }else if (transServiceType.Equals("2"))
                    {
                        ListDthDisc.Items.Add(discRow);
                    }else if (transServiceType.Equals("3"))
                    {
                        ListEntDisc.Items.Add(discRow);
                    }
                    i++;
                }

            }
            Constatnt.SetProgressIndicator(false);
            return responseString;

        }



        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }






    }
}