﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Device.Location;
using Microsoft.Phone.Maps;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Phone.Maps.Controls;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Maps.Services;
using Microsoft.Phone.Maps.Toolkit;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media.Imaging;
using SQLite;
using SqlLiteWP8.Model;
using System.IO.IsolatedStorage;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class LocationAddress : PhoneApplicationPage
    {
        const int MIN_ZOOM_LEVEL = 1;
        const int MAX_ZOOM_LEVEL = 20;
        const int MIN_ZOOMLEVEL_FOR_LANDMARKS = 16;
        GeoCoordinate myGeoCoordinate;
        string latiitude, longitude;
        SQLiteAsyncConnection conn;
        string addressLine, areaLine, cityLine, stateline, postalCode;
        GeoCoordinate currentLocation = null;
        MapLayer locationLayer = null;
        LocationData locatonData;
        public LocationAddress()
        {
            InitializeComponent();
            // ShowMyLocationOnTheMap();
            //  GetLocation();
            myGeoCoordinate = new GeoCoordinate();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            locatonData = new LocationData();
           // setAddressData();
            CreateDatabase();
           
           
        }



        public async static void setAddressData()
        {
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection("plans");
            LocationData location = new LocationData();

            location.address = Constatnt.LoadPersistent<String>("address");
            location.area = Constatnt.LoadPersistent<String>("area_name");
            location.state = Constatnt.LoadPersistent<String>("state_name");
            location.city = Constatnt.LoadPersistent<String>("city_name");
            location.lattitude = Constatnt.LoadPersistent<String>("latitude");
            location.longitude = Constatnt.LoadPersistent<String>("longitude");
            location.zipcode = Constatnt.LoadPersistent<String>("pin");

            List<String> lst = new List<string>();
            var query = conn.Table<LocationData>();
            //string val = "value";
            List<LocationData> result = await query.ToListAsync();
            if (result.Count() >= 1)
            {
                //int i = await conn.ExecuteAsync("UPDATE LocationData Set address = ?,city = ?,area = ?,state = ?,zipcode = ?,lattitude = ?,longitude = ?, WHERE locId = 0", bla, id);// UpdateAsync(location);
            }
            else
            {
                await conn.InsertAsync(location);
            }






            

        }

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }

        private async Task<string> showData()
        {
            var query = conn.Table<LocationData>();
            //string val = "value";
            List<LocationData> result = await query.ToListAsync();
            if (result.Count() == 0)
            {
                ShowMyLocationOnTheMap();
            }
            else
            {
               

            foreach (var item in result)
            {
                try
                {
                    latiitude = item.lattitude;
                    longitude = item.longitude;
                    showPointOnMapFromDB(latiitude, longitude);
                    TextAddressLine.Text = item.address;
                    TextArea.Text = item.area;

                    TextCity.Text = item.city;

                    TextState.Text = item.state;

                    TextZip.Text = item.zipcode;
                }
                catch (Exception e123)
                {
                    System.Diagnostics.Debug.WriteLine(e123.StackTrace);
                }
            }
           
            }
            return "aa";
        }


        private async void CreateDatabase()
        {
            //SQLiteConnection db=new SQLiteConnection();
            conn = new SQLiteAsyncConnection("plans");
            await conn.CreateTableAsync<LocationData>();


        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("latiitude"))
            {
                latiitude = NavigationContext.QueryString["latiitude"];
                if (NavigationContext.QueryString.ContainsKey("longitude"))
                {
                    longitude = NavigationContext.QueryString["longitude"];

                    showPointOnMapFromDBAndMap(latiitude, longitude);

                }
                else
                {
                    Task<string> aa = showData();
                    //ShowMyLocationOnTheMap();
                }
            }
            else
            {
                Task<string> aa = showData();
             //   ShowMyLocationOnTheMap();
            }


        }

        private async void ShowMyLocationOnTheMapByCordinate(double lat, double longi)
        {
            Geolocator myGeolocator = new Geolocator();

            Geoposition myGeoposition = await myGeolocator.GetGeopositionAsync();
            Geocoordinate myGeocoordinate = myGeoposition.Coordinate;

            GeoCoordinate myGeoCoordinate =
                Constatnt.ConvertGeocoordinate(myGeocoordinate);
            this.sampleMap.Center = myGeoCoordinate;
            this.sampleMap.ZoomLevel = 13;
            Geolocator gl = new Geolocator();
            //GeoCoordinate myGeoCoordinate = new GeoCoordinate();
            myGeoCoordinate.Latitude = lat;
            myGeoCoordinate.Longitude = longi;

            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Colors.Blue);
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;


            MapOverlay myLocationOverlay = new MapOverlay();
            myLocationOverlay.Content = myCircle;
            myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
            myLocationOverlay.GeoCoordinate = myGeoCoordinate;



            MapLayer myLocationLayer = new MapLayer();
            myLocationLayer.Add(myLocationOverlay);

            sampleMap.Layers.Add(myLocationLayer);


            string address;
            ReverseGeocodeQuery query = new ReverseGeocodeQuery();
            query.GeoCoordinate = myGeoCoordinate;// new GeoCoordinate(12.56, 77.34);
            query.QueryCompleted += (s, e) =>
            {
                if (e.Error != null)
                    return;

                address = e.Result[0].Information.Address.Street;
                TextAddressLine.Text = address;
                addressLine = address;
                TextArea.Text = e.Result[0].Information.Address.District;
                areaLine = TextArea.Text;
                TextCity.Text = e.Result[0].Information.Address.City;
                cityLine = TextCity.Text;
                TextState.Text = e.Result[0].Information.Address.State;
                stateline = TextState.Text;
                TextZip.Text = e.Result[0].Information.Address.PostalCode;
                postalCode = TextZip.Text;
                // MessageBox.Show(address.ToString());
            };
            query.QueryAsync();
        }



        private async void ShowMyLocationOnTheMapCord()
        {
            /*   Geolocator myGeolocator = new Geolocator();
               Geoposition myGeoposition = await myGeolocator.GetGeopositionAsync();
               Geocoordinate myGeocoordinate = myGeoposition.Coordinate;
               myGeoCoordinate =
                  Constatnt.ConvertGeocoordinate(myGeocoordinate);
               this.sampleMap.Center = myGeoCoordinate;
               this.sampleMap.ZoomLevel = 13;
               */

            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Colors.Blue);
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;



            MapOverlay myLocationOverlay = new MapOverlay();
            myLocationOverlay.Content = myCircle;
            myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
            myLocationOverlay.GeoCoordinate = myGeoCoordinate;



            MapLayer myLocationLayer = new MapLayer();
            myLocationLayer.Add(myLocationOverlay);

            sampleMap.Layers.Add(myLocationLayer);


            string address;
            ReverseGeocodeQuery query = new ReverseGeocodeQuery();
            query.GeoCoordinate = myGeoCoordinate;// new GeoCoordinate(12.56, 77.34);
            query.QueryCompleted += (s, e) =>
            {
                if (e.Error != null)
                    return;

                address = e.Result[0].Information.Address.Street;
                TextAddressLine.Text = address;
                addressLine = address;
                TextArea.Text = e.Result[0].Information.Address.District;
                areaLine = TextArea.Text;
                TextCity.Text = e.Result[0].Information.Address.City;
                cityLine = TextCity.Text;
                TextState.Text = e.Result[0].Information.Address.State;
                stateline = TextState.Text;
                TextZip.Text = e.Result[0].Information.Address.PostalCode;
                postalCode = TextZip.Text;
                // MessageBox.Show(address.ToString());
            };
            query.QueryAsync();
        }




        private async void ShowMyLocationOnTheMap()
        {
            sampleMap.Layers.Clear();
            Geolocator myGeolocator = new Geolocator();
            Geoposition myGeoposition = await myGeolocator.GetGeopositionAsync();
            Geocoordinate myGeocoordinate = myGeoposition.Coordinate;
            myGeoCoordinate =
               Constatnt.ConvertGeocoordinate(myGeocoordinate);
            this.sampleMap.Center = myGeoCoordinate;
            this.sampleMap.ZoomLevel = 13;


            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Colors.Blue);
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;


            MapOverlay myLocationOverlay = new MapOverlay();
            myLocationOverlay.Content = myCircle;
            myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
            myLocationOverlay.GeoCoordinate = myGeoCoordinate;
            latiitude = myGeoCoordinate.Latitude.ToString();
            longitude = myGeoCoordinate.Longitude.ToString();


            MapLayer myLocationLayer = new MapLayer();
            myLocationLayer.Add(myLocationOverlay);

            sampleMap.Layers.Add(myLocationLayer);


            string address;
            ReverseGeocodeQuery query = new ReverseGeocodeQuery();
            query.GeoCoordinate = myGeoCoordinate;// new GeoCoordinate(12.56, 77.34);
            query.QueryCompleted += (s, e) =>
            {
                if (e.Error != null)
                    return;

                address = e.Result[0].Information.Address.Street;
                TextAddressLine.Text = address;
                addressLine = address;
                TextArea.Text = e.Result[0].Information.Address.District;
                areaLine = TextArea.Text;
                TextCity.Text = e.Result[0].Information.Address.City;
                cityLine = TextCity.Text;
                TextState.Text = e.Result[0].Information.Address.State;
                stateline = TextState.Text;
                TextZip.Text = e.Result[0].Information.Address.PostalCode;
                postalCode = TextZip.Text;
                // MessageBox.Show(address.ToString());
            };
            query.QueryAsync();
        }

        async public static void GetLocation()
        {
            try
            {
                var geolocator = new Geolocator();

                Geoposition position = await geolocator.GetGeopositionAsync();

                Geocoordinate coordinate = position.Coordinate;
                MapLayer layer0 = new MapLayer();
                Pushpin pushpin2 = new Pushpin();
                pushpin2.GeoCoordinate = new GeoCoordinate(coordinate.Latitude, coordinate.Longitude);
                MapOverlay overlay2 = new MapOverlay();
                overlay2.Content = pushpin2;
                layer0.Add(overlay2);
               // sampleMap.Layers.Add(layer0);
                // MessageBox.Show("Latitude = " + coordinate.Latitude + " Longitude = " + coordinate.Longitude);



            }
            catch (Exception ee)
            {
                Console.WriteLine(ee.StackTrace);
            }
        }



        private void sampleMap_Loaded(object sender, RoutedEventArgs e)
        {
            MapsSettings.ApplicationContext.ApplicationId = "44aefe8a-fff1-40b9-8a2e-76c835339fdc";
            MapsSettings.ApplicationContext.AuthenticationToken = "hrN1FC4q733LtXFkLPv4kg";
        }



        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }



        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
            Constatnt.handleSidebar(index, this.NavigationService, "NewMainPage");

        }




        private void ShowLocation()
        {
            // Create a small circle to mark the current location.
            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Colors.Blue);
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;

            // Create a MapOverlay to contain the circle.
            MapOverlay myLocationOverlay = new MapOverlay();
            myLocationOverlay.Content = myCircle;
            myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
            myLocationOverlay.GeoCoordinate = currentLocation;

            // Create a MapLayer to contain the MapOverlay.
            locationLayer = new MapLayer();
            locationLayer.Add(myLocationOverlay);

            // Add the MapLayer to the Map.
            sampleMap.Layers.Add(locationLayer);

        }






        private void CenterMapOnLocation()
        {
            sampleMap.Center = currentLocation;
        }

        private void saveLocatin_Click(object sender, RoutedEventArgs e)
        {
            if (TextAddressLine.Text.Trim().Length == 0 || TextArea.Text.Trim().Length == 0 || TextCity.Text.Trim().Length == 0 || TextState.Text.Trim().Length == 0 || TextZip.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please fill all details");
            }
            else
            {
                Task<string>abc= MakeWebRequestForUpdateAddress();
            }
        }

        private void sampleMap_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {


            NavigationService.Navigate(new Uri("/AddLocation.xaml?latiitude=" + latiitude + "&longitude=" + longitude, UriKind.Relative));
            //NavigationService.Navigate(new Uri("/AddLocation.xaml?", UriKind.Relative));

        }



        public async Task<string> MakeWebRequestForUpdateAddress()
        {
         // string mobile=  (String)IsolatedStorageSettings.ApplicationSettings["mobile"];
            string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "updateRetailerAddress"),
                        new KeyValuePair<string, string>("mobile",Constatnt.LoadPersistent<string>("mobile")),
                        new KeyValuePair<string, string>("longitude", longitude),
                        new KeyValuePair<string, string>("latitude", latiitude),
                        new KeyValuePair<string, string>("address",TextAddressLine.Text),
                        new KeyValuePair<string, string>("area",TextArea.Text),
                        new KeyValuePair<string, string>("city", TextCity.Text),
                        new KeyValuePair<string, string>("state",TextState.Text),
                        new KeyValuePair<string, string>("pincode", TextZip.Text)
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);
            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status = dict["status"].ToString();
            var desc = dict["description"];
            if (status.Equals("success"))
            {
                
                //TextAddressLine.Text = desc[];

                
                locatonData.address = TextAddressLine.Text;

                locatonData.area = desc["area"].ToString().Trim().Length==0?TextArea.Text:desc["area"].ToString();
                locatonData.state= desc["state"].ToString().Trim().Length==0?TextState.Text:desc["state"].ToString();
                locatonData.city = desc["city"].ToString().Trim().Length==0?TextCity.Text:desc["city"].ToString();
                locatonData.lattitude = desc["latitude"].ToString().Trim().Length == 0 ? latiitude : desc["latitude"].ToString();
                locatonData.longitude = desc["longitude"].ToString().Trim().Length == 0 ? longitude : desc["longitude"].ToString();
                locatonData.zipcode = desc["pincode"].ToString().Trim().Length == 0 ? TextZip.Text : desc["pincode"].ToString();
                List<String> lst = new List<string>();
                var query = conn.Table<LocationData>();
                //string val = "value";
                
                List<LocationData> result = await query.ToListAsync();
                if (result.Count() >= 1)
                {
                   // int i= await conn.UpdateAsync(locatonData);
                    int i = await conn.ExecuteAsync("UPDATE LocationData Set address = 'umesh',city = 'jobat',area = 'alirajpur',state = 'MP',zipcode = '457990',lattitude = '22.71',longitude = '74.12' WHERE locId = 1");//, locatonData.address, locatonData.city, locatonData.area, locatonData.state, locatonData.zipcode, locatonData.lattitude, locatonData.longitude);// UpdateAsync(location);
                    
                    List<LocationData> result1 = await query.ToListAsync();
                }
                else
                {
                    await conn.InsertAsync(locatonData);
                }
               
                foreach (var item in result)
                {
                    latiitude = item.lattitude;
                    longitude = item.longitude;
                    TextAddressLine.Text = item.address;
                    TextArea.Text = item.area;

                    TextCity.Text = item.city;

                    TextState.Text = item.state;

                    TextZip.Text = item.zipcode;
                }
                NavigationService.Navigate(new Uri("/NewMainPage.xaml?",UriKind.Relative));
                NavigationService.RemoveBackEntry();
            }
            else
            {
                MessageBox.Show(desc.ToString());
            }
          

                

            

    

            return responseString;

        }


        public void showPointOnMapFromDBAndMap(string latiitude, string longitude)
        {
            sampleMap.Center = new GeoCoordinate(Convert.ToDouble(latiitude), Convert.ToDouble(longitude));
            sampleMap.ZoomLevel = 10;
            MapLayer layer0 = new MapLayer();
            Pushpin pushpin0 = new Pushpin();
            // sampleMapFull.map
            pushpin0.GeoCoordinate = new GeoCoordinate(Convert.ToDouble(latiitude), Convert.ToDouble(longitude));
            MapOverlay overlay0 = new MapOverlay();

            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Colors.Blue);
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;


            overlay0.Content = myCircle;
            overlay0.GeoCoordinate = new GeoCoordinate(Convert.ToDouble(latiitude), Convert.ToDouble(longitude));
            layer0.Add(overlay0);
            sampleMap.Layers.Add(layer0);

           string address;
            ReverseGeocodeQuery query = new ReverseGeocodeQuery();
            query.GeoCoordinate = new GeoCoordinate(Convert.ToDouble(latiitude), Convert.ToDouble(longitude));
            query.QueryCompleted += (s, e1) =>
            {
                if (e1.Error != null)
                    return;

                address = e1.Result[0].Information.Address.Street;
                TextAddressLine.Text = address;
                addressLine = address;
                TextArea.Text = e1.Result[0].Information.Address.District;
                areaLine = TextArea.Text;
                TextCity.Text = e1.Result[0].Information.Address.City;
                cityLine = TextCity.Text;
                TextState.Text = e1.Result[0].Information.Address.State;
                stateline = TextState.Text;
                TextZip.Text = e1.Result[0].Information.Address.PostalCode;
                postalCode = TextZip.Text;
                // MessageBox.Show(address.ToString());
            };
            query.QueryAsync();

        }

        public void showPointOnMapFromDB(string latiitude, string longitude)
        {
            sampleMap.Center = new GeoCoordinate(Convert.ToDouble(latiitude), Convert.ToDouble(longitude));
            sampleMap.ZoomLevel = 10;
            MapLayer layer0 = new MapLayer();
            Pushpin pushpin0 = new Pushpin();
            // sampleMapFull.map
            pushpin0.GeoCoordinate = new GeoCoordinate(Convert.ToDouble(latiitude), Convert.ToDouble(longitude));
            MapOverlay overlay0 = new MapOverlay();

            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Colors.Blue);
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;


            overlay0.Content = myCircle;
            overlay0.GeoCoordinate = new GeoCoordinate(Convert.ToDouble(latiitude), Convert.ToDouble(longitude));
            layer0.Add(overlay0);
            sampleMap.Layers.Add(layer0);

            /*    string address;
                ReverseGeocodeQuery query = new ReverseGeocodeQuery();
                query.GeoCoordinate = new GeoCoordinate(Convert.ToDouble(latiitude), Convert.ToDouble(longitude));
                query.QueryCompleted += (s, e1) =>
                {
                    if (e1.Error != null)
                        return;

                    address = e1.Result[0].Information.Address.Street;
                    TextAddressLine.Text = address;
                    addressLine = address;
                    TextArea.Text = e1.Result[0].Information.Address.District;
                    areaLine = TextArea.Text;
                    TextCity.Text = e1.Result[0].Information.Address.City;
                    cityLine = TextCity.Text;
                    TextState.Text = e1.Result[0].Information.Address.State;
                    stateline = TextState.Text;
                    TextZip.Text = e1.Result[0].Information.Address.PostalCode;
                    postalCode = TextZip.Text;
                    // MessageBox.Show(address.ToString());
                };
                query.QueryAsync();*/

        }




    }
}