﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class BillPayment : PhoneApplicationPage
    {
        bool isNetOrSms = true;
        public BillPayment()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid,border1, border2, balText);
            
            SystemTray.ProgressIndicator = new ProgressIndicator();
            SetProgressIndicator(false);
            try
            {
                isNetOrSms = (Boolean)IsolatedStorageSettings.ApplicationSettings["isNetOrSms"];
            }
            catch (Exception e4)
            {
                Console.WriteLine(e4.Message);
            }
            Console.WriteLine("Working");
        }

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }

        public void SetProgressIndicator(bool value)
        {
            SystemTray.ProgressIndicator.IsIndeterminate = value;
            SystemTray.ProgressIndicator.IsVisible = value;
        }


        string id = "";
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);


            BitmapImage btm = null;

            if (NavigationContext.QueryString.TryGetValue("id", out id))


                if (id == Constatnt.OPERATOR_BILL_CELLONE)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_bsnl.png", UriKind.Relative));
                    operatorName.Text = "BSNL";

                }

                else if (id == Constatnt.OPERATOR_BILL_IDEA)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_idea.png", UriKind.Relative));
                    operatorName.Text = "Idea";

                }
                else if (id == Constatnt.OPERATOR_BILL_LOOP)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_loop.png", UriKind.Relative));
                    operatorName.Text = "Loop";
                }

                else if (id == Constatnt.OPERATOR_BILL_VODAFONE)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_vodafone.png", UriKind.Relative));
                    operatorName.Text = "Vodafone";
                }

                else if (id == Constatnt.OPERATOR_BILL_TATATELESERVICE)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_indicom.png", UriKind.Relative));
                    operatorName.Text = "Tata Indicom";
                }
                else if (id == Constatnt.OPERATOR_BILL_DOCOMO)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_docomo.png", UriKind.Relative));
                    operatorName.Text = "Tata DOCOMO";
                }
                else if (id == Constatnt.OPERATOR_BILL_RELAINCE)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_reliance_cdma.png", UriKind.Relative));
                    operatorName.Text = "Relaince";
                }
                else if (id == Constatnt.OPERATOR_BILL_AIRTEL)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_airtel.png", UriKind.Relative));
                    operatorName.Text = "Airtel";
                }



            operator_logo.Source = btm;
        }

        private void TxtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (textBillMobNumber.Text.Length == 10)
                textBillMobAmount.Focus();
        }

        private void payBillNow(object sender, RoutedEventArgs e)
        {
            if (textBillMobNumber.Text.Length != 10)
            {
                MessageBox.Show("Please Enter Correct Number");
            }
            else
            {
                if (textBillMobAmount.Text.Length ==0)
                {
                    MessageBox.Show("Please Enter Correct Amount");
                }
                else
                {

                    MessageBoxButton buttons = MessageBoxButton.OKCancel;
                    // Show message box
                    MessageBoxResult result = MessageBox.Show("Operator: " + operatorName.Text + "\nMobile Number: " + textBillMobNumber.Text + "\nAmount:" + textBillMobAmount.Text + "\nAre you sure you want to continue?\n", "Bill Payment", buttons);
                    //  method=authenticate&mobile=9898120212&password=1234&device_id=d80c9122dfcfd25c32438e12ce1a1c9233e84ac5&type=1&device_type=java
                    if (result == MessageBoxResult.OK)
                    {

                        byte[] myDeviceID = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");

                        string DeviceIDAsString = Convert.ToBase64String(myDeviceID);
                        var values = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("method", "mobBillPayment"),
                        new KeyValuePair<string, string>("mobileNumber", textBillMobNumber.Text),
                        new KeyValuePair<string, string>("operator", id),
                        new KeyValuePair<string, string>("subId", textBillMobNumber.Text),
                        new KeyValuePair<string, string>("amount", textBillMobAmount.Text),
                        new KeyValuePair<string, string>("type", "flexi"),
                        new KeyValuePair<string, string>("circle", ""),
                        new KeyValuePair<string, string>("special", "0"),
                         //new KeyValuePair<string, string>("timestamp", Constatnt.getTime()),
                          //new KeyValuePair<string, string>("hash_code",Constatnt.CalculateSHA1(Constatnt.getDeviceId()+textNumber.Text+textAmount.Text+Constatnt.getTime()) ),
                      // new KeyValuePair<string, string>("device_type", "windows"),
		 
                    };
                        if (isNetOrSms)
                        {
                            SetProgressIndicator(true);
                            Task<string> abc = MakeWebRequest();
                        }
                        else
                        {
                            string recharge = "*" + id + "" + textBillMobNumber.Text + "*" + textBillMobAmount.Text;
                            SmsComposeTask smsComposeTask = new SmsComposeTask();
                            smsComposeTask.To = "09223178889";
                            smsComposeTask.Body = recharge;

                            smsComposeTask.Show();
                        }

                    }
                }

            }

        }
        public async Task<string> MakeWebRequest()
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";
            int op_id=int.Parse(id)-35;
            var baseAddress = new Uri(Constatnt.url);
           // var baseAddress = new Uri("http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?");

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
                        new KeyValuePair<string, string>("method", "mobBillPayment"),
                        new KeyValuePair<string, string>("mobileNumber", textBillMobNumber.Text),
                        new KeyValuePair<string, string>("operator",op_id.ToString()),
                        new KeyValuePair<string, string>("subId", textBillMobNumber.Text),
                        new KeyValuePair<string, string>("amount", textBillMobAmount.Text),
                        new KeyValuePair<string, string>("type", "flexi"),
                        new KeyValuePair<string, string>("circle", ""),
                        new KeyValuePair<string, string>("special", "0"),
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);
            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            //RootObjectRecharge[] root = JsonConvert.DeserializeObject<RootObjectRecharge[]>(responseString);


            string status1 = dict["status"].ToString();
            if (status1.Equals("success"))
            {
                bool isOk = Constatnt.SavePersistent(Constatnt.CURRENT_BALANCE, dict["balance"].ToString());
                MessageBox.Show("Bill Paid Successfully.\nTransaction Id: " + dict["description"].ToString());
                NavigationService.Navigate(new Uri("/BillPaymentMain.xaml", UriKind.Relative));
                NavigationService.RemoveBackEntry();
            }
            else
            {
                MessageBox.Show(dict["description"].ToString());
            }







            SetProgressIndicator(false);
            return responseString;

        }
        public class RootObjectRecharge
        {
            public string status { get; set; }
            public string balance { get; set; }
            public string description { get; set; }
        }
    }
}

/*   // var httpClient = new HttpClient(new HttpClientHandler());
    if (Login.httpClient == null)
    {
        MessageBox.Show("Null");
    }
  /*  HttpResponseMessage response = await Login.httpClient.PostAsync(Constatnt.url, new FormUrlEncodedContent(values));
    response.EnsureSuccessStatusCode();
    var responseString = await response.Content.ReadAsStringAsync();


    string output = responseString.Remove(responseString.Length - 1, 1);
    string output1 = output.Remove(output.Length - 1, 1);
    string output2 = output1.Remove(0, 1);*/

/*   var values1 = new List<KeyValuePair<string, string>>
           {
               new KeyValuePair<string, string>("method", "getSessionVar")
           };
   HttpResponseMessage response1 = await Login.httpClient. Instance.PostAsync(Constatnt.url + "method=mobRecharge&mobileNumber=9637260589&operator=2&subId=9637260589& amount=10&type=flexi&circle=&special=0&timestamp=1392725591312&profile_id=18&hash_code=e6b9d662626e45234c188d702a6c8ef57aa62521& device_type=android", null);
   response1.EnsureSuccessStatusCode();
   var responseString = await response1.Content.ReadAsStringAsync();
   responseString = await response1.Content.ReadAsStringAsync();
   RootObjectRecharge[] root = JsonConvert.DeserializeObject<RootObjectRecharge[]>(responseString);


   string status1 = root[0].status;
   if (status1 == "success")
   {
       MessageBox.Show("Recharge Request Sent Succesfully");
       Constatnt.mCurrentBalance = root[0].balance;
       ///NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
   }
   else
   {
       MessageBox.Show(root[0].description);
   }



   return responseString;

            
//  }
   
private void webClient_DownloadStringCompleted1(object sender, DownloadStringCompletedEventArgs e)
{
   Console.WriteLine(e.Result);
}

}


    
}*/