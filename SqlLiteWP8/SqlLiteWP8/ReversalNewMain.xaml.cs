﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace SqlLiteWP8
{
    public partial class ReversalNewMain : PhoneApplicationPage
    {
         bool isNetOrSms;
        string serviceTypeList;
        public ReversalNewMain()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            
            try
            {
                isNetOrSms = Constatnt.LoadPersistent<Boolean>("isNetOrSms");//)IsolatedStorageSettings.ApplicationSettings["isNetOrSms"];
            }
            catch (Exception e4)
            {
                Console.WriteLine(e4.Message);
            }




            if (isNetOrSms)
            {
                Task<string> abc = MakeWebRequest("", "1");
                Task<string> abc1 = MakeWebRequest("", "2");
                Task<string> abc2 = MakeWebRequest("", "3");
                Task<string> abc3 = MakeWebRequest("", "4");
                Task<string> abc4 = MakeWebRequest("", "5");
            }
            btnStatusRev.IsEnabled = false;
            btnRequestRev.IsEnabled = true;
            statusDate.Visibility = Visibility.Visible;
            reqStackPanel.Visibility = Visibility.Collapsed;
            dateDthStatus.Visibility = Visibility.Visible;
            reqStackPanelDth.Visibility = Visibility.Collapsed;
            statusDateEnt.Visibility = Visibility.Visible;
            reqStackPanelEnt.Visibility = Visibility.Collapsed;
            statusDateBill.Visibility = Visibility.Visible;
            reqStackPanelBill.Visibility = Visibility.Collapsed;
            statusDatePay1.Visibility = Visibility.Visible;
            reqStackPanelPay1.Visibility = Visibility.Collapsed;

           
        }

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }

        private void DatePicker_ValueChangedMob(
          object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            Task<string> abc3 = MakeWebRequest(year + "-" + month + "-" + day,"1");
            //string day=
            //MessageBox.Show(da.ToString());
        }

        private void DatePicker_ValueChangedPay1(
          object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            Task<string> abc3 = MakeWebRequest(year + "-" + month + "-" + day, "5");
            //string day=
            //MessageBox.Show(da.ToString());
        }


        private void DatePicker_ValueChangedDth(
          object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            Task<string> abc3 = MakeWebRequest(year + "-" + month + "-" + day,"2");
            //string day=
            //MessageBox.Show(da.ToString());
        }


        private void DatePicker_ValueChangedEnt(
          object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            Task<string> abc3 = MakeWebRequest(year + "-" + month + "-" + day,"3");
            //string day=
            //MessageBox.Show(da.ToString());
        }

        private void DatePicker_ValueChangedBill(
          object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime dat = (DateTime)e.NewDateTime;
            int day = dat.Day;
            int month = dat.Month;
            int year = dat.Year;
            Task<string> abc3 = MakeWebRequest(year + "-" + month + "-" + day,"4");
            //string day=
            //MessageBox.Show(da.ToString());
        }
        




        public async Task<string> MakeWebRequest(string date,string serviceType)
        {
            string responseString = "";
            ListRevMob.Items.Clear();
            ListRevDth.Items.Clear();
            ListRevEnt.Items.Clear();
            ListRevPay1.Items.Clear();
            ListRevBill.Items.Clear();
            Button bt = new Button();
            bt.Content = "Add More";
            bt.Foreground = new SolidColorBrush(Colors.Red);
            // listBox1.Items.Remove(bt);
            if (ListRevMob.Items.Count>0)
            ListRevMob.Items.RemoveAt(ListRevMob.Items.Count-1);// (bt);
            if (ListRevDth.Items.Count > 0)
            ListRevDth.Items.RemoveAt(ListRevDth.Items.Count-1);// (bt);
            if (ListRevEnt.Items.Count > 0)
            ListRevEnt.Items.RemoveAt(ListRevEnt.Items.Count-1);// (bt);
            if (ListRevPay1.Items.Count > 0)
            ListRevPay1.Items.RemoveAt(ListRevPay1.Items.Count-1);// (bt);
            if (ListRevBill.Items.Count > 0)
            ListRevBill.Items.RemoveAt(ListRevBill.Items.Count-1);// (bt);

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "reversalTransactions"),
                        new KeyValuePair<string, string>("date",date),
                        new KeyValuePair<string, string>("service", serviceType),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            string status1 = dict["status"].ToString();

            if (status1.Equals("success"))
            {
                int i = 0;
                foreach (var item in desc)
                {
                    foreach (var innerItem in item)
                    {
                        var timestamp = innerItem["0"];
                        var time = timestamp["timestamp"];

                        var products = innerItem["products"];
                        var name = products["name"];
                        var vendors_activations = innerItem["vendors_activations"];
                        var product_id = vendors_activations["product_id"];
                        var mobile = vendors_activations["mobile"];
                        var amount = vendors_activations["amount"];
                        var status = vendors_activations["status"];
                        i++;


                        StatusRow statusRow = new StatusRow();
                        statusRow.TextMobile.Text = mobile.ToString();
                        statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                        statusRow.TextDate.Text = time.ToString();
                        if (i % 2 == 0)
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                        }
                        else
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                        }
                        if (status.ToString() == "1" || status.ToString() == "0")
                        {
                            statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        else
                        {
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                        if (serviceType.Equals("1"))
                        {
                            ListRevMob.Items.Add(statusRow);
                        }
                        else if (serviceType.Equals("2"))
                        {
                            ListRevDth.Items.Add(statusRow);
                        }
                        else if (serviceType.Equals("4"))
                        {
                            ListRevBill.Items.Add(statusRow);
                        }
                        else if (serviceType.Equals("3"))
                        {
                            ListRevEnt.Items.Add(statusRow);
                        }
                        else if (serviceType.Equals("5"))
                        {
                            ListRevPay1.Items.Add(statusRow);
                        }



                    }
                }
            }
            else
            {
                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=DTHMain", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }

            if (serviceType.Equals("1"))
            {
                if (ListRevMob.Items.Count == 0)
                {
                    noMobileGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    noMobileGrid.Visibility = Visibility.Collapsed;
                }
            }
            else if (serviceType.Equals("2"))
            {
                if (ListRevDth.Items.Count == 0)
                {
                    noDataDthGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    noDataDthGrid.Visibility = Visibility.Collapsed;
                }
            }
            else if (serviceType.Equals("3"))
            {
                if (ListRevEnt.Items.Count == 0)
                {
                    noDataEntGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    noDataEntGrid.Visibility = Visibility.Collapsed;
                }
            }
            else if (serviceType.Equals("4"))
            {
                if (ListRevBill.Items.Count == 0)
                {
                    noDataBillGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    noDataBillGrid.Visibility = Visibility.Collapsed;
                }
            }
            else if (serviceType.Equals("5"))
            {
                if (ListRevPay1.Items.Count == 0)
                {
                    noDataPay1Grid.Visibility = Visibility.Visible;
                }
                else
                {
                    noDataPay1Grid.Visibility = Visibility.Collapsed;
                }
            }

            Constatnt.SetProgressIndicator(false);
            return responseString;

        }



        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }

        private void BtnStatusClick(object sender, RoutedEventArgs e)
        {
           /* if (btnStatusRev.IsEnabled == true)
            {*/
            Task<string> abc = MakeWebRequest("", "1");
            Task<string> abc1 = MakeWebRequest("", "2");
            Task<string> abc2 = MakeWebRequest("", "3");
            Task<string> abc3 = MakeWebRequest("", "4");
            Task<string> abc4 = MakeWebRequest("", "5");

                btnStatusRev.IsEnabled = false;
                btnRequestRev.IsEnabled = true;
                statusDate.Visibility = Visibility.Visible;
                reqStackPanel.Visibility = Visibility.Collapsed;
                dateDthStatus.Visibility = Visibility.Visible;
                reqStackPanelDth.Visibility = Visibility.Collapsed;
                statusDateEnt.Visibility = Visibility.Visible;
                reqStackPanelEnt.Visibility = Visibility.Collapsed;
                reqStackPanelBill.Visibility = Visibility.Collapsed;
                statusDateBill.Visibility = Visibility.Visible;
                reqStackPanelPay1.Visibility = Visibility.Collapsed;
                statusDatePay1.Visibility = Visibility.Visible;
           /* }
            else
            {
                btnStatusRev.IsEnabled = true;
                btnRequestRev.IsEnabled = false;
            }*/
        }


        
        private void BtnReversalClick(object sender, RoutedEventArgs e)
        {
            btnStatusRev.IsEnabled = true;
            btnRequestRev.IsEnabled = false;
            Task<string> abc3 = MakeWebRequestForRequest("1");
            Task<string> abc4 = MakeWebRequestForRequest("2");
            Task<string> abc5 = MakeWebRequestForRequest("3");
            Task<string> abc6 = MakeWebRequestForRequest("4");
            Task<string> abc7 = MakeWebRequestForRequest("5");
            reqStackPanel.Visibility = Visibility.Visible;
            statusDate.Visibility = Visibility.Collapsed;
            reqStackPanelDth.Visibility = Visibility.Visible;
            dateDthStatus.Visibility = Visibility.Collapsed;
            reqStackPanelEnt.Visibility = Visibility.Visible;
            statusDateEnt.Visibility = Visibility.Collapsed;
            reqStackPanelBill.Visibility = Visibility.Visible;
            statusDateBill.Visibility = Visibility.Collapsed;
            reqStackPanelPay1.Visibility = Visibility.Visible;
            statusDatePay1.Visibility = Visibility.Collapsed;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string searchNumber = searchText.Text;
            if (searchNumber.Trim().Length != 10)
            {
                Task<string> abc = MakeWebRequestSearch(searchNumber,"1");
            }
            else
            {
                MessageBox.Show("Please enter correct number.");
            }

        }

        private void Button_Click_Bill(object sender, RoutedEventArgs e)
        {
            string searchNumber = searchText.Text;
            if (searchNumber.Trim().Length != 10)
            {
                Task<string> abc = MakeWebRequestSearch(searchNumber, "4");
            }
            else
            {
                MessageBox.Show("Please enter correct number.");
            }

        }

        private void Button_Click_Dth(object sender, RoutedEventArgs e)
        {
            string searchNumber = searchText.Text;
            if (searchNumber.Trim().Length != 10)
            {
                Task<string> abc = MakeWebRequestSearch(searchNumber, "2");
            }
            else
            {
                MessageBox.Show("Please enter correct number.");
            }

        }

        private void Button_Click_Ent(object sender, RoutedEventArgs e)
        {
            string searchNumber = searchText.Text;
            if (searchNumber.Trim().Length != 10)
            {
                Task<string> abc = MakeWebRequestSearch(searchNumber, "3");
            }
            else
            {
                MessageBox.Show("Please enter correct number.");
            }

        }
        private void Button_Click_Pay1(object sender, RoutedEventArgs e)
        {
            string searchNumber = searchText.Text;
            if (searchNumber.Trim().Length != 10)
            {
                Task<string> abc = MakeWebRequestSearch(searchNumber, "5");
            }
            else
            {
                MessageBox.Show("Please enter correct number.");
            }

        }




        public async Task<string> MakeWebRequestSearch(string searchNumber,string serviceType)
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";

            //var baseAddress = new Uri("http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?");
            var baseAddress = new Uri(Constatnt.url);
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "mobileTransactions"),
                        new KeyValuePair<string, string>("mobile",searchNumber),
                        new KeyValuePair<string, string>("service", serviceType),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);







            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status1 = dict["status"].ToString();
            var desc = dict["description"];
            if (status1.Equals("success"))
            {

                // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
                int i = 0;
                foreach (var item in desc)
                {
                    foreach (var innerItem in item)
                    {
                        var timestamp = innerItem["0"];
                        var time = timestamp["timestamp"];

                        var products = innerItem["products"];
                        var name = products["name"];
                        var vendors_activations = innerItem["vendors_activations"];
                        var product_id = vendors_activations["product_id"];
                        var mobile = vendors_activations["mobile"];
                        var amount = vendors_activations["amount"];
                        var status = vendors_activations["status"];
                        i++;
                        StatusRow statusRow = new StatusRow();
                        statusRow.TextMobile.Text = mobile.ToString();
                        statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                        statusRow.TextDate.Text = time.ToString();
                        if (i % 2 == 0)
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                        }
                        else
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                        }
                        if (status.ToString() == "1" || status.ToString() == "0")
                        {
                            statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        else
                        {
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                        if (serviceType.Equals("1"))
                        {
                            ListRevMob.Items.Add(statusRow);
                        }
                        else if (serviceType.Equals("2"))
                        {
                            ListRevDth.Items.Add(statusRow);
                        }
                        else if (serviceType.Equals("3"))
                        {
                            ListRevEnt.Items.Add(statusRow);
                        }


                    }

                }
            }
            else
            {
                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=DTHMain", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }

            if (serviceType.Equals("1"))
            {
                if (ListRevMob.Items.Count == 0)
                {
                    noMobileGrid.Visibility = Visibility.Visible;
                }
            }
            else if (serviceType.Equals("2"))
            {
                if (ListRevDth.Items.Count == 0)
                {
                    noDataDthGrid.Visibility = Visibility.Visible;
                }
            }
            else if (serviceType.Equals("3"))
            {
                if (ListRevEnt.Items.Count == 0)
                {
                    noDataEntGrid.Visibility = Visibility.Visible;
                }
            }
            else if (serviceType.Equals("4"))
            {
                if (ListRevBill.Items.Count == 0)
                {
                    noDataBillGrid.Visibility = Visibility.Visible;
                }
            }
            else if (serviceType.Equals("5"))
            {
                if (ListRevPay1.Items.Count == 0)
                {
                    noDataPay1Grid.Visibility = Visibility.Visible;
                }
            }


            Constatnt.SetProgressIndicator(false);
            return responseString;

        }







        public async Task<string> MakeWebRequestForRequest(string serviceType)
        {
            Constatnt.SetProgressIndicator(true);
            ListRevMob.Items.Clear();
            ListRevDth.Items.Clear();
            ListRevEnt.Items.Clear();
            ListRevBill.Items.Clear();
            ListRevPay1.Items.Clear();

            string responseString = "";

           
            var baseAddress = new Uri(Constatnt.url);
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "lastten"),
                        //new KeyValuePair<string, string>("date",date),
                        new KeyValuePair<string, string>("service",serviceType),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


           




            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status1 = dict["status"].ToString();
            var desc = dict["description"];
            if (status1.Equals("success"))
            {
                // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
                int i = 0;
                foreach (var item in desc)
                {
                    foreach (var innerItem in item)
                    {
                        var timestamp = innerItem["0"];
                        var time = timestamp["timestamp"];

                        var products = innerItem["products"];
                        var name = products["name"];
                        var vendors_activations = innerItem["vendors_activations"];
                        var product_id = vendors_activations["product_id"];
                        var mobile = vendors_activations["mobile"];
                        var amount = vendors_activations["amount"];
                        var status = vendors_activations["status"];
                        var id = vendors_activations["id"];
                        i++;
                        StatusRow statusRow = new StatusRow();
                        statusRow.TextMobile.Text = mobile.ToString();
                        statusRow.TextAmt.Text = "Rs. " + amount.ToString();
                        statusRow.TextDate.Text = time.ToString();
                        statusRow.index.Text = id.ToString();

                        if (i % 2 == 0)
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Colors.White);
                        }
                        else
                        {
                            statusRow.LayoutRoot.Background = new SolidColorBrush(Constatnt.ConvertStringToColor("#E2F1DE"));
                        }
                        if (status.ToString() == "1" || status.ToString() == "0")
                        {
                            statusRow.statusIcon.Source = new BitmapImage(Constatnt.getStatusIcon("6"));
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                            statusRow.statusIcon.MouseLeave += new MouseEventHandler(panel1_MouseLeave);
                            //statusRow.statusIcon.MouseLeftButtonDown += new MouseButtonEventHandler(panel1_MouseLeave);
                            statusRow.statusIcon.Name = i + "";// d.ToString();
                            statusRow.statusIcon.Tag = i;
                        }
                        else
                        {
                            statusRow.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon(status.ToString()));
                        }
                        statusRow.operatorIcon.Source = new BitmapImage(Constatnt.getOperatorUri(product_id.ToString()));
                        if (serviceType.Equals("1"))
                        {
                            ListRevMob.Items.Add(statusRow);
                        }
                        else if (serviceType.Equals("2"))
                        {
                            ListRevDth.Items.Add(statusRow);
                        }
                        else if (serviceType.Equals("3"))
                        {
                            ListRevEnt.Items.Add(statusRow);
                        }
                        else if (serviceType.Equals("4"))
                        {
                            ListRevBill.Items.Add(statusRow);
                        }
                        else if (serviceType.Equals("5"))
                        {
                            ListRevPay1.Items.Add(statusRow);
                        }
                       

                        serviceTypeList = serviceType;

                    }

                }

            }
            else
            {
                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=MobileMain", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }
            // listBox1.ItemsSource = mov;
            // listBox2.ItemsSource = mov;

            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output2);


            if (serviceType.Equals("1"))
            {
                if (ListRevMob.Items.Count == 0)
                {
                    noMobileGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    noMobileGrid.Visibility = Visibility.Collapsed;
                }
            }
            else if (serviceType.Equals("2"))
            {
                if (ListRevDth.Items.Count == 0)
                {
                    noDataDthGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    noDataDthGrid.Visibility = Visibility.Collapsed;
                }
            }
            else if (serviceType.Equals("3"))
            {
                if (ListRevEnt.Items.Count == 0)
                {
                    noDataEntGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    noDataEntGrid.Visibility = Visibility.Collapsed;
                }
            }
            else if (serviceType.Equals("4"))
            {
                if (ListRevBill.Items.Count == 0)
                {
                    noDataBillGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    noDataBillGrid.Visibility = Visibility.Collapsed;
                }
            }
            else if (serviceType.Equals("5"))
            {
                if (ListRevPay1.Items.Count == 0)
                {
                    noDataPay1Grid.Visibility = Visibility.Visible;
                }
                else
                {
                    noDataPay1Grid.Visibility = Visibility.Collapsed;
                }
            }



            Constatnt.SetProgressIndicator(false);

            return responseString;

        }


        private void panel1_MouseLeave(object sender, System.EventArgs e)
        {
            Image button = (Image)sender;

            string rowIndex = button.Name;

            StatusRow row=null;// = (StatusRow)listBox2.Items[index - 1];
            int index = Convert.ToInt32(rowIndex);

            if (serviceTypeList.Equals("1"))
            {
                row = (StatusRow)ListRevMob.Items[index - 1];
                
            }
            else if (serviceTypeList.Equals("2"))
            {
                row = (StatusRow)ListRevDth.Items[index - 1];
                
            }
            else if (serviceTypeList.Equals("3"))
            {
                row = (StatusRow)ListRevEnt.Items[index - 1];
                
            }
            else if (serviceTypeList.Equals("4"))
            {
                row = (StatusRow)ListRevBill.Items[index - 1];
                
            }



           
            string complaintId = row.index.Text;
            MessageBox.Show("Are you sure you want to complaint for " + row.TextMobile.Text + "  number.");
            Task<string> complaint = MakeWebRequestForComplaint(complaintId, index);


        }

        public async Task<string> MakeWebRequestForComplaint(string complaintId, int index)
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "reversal"),
                        //new KeyValuePair<string, string>("date","2014-02-10"),
                        new KeyValuePair<string, string>("id", complaintId),
                        
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);







            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var desc = dict["description"];
            // RootObjectDthStatus[] root = JsonConvert.DeserializeObject<RootObjectDthStatus[]>(output4);
            string status1 = dict["status"].ToString();

            if (status1.Equals("success"))
            {
                MessageBox.Show("Your complaint registered succesfully.");
                StatusRow row1 = new StatusRow();
                // button.Source = new BitmapImage(Constatnt.getStatusIcon("4"));
                StatusRow row = null;// (StatusRow)listBox2.Items[index];
                if (serviceTypeList.Equals("1"))
                {
                    row = (StatusRow)ListRevMob.Items[index - 1];

                }
                else if (serviceTypeList.Equals("2"))
                {
                    row = (StatusRow)ListRevDth.Items[index - 1];

                }
                else if (serviceTypeList.Equals("3"))
                {
                    row = (StatusRow)ListRevEnt.Items[index - 1];

                }
                else if (serviceTypeList.Equals("4"))
                {
                    row = (StatusRow)ListRevBill.Items[index - 1];

                }


                row.ComplaintIcon.Source = new BitmapImage(Constatnt.getStatusIcon("4"));
                row.statusIcon.Visibility = Visibility.Collapsed;
               
            }
            else
            {
                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=ReversalNewMain", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }

            Constatnt.SetProgressIndicator(false);

            return responseString;

        }

        






    }
}