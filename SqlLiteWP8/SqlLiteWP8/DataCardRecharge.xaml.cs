﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Tasks;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace SqlLiteWP8
{
    public partial class DataCardRecharge : PhoneApplicationPage
    {
        bool isNetOrSms = true;
        string special;
        public DataCardRecharge()
        {

            InitializeComponent();

            try
            {
                isNetOrSms = (Boolean)IsolatedStorageSettings.ApplicationSettings["isNetOrSms"];
            }
            catch (Exception e4)
            {
                Console.WriteLine(e4.Message);
            }
            Console.WriteLine("Working");
        }

        string id = "";
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);


            BitmapImage btm = null;

            if (NavigationContext.QueryString.TryGetValue("id", out id))


                if (id == Constatnt.ID_OPERATOR_AIRTEL)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_airtel.png", UriKind.Relative));
                    operatorName.Text = "Airtel";
                    special = "0";

                }
                else if (id == Constatnt.ID_OPERATOR_AIRCEL)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_aircel.png", UriKind.Relative));
                    operatorName.Text = "Aircel";
                    special = "0";

                }
                else if (id == Constatnt.ID_OPERATOR_IDEA)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_idea.png", UriKind.Relative));
                    operatorName.Text = "Idea";
                    special = "0";

                }

                else if (id == Constatnt.ID_OPERATOR_BSNL)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_bsnl.png", UriKind.Relative));
                    operatorName.Text = "BSNL";
                    special = "1";
                }
                else if (id == Constatnt.ID_OPERATOR_MTNL)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_mtnl.png", UriKind.Relative));
                    operatorName.Text = "MTNL";
                    special = "1";
                }
                else if (id == Constatnt.ID_OPERATOR_MTS)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_mts.png", UriKind.Relative));
                    operatorName.Text = "MTS";
                    special = "0";
                }
                else if (id == Constatnt.ID_OPERATOR_VODAFONE)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_vodafone.png", UriKind.Relative));
                    operatorName.Text = "Vodafone";
                    special = "0";
                }

                else if (id == Constatnt.ID_OPERATOR_DOCOMO)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_docomo.png", UriKind.Relative));
                    operatorName.Text = "Tata DOCOMO";
                    special = "1";
                }
                else if (id == Constatnt.ID_OPERATOR_RELIANCE_CDMA)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_reliance_cdma.png", UriKind.Relative));
                    operatorName.Text = "Reliance Net Connect";
                }
                else if (id == Constatnt.ID_OPERATOR_RELIANCE_GSM)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_reliance.png", UriKind.Relative));
                    operatorName.Text = "Reliance GSM";
                }


            operator_logo.Source = btm;
        }

        private void rechargeNow(object sender, RoutedEventArgs e)
        {
            if (textNumber.Text.Length != 10)
            {
                MessageBox.Show("Please Enter Coreect Number");
            }
            else
            {
                if (int.Parse(textAmount.Text) < 10)
                {
                    MessageBox.Show("Please Enter Coreect Amount");
                }
                else
                {



                    MessageBoxButton buttons = MessageBoxButton.OKCancel;
                    // Show message box
                    MessageBoxResult result = MessageBox.Show("hi", "fdszg", buttons);
                    //  method=authenticate&mobile=9898120212&password=1234&device_id=d80c9122dfcfd25c32438e12ce1a1c9233e84ac5&type=1&device_type=java
                    if (result == MessageBoxResult.OK)
                    {

                        byte[] myDeviceID = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");

                        string DeviceIDAsString = Convert.ToBase64String(myDeviceID);
                      
                        if (isNetOrSms)
                        {
                            Task<string> rec = MakeWebRequestForStatus();
                        }
                        else
                        {
                            string recharge = "*" + id + "" + textNumber.Text + "*" + textAmount.Text;
                            SmsComposeTask smsComposeTask = new SmsComposeTask();
                            smsComposeTask.To = "09223178889";
                            smsComposeTask.Body = recharge;

                            smsComposeTask.Show();
                        }

                    }
                }

            }
        }

        public async Task<string> MakeWebRequestForStatus()
        {
            string responseString = "";
            var baseAddress = new Uri("http://192.168.0.32/apis/receiveWeb/mindsarray/mindsarray/json?");
            //var baseAddress = new Uri("http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?");

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
       new KeyValuePair<string, string>("method", "mobRecharge"),
                        new KeyValuePair<string, string>("mobileNumber", textNumber.Text),
                        new KeyValuePair<string, string>("operator", id),
                        new KeyValuePair<string, string>("subId", textNumber.Text),
                        new KeyValuePair<string, string>("amount", textAmount.Text),
                        new KeyValuePair<string, string>("type", "flexi"),
                        new KeyValuePair<string, string>("circle", ""),
                        new KeyValuePair<string, string>("special", special),
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


            RevStatusList mov = new RevStatusList();




            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status = dict["status"].ToString();
            if (status.Equals("success"))
            {
                MessageBox.Show("Recharge Request Sent Successfully");
                Constatnt.mCurrentBalance = dict["balance"].ToString();
            }
            else if (status.Equals("failure"))
            {
                MessageBox.Show(dict["description"].ToString());
            }
            return output4;
        }

        private void Home_Click(object sender, RoutedEventArgs e)
        {

            MessageBox.Show("Home_Click");
        }


        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }



    }



}