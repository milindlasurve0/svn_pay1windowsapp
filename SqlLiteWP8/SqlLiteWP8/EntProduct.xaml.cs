﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;

namespace SqlLiteWP8
{
    public partial class EntProduct : UserControl
    {
        public EntProduct()
        {
            InitializeComponent();
            TextEntTitle.Text = EntertainmentMain.shortDesc;
            TextDetails.Text = EntertainmentMain.longDesc;
            Uri uri = new Uri(EntertainmentMain.ImageSouce, UriKind.Relative);
            prodImage.Source = new BitmapImage(uri);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/EntertainmentMain.xaml?", UriKind.Relative));
            (Application.Current.RootVisual as PhoneApplicationFrame).RemoveBackEntry();
        }
       
    }
}
