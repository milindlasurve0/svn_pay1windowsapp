﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace SqlLiteWP8
{
    public partial class Settings : PhoneApplicationPage
    {
        public Settings()
        {
            InitializeComponent();
        }
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Home_Click");
        }


        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }
        private void Click_Use_SMS(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/InterNetSetting.xaml", UriKind.Relative));
        }

        private void Click_Update_Mobile(object sender, RoutedEventArgs e)
        {

        }

        private void Click_Change_Pin(object sender, RoutedEventArgs e)
        {

        }
    }
}