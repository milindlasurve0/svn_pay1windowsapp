﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class resetPassword : PhoneApplicationPage
    {
        string oldPass, newPass, confPass;
        public resetPassword()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            
        }
        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            oldPass = oldPassword.Password;
            newPass = newPassword.Password;
            confPass = confNewPassword.Password;


           // if (oldPass.Length == 4 && newPass.Length == 4 && confPass.Length == 4)
            //{
            if (!oldPass.Equals(newPass))
            {
                if (newPass.Equals(confPass))
                {
                    Task<string> abc = MakeWebRequest();
                }
                else
                {
                    MessageBox.Show("Pin does not match.");
                }
            }
            else
            {
                MessageBox.Show("Old and New Pin are same.");
            }
            
           
        }


        public async Task<string> MakeWebRequest()
        {
            string responseString = "";

            //var baseAddress = new Uri("http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?");
            var baseAddress = new Uri(Constatnt.url);
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        




        new KeyValuePair<string, string>("method", "updatePin"),
                        new KeyValuePair<string, string>("newPin",newPass),
                        new KeyValuePair<string, string>("oldPin", oldPass),
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }
            responseString = responseString.Replace("\n", " ").Trim();
            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);



            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            var status = dict["status"];
            var desc = dict["description"];
            if (status.ToString() == "success")
            {
                MessageBox.Show(desc.ToString());
                NavigationService.Navigate(new Uri("/Login.xaml?page=NewMainPage", UriKind.Relative));
                this.NavigationService.RemoveBackEntry();
            }
            else
            {
                var code = dict["code"];
                if (code.ToString().Equals("403"))
                {
                    NavigationService.Navigate(new Uri("/Login.xaml?page=resetPassword", UriKind.Relative));
                }
                else
                {
                    MessageBox.Show(desc.ToString());
                }
            }
            return "hi";
    
    
}


        
        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

    }
}