﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlLiteWP8
{
    public class MyItem
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Description { get; set; }

        public MyItem(string title, string subtitle, string description)
        {
            Title = title;
            Subtitle = subtitle;
            Description = description;
        }
    }
}
