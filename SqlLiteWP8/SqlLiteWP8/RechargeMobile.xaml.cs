﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Tasks;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SqlLiteWP8.Model;
using SQLite;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class RechargeMobile : PhoneApplicationPage
    {
        Notification notifyMsg = new Notification();
       
        string id;
        string oper_id;
        bool isNetOrSms;
        public RechargeMobile()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
           
            try
            {
                isNetOrSms =Constatnt.LoadPersistent<Boolean>("isNetOrSms");//)IsolatedStorageSettings.ApplicationSettings["isNetOrSms"];
            }catch(Exception e4){
                Console.WriteLine(e4.Message);
            }
            Console.WriteLine("Working");
        }

        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (NavigationContext.QueryString.ContainsKey("amt") && NavigationContext.QueryString.ContainsKey("validity") && NavigationContext.QueryString.ContainsKey("desc") && NavigationContext.QueryString.ContainsKey("op_id"))
            {
               string amt = NavigationContext.QueryString["amt"];
               string validity = NavigationContext.QueryString["validity"];
               string desc = NavigationContext.QueryString["desc"];
               id = NavigationContext.QueryString["op_id"];
               oper_id = id;
               planDesc.Visibility = Visibility.Visible;
               rechargeBtn.Margin = new Thickness(21, 412, 0, 0);
               planDesc.Text = desc;
               textAmount.Text = amt;
               setLogo(oper_id);
            }

           

            if (NavigationContext.QueryString.TryGetValue("id", out id))

                oper_id = id;
            setLogo(oper_id);
        }

      public void  setLogo(string id){
          BitmapImage btm = null;
             if (id == Constatnt.ID_OPERATOR_AIRTEL)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_airtel.png", UriKind.Relative));
                    operatorName.Text = "Airtel";
                   
                }
                else if (id == Constatnt.ID_OPERATOR_AIRCEL)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_aircel.png", UriKind.Relative));
                    operatorName.Text = "Aircel";
                    
                }
                else if (id == Constatnt.ID_OPERATOR_IDEA)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_idea.png", UriKind.Relative));
                    operatorName.Text = "Idea";
                   
                }
                else if (id == Constatnt.ID_OPERATOR_LOOP)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_loop.png", UriKind.Relative));
                    operatorName.Text = "Loop";
                }
                else if (id == Constatnt.ID_OPERATOR_BSNL)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_bsnl.png", UriKind.Relative));
                    operatorName.Text = "BSNL";
                }
                else if (id == Constatnt.ID_OPERATOR_MTNL)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_mtnl.png", UriKind.Relative));
                    operatorName.Text = "MTNL";
                }
                else if (id == Constatnt.ID_OPERATOR_MTS)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_mts.png", UriKind.Relative));
                    operatorName.Text = "MTS";
                }
                else if (id == Constatnt.ID_OPERATOR_VODAFONE)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_vodafone.png", UriKind.Relative));
                    operatorName.Text = "Vodafone";
                }
                else if (id == Constatnt.ID_OPERATOR_VIDEOCON)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_videocon.png", UriKind.Relative));
                    operatorName.Text = "Videocon";
                }
                else if (id == Constatnt.ID_OPERATOR_TATA_INDICOM)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_indicom.png", UriKind.Relative));
                    operatorName.Text = "Tata Indicom";
                }
                else if (id == Constatnt.ID_OPERATOR_DOCOMO)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_docomo.png", UriKind.Relative));
                    operatorName.Text = "Tata DOCOMO";
                }
                else if (id == Constatnt.ID_OPERATOR_RELIANCE_CDMA)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_reliance_cdma.png", UriKind.Relative));
                    operatorName.Text = "Reliance CDMA";
                }
                else if (id == Constatnt.ID_OPERATOR_RELIANCE_GSM)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_reliance.png", UriKind.Relative));
                    operatorName.Text = "Reliance GSM";
                }
                else if (id == Constatnt.ID_OPERATOR_UNINOR)
                {
                    btm = new BitmapImage(new Uri(@"Images/m_uninor.png", UriKind.Relative));
                    operatorName.Text = "Uninor";
                }

            operator_logo.Source = btm;
        }



        private void rechargeNow(object sender, RoutedEventArgs e)
        {
            if (textNumber.Text.Length != 10)
            {
                MessageBox.Show("Please Enter correct number");
            }
            else
            {
                if (textAmount.Text.Length==0)
                {
                    MessageBox.Show("Please Enter correct amount");
                }
                else
                {



                    MessageBoxButton buttons = MessageBoxButton.OKCancel;
                    // Show message box
                    MessageBoxResult result = MessageBox.Show( "Operator: "+operatorName.Text+"\nMobile Number: "+textNumber.Text+"\nAmount:"+textAmount.Text+"\nAre you sure you want to continue?\n", "Mobile Recharge", buttons);
                    //  method=authenticate&mobile=9898120212&password=1234&device_id=d80c9122dfcfd25c32438e12ce1a1c9233e84ac5&type=1&device_type=java
                    if (result == MessageBoxResult.OK)
                    {

                        byte[] myDeviceID = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");

                        string DeviceIDAsString = Convert.ToBase64String(myDeviceID);
                        var values = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("method", "mobRecharge"),
                        new KeyValuePair<string, string>("mobileNumber", textNumber.Text),
                        new KeyValuePair<string, string>("operator", id),
                        new KeyValuePair<string, string>("subId", textNumber.Text),
                        new KeyValuePair<string, string>("amount", textAmount.Text),
                        new KeyValuePair<string, string>("type", "flexi"),
                        new KeyValuePair<string, string>("circle", ""),
                        new KeyValuePair<string, string>("special", "0"),
                         //new KeyValuePair<string, string>("timestamp", Constatnt.getTime()),
                          //new KeyValuePair<string, string>("hash_code",Constatnt.CalculateSHA1(Constatnt.getDeviceId()+textNumber.Text+textAmount.Text+Constatnt.getTime()) ),
                      // new KeyValuePair<string, string>("device_type", "windows"),
		 
                    };
                        if (isNetOrSms)
                        {
                            MakeWebRequestForStatus();
                           /* string winPhoneGeekTweetsUrl = @"http://panel.activestores.in/apis/receiveWeb/mindsarray/mindsarray/json?method=mobRecharge&mobileNumber=8422057400&operator=2&subId=9637260589& amount=10&type=flexi&circle=&special=0&timestamp=1392725591312&profile_id=18&hash_code=e6b9d662626e45234c188d702a6c8ef57aa62521& device_type=android";

                           WebClient webClient = new WebClient();
                            webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webClient_DownloadStringCompleted1);
                            webClient.DownloadStringAsync(new Uri(winPhoneGeekTweetsUrl));*/
                        }
                        else
                        {
                            string recharge = "*" + id + "*" + textNumber.Text + "*" + textAmount.Text;
                            SmsComposeTask smsComposeTask = new SmsComposeTask();
                            smsComposeTask.To = "09223178889";
                            smsComposeTask.Body = recharge;
                            
                            smsComposeTask.Show();  
                           
                        }

                    }
                }

            }
        }

       /* public async System.Threading.Tasks.Task<string> MakeWebRequest(List<KeyValuePair<string, string>> values)
        {

           





        /*   // var httpClient = new HttpClient(new HttpClientHandler());
            if (Login.httpClient == null)
            {
                MessageBox.Show("Null");
            }
          /*  HttpResponseMessage response = await Login.httpClient.PostAsync(Constatnt.url, new FormUrlEncodedContent(values));
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);*/

         /*   var values1 = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("method", "getSessionVar")
                    };
            HttpResponseMessage response1 = await Login.httpClient. Instance.PostAsync(Constatnt.url + "method=mobRecharge&mobileNumber=9637260589&operator=2&subId=9637260589& amount=10&type=flexi&circle=&special=0&timestamp=1392725591312&profile_id=18&hash_code=e6b9d662626e45234c188d702a6c8ef57aa62521& device_type=android", null);
            response1.EnsureSuccessStatusCode();
            var responseString = await response1.Content.ReadAsStringAsync();
            responseString = await response1.Content.ReadAsStringAsync();
            RootObjectRecharge[] root = JsonConvert.DeserializeObject<RootObjectRecharge[]>(responseString);


            string status1 = root[0].status;
            if (status1 == "success")
            {
                MessageBox.Show("Recharge Request Sent Succesfully");
                Constatnt.mCurrentBalance = root[0].balance;
                ///NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
            else
            {
                MessageBox.Show(root[0].description);
            }



            return responseString;*/

            /*   HttpResponseMessage response = await http.GetAsync(Constatnt.url+"method=mobRecharge&mobileNumber=9874663110&amount=10&operator=2&type=flexi&subId=9874663110&special=0);
               string js = await response.Content.ReadAsStringAsync();
               MessageBox.Show(js);
               return js;*/
      //  }
   
        private void webClient_DownloadStringCompleted1(object sender, DownloadStringCompletedEventArgs e)
        {
            Console.WriteLine(e.Result);
        }



         public async Task<string> MakeWebRequestForStatus()
        {
                string responseString = "";

            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
       new KeyValuePair<string, string>("method", "mobRecharge"),
                        new KeyValuePair<string, string>("mobileNumber", textNumber.Text),
                        new KeyValuePair<string, string>("operator", id),
                        new KeyValuePair<string, string>("subId", textNumber.Text),
                        new KeyValuePair<string, string>("amount", textAmount.Text),
                        new KeyValuePair<string, string>("type", "flexi"),
                        new KeyValuePair<string, string>("circle", ""),
                        new KeyValuePair<string, string>("special", "0"),
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);


            RevStatusList mov = new RevStatusList();

            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string status = dict["status"].ToString();
            if (status.Equals("success"))
            {
                string bal = dict["balance"].ToString();
                bool isOk = Constatnt.SavePersistent(Constatnt.CURRENT_BALANCE, bal);
                MessageBox.Show("Recharge Request Sent Successfully.\nTransaction Id: " + dict["description"].ToString());
                NavigationService.Navigate(new Uri("/MobileMain.xaml", UriKind.Relative));
                NavigationService.RemoveBackEntry();
            }
            else if(status.Equals("failure"))
            {
                MessageBox.Show(dict["description"].ToString());
            }
             return output4;
}

         private bool _isSettingsOpen = false;
         private void Home_Click(object sender, RoutedEventArgs e)
         {
             Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
             if (_isSettingsOpen)
             {
                 VisualStateManager.GoToState(this, "SettingsClosedState", true);
                 _isSettingsOpen = false;
             }
             else
             {
                 VisualStateManager.GoToState(this, "SettingsOpenState", true);
                 _isSettingsOpen = true;
             }
         }
         private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
         {

             int index = L1.SelectedIndex;
             Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

         }

         private void Help_Click(object sender, RoutedEventArgs e)
         {
             NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
         }

         private void Login_Click(object sender, RoutedEventArgs e)
         {
             NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
         }

         private void Button_Check_Plans(object sender, RoutedEventArgs e)
         {
             NavigationService.Navigate(new Uri("/SelectCircle.xaml?op_id=" + oper_id, UriKind.Relative));
         }

         private void TxtName_KeyDown(object sender, KeyEventArgs e)
         {
             if (textNumber.Text.Length == 10)
                 textAmount.Focus();
         }


    }

    
  
}