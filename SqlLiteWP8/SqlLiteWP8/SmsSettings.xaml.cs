﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;

namespace SqlLiteWP8
{
    public partial class SmsSettings : PhoneApplicationPage
    {
        bool check = false;
        public SmsSettings()
        {
            InitializeComponent();
            bool isNetOrSms = false;
            try
            {
                isNetOrSms = (Boolean)IsolatedStorageSettings.ApplicationSettings["isNetOrSms"];
            }
            catch (Exception e4)
            {
                Console.WriteLine(e4.Message);
            }
            if (isNetOrSms)
            {
                checkInternet.IsChecked = true;
                checkSms.IsChecked = false;
            }
            else
            {
                checkInternet.IsChecked = false;
                checkSms.IsChecked = true;
            }

            
           
        }
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Home_Click");
        }


        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
           
            try
            {
                IsolatedStorageSettings.ApplicationSettings.Clear();
                IsolatedStorageSettings.ApplicationSettings.Add("isNetOrSms", check);
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
            catch (Exception e3)
            {
                Console.WriteLine(check);
            }
           
        }

        private void checkSms_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox chk1 = (CheckBox)sender;

            if ((Boolean)chk1.IsChecked)
            {
                chk1.IsChecked = false;
            }
            else
            {
                chk1.IsChecked = true;
            }
            MessageBox.Show("" + chk1.IsChecked);
        }

        private void checkInternet_Checked(object sender, RoutedEventArgs e)
        {
           
        }
       

        
    }
}