﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SqlLiteWP8.Model;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using SQLite;

using System.Diagnostics;
using System.Windows.Threading;
using System;

namespace SqlLiteWP8
{
    public partial class DthPlans : PhoneApplicationPage
    {
        DthPlanDB DthAllPlans = new DthPlanDB();
        SQLiteAsyncConnection conn;
        string code;
        int i = 0;
        
        DispatcherTimer newTimer;
        string op_id;
        public DthPlans()
        {
            InitializeComponent();

            CreateDatabase();
            
        }


        private void Home_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }


        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }


        private async void getRechargetype()
        {
            List<String> lst=new List<string>();
            var query = conn.Table<DthPlanDB>();
             string val = "value";
             List<DthPlanDB> result = await query.ToListAsync();
             List<string> l = new List<string>();
             if (!result.Exists(x => x.dthPlanType == val))
                 foreach (var item in result)
                 {
                     lst.Add(item.dthPlanType);
                 }

           /*  IEnumerable<string> uniqueValues = lst.SelectMany(x => x).Distinct();
             int uniqueCount = uniqueValues.Count();
            String[] unique = duration.Distinct().ToArray();*/


        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            if (NavigationContext.QueryString.ContainsKey("op_id"))
            {
                op_id = NavigationContext.QueryString["op_id"];
                
                getData();
            }

            /*if (NavigationContext.QueryString.ContainsKey("code"))
            {
                code = NavigationContext.QueryString["code"];
               
                //getRechargetype();
                //

            }*/
            
        }
        
        public async Task<string> MakeWebRequest(string apiUrl)
        {
            String result = "";
            var httpClient = new HttpClient(new HttpClientHandler());

            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, null);
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            try
            {

                string output = responseString.Remove(responseString.Length - 1, 1);
                string output1 = output.Remove(output.Length - 1, 1);
                string output2 = output1.Remove(0, 1);
                string output3 = output2.Remove(0, 1);
                string output4 = output3.Remove(output3.Length - 1, 1);
                var dict = (JObject)JsonConvert.DeserializeObject(output4);
                var desc = dict[op_id];
                int i = 0;

                var prod_code_pay1 = desc["prod_code_pay1"];
                DthAllPlans.dthPlanOpertorID = prod_code_pay1.ToString();

                var opr_name = desc["opr_name"];
                DthAllPlans.dthPlanOperatorName = opr_name.ToString();

                var circles = desc["circles"];


                /*code = prod_code_pay1.ToString();
                var BR = circles[code];
                var circle_id = BR["circle_id"];
                DthAllPlans.dthPlanCircleID = circle_id.ToString();
                var circle_name = BR["circle_name"];
                DthAllPlans.dthPlanCircleName = circle_name.ToString();*/
                 JObject jObject1 = (JObject)JsonConvert.DeserializeObject(circles.ToString());
                 var all = circles["all"];
                 var plans = all["plans"];

                
                     //var plans = kvp1["plans"];
                     JObject jObject = (JObject)JsonConvert.DeserializeObject(plans.ToString());
                     Dictionary<string, string> res = new Dictionary<string, string>(jObject.Count);
                     foreach (var kvp in jObject)
                     {
                         var planType = kvp.Key;
                         var planDetails = kvp.Value;
                         DthAllPlans.dthPlanType = planType.ToString();
                         foreach (var plansDetails in planDetails)
                         {
                             var plan_amt = plansDetails["plan_amt"];
                             DthAllPlans.dthPlanAmount = Convert.ToInt32(plansDetails["plan_amt"].ToString());
                             var plan_validity = plansDetails["plan_validity"];
                             DthAllPlans.dthPlanValidity = plan_validity.ToString();
                             var plan_desc = plansDetails["plan_desc"];
                             DthAllPlans.dthPlanDescription = plan_desc.ToString();
                             DthAllPlans.dthPlanUptateTime = DateTime.Now.ToString();
                             await conn.InsertAsync(DthAllPlans);
                         }
                     }

                 //}




            }
            catch (Exception e)
            {
                Console.WriteLine("");
            }
          

            getData();


            return result;

        }

        private async void CreateDatabase()
        {
            conn = new SQLiteAsyncConnection("plans");
            await conn.CreateTableAsync<DthPlanDB>();
        
                
        }

     

        public async void getData()
        {
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection("plans");

            try
            {
                var query = conn.Table<DthPlanDB>().Where(x =>x.dthPlanOpertorID == op_id);
               // var col = conn.QueryAsync<Plans>("SELECT * FROM Plans WHERE planOpertorID='2' && planCircleID='BR'");
                Console.WriteLine("xfggf");
               
                var result = await query.ToListAsync();
                List<PlanUserControl>[] a = new List<PlanUserControl>[25];
                List<String> ptype = new List<String>();
                foreach (var item in result)
                {
                   PlanUserControl row = new PlanUserControl();
                    row.Validity.Text = item.dthPlanValidity;
                    row.amount.Text = "Rs. "+item.dthPlanAmount;
                    row.PlanDesc.Text = item.dthPlanDescription;
                    string typr = item.dthPlanType;
                    //L1.FindName(item.planType);
                    int x = 0;

                    if (!ptype.Contains(typr))//ptype does not contain itrType
                    {
                        ptype.Add(typr);
                        x = ptype.IndexOf(typr);
                        a[x] = new List<PlanUserControl>();

                    }
                    else { 
                        x = ptype.IndexOf(typr);
                    }
                    a[x].Add(row);

                  i++;
                    
                    //L1.Items.Add(row);
                }
                //Pivot myPivot = new Pivot();
                System.Diagnostics.Debug.WriteLine(a);
                for (int j = 1; j <= a.Count(); j++)
                {
                    if (a[j] != null)
                    {
                       
                        PivotItem myNewPivotItem = new PivotItem();
                        myNewPivotItem.Name = ptype[j];

                        myNewPivotItem.Header = ptype[j];
                        Grid myNewGrid = new Grid();
                        ListBox txtblk = new ListBox();

                        txtblk.ItemsSource = a[j];

                       
                        myNewGrid.Children.Add(txtblk);

                        myNewPivotItem.Content = myNewGrid;

                        myPivot.Items.Add(myNewPivotItem);
                        txtblk.SelectionChanged += new SelectionChangedEventHandler(listBox1_SelectedIndexChanged);
                      
                    }
                }
               
            }
            catch (Exception e2)
            {
                Debug.WriteLine(e2.StackTrace);
            }

            if (i == 0)
            {
                Task<string> abc = MakeWebRequest("https://panel.pay1.in/apis/receiveWeb/mindsarray/mindsarray/json?method=getPlanDetails&operator="+op_id+"&circle=all");
            }
            else
            {

              

            }
            
        }

        private void listBox1_SelectedIndexChanged(object sender, SelectionChangedEventArgs e)
        {
            PlanUserControl control = (sender as ListBox).SelectedItem as PlanUserControl;
            string validity = control.Validity.Text;
            string desc = control.PlanDesc.Text;
            string amt = control.amount.Text;
            NavigationService.Navigate(new Uri("/RechargeMobile.xaml?amt=" + amt+"&validity="+validity+"&desc="+desc, UriKind.Relative));
            NavigationService.RemoveBackEntry();
        }
    }
}