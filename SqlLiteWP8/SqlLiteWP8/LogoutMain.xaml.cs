﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace SqlLiteWP8
{
    public partial class LogoutMain : PhoneApplicationPage
    {
        public LogoutMain()
        {
            InitializeComponent();
        }

        private void Button_LogOut(object sender, RoutedEventArgs e)
        {
            bool isOk = Constatnt.SavePersistent(Constatnt.IS_LOGIN, false);
           // bool mer = Constatnt.SavePersistent("merchant", "");
            Task<string> abc = MakeWebRequest();
        }


        public async Task<string> MakeWebRequest()
        {
            Constatnt.SetProgressIndicator(true);
            string responseString = "";
            
            var baseAddress = new Uri(Constatnt.url);

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var content = new FormUrlEncodedContent(new[]
    {
        new KeyValuePair<string, string>("method", "logout"),
                       
                        new KeyValuePair<string, string>("device_type", "windows8")
    });
                cookieContainer.Add(baseAddress, Login.cooki);
                try
                {
                    //var result = client.PostAsync(url, content).Result;
                    HttpResponseMessage response = await client.PostAsync(Constatnt.url, content);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ed)
                {
                    Console.WriteLine(ed.Message);
                }
            }


            string output = responseString.Remove(responseString.Length - 1, 1);
            string output1 = output.Remove(output.Length - 1, 1);
            string output2 = output1.Remove(0, 1);
            string output3 = output2.Remove(0, 1);
            string output4 = output3.Remove(output3.Length - 1, 1);



            var dict = (JObject)JsonConvert.DeserializeObject(output4);
            string desc = dict["status"].ToString();

            if (desc.Equals("success"))
            {
                MessageBox.Show("You are logged Out successfully.");
                this.NavigationService.Navigate(new Uri("/NewMainPag.xaml?", UriKind.Relative));
                this.NavigationService.GoBack();// RemoveBackEntry();
                //this.NavigationService.BackStack;
            }
            else
            {
                MessageBox.Show("You are not logged Out successfully. \n Please try again.");
            }
           
                

            
            Constatnt.SetProgressIndicator(false);
            return responseString;

        }

        private void Button_Cancel(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }




    }



}