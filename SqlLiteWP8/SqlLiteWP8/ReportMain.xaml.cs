﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Input;

namespace SqlLiteWP8
{
    public partial class ReportMain : PhoneApplicationPage
    {
        public ReportMain()
        {
            InitializeComponent();
            Constatnt.loadSideBar(L1, balanceText, TextMerchant, notificationCount, notiGrid, border1, border2, balText);
            
            List<string> list= new List<String> { "Earnings", "Transaction", "Discount Structure","TopUp","Account History"};
            foreach (string str in list)
            {
                ReportRow row = new ReportRow();
                row.TextType.Text = str;
                ReportListBox.Items.Add(row);
            }
            
            //ReportListBox.ItemsSource = new List<String> { "Earnings", "Transaction", "Discount Structure","TopUp"};
        }

        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            Constatnt.handleButtonClick(btnMob, btnDth, btnEnt, btnComplaint, btnBill, btnReports, this.NavigationService);
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            Constatnt.handleSidebar(index, this.NavigationService, "MobileMain");

        }
        private void btn_back_MouseLeave(object sender, MouseEventArgs e)
        {
            NavigationService.Navigate(new Uri("/NewMainPage.xaml?", UriKind.Relative));
        }
        private void Help_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Help.xaml", UriKind.Relative));
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.Relative));
        }

        private void ReportListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int i = ReportListBox.SelectedIndex;
            Console.WriteLine(""+i);
            if (i == 0)
            {
                NavigationService.Navigate(new Uri("/Earnings.xaml", UriKind.Relative));
            }
            else if (i == 1)
            {
                NavigationService.Navigate(new Uri("/TransactionNew.xaml", UriKind.Relative));
            }
            else if (i == 2)
            {
                NavigationService.Navigate(new Uri("/Discount.xaml", UriKind.Relative));
            }
            else if (i == 3)
            {
                NavigationService.Navigate(new Uri("/TopUp.xaml", UriKind.Relative));
            }
            else if (i == 4)
            {
                NavigationService.Navigate(new Uri("/AccountHistory.xaml", UriKind.Relative));
            }
        }
    }

}