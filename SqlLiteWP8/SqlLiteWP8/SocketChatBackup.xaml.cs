﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.IO;

namespace SqlLiteWP8
{
    public partial class SocketCheck : PhoneApplicationPage
    {
        // Constants
        string chatWith = "adminsupport@dev.pay1.in";
        const int ECHO_PORT = 5222;  // The Echo protocol uses port 7 in this sample
        const int QOTD_PORT = 5222; // The Quote of the Day (QOTD) protocol uses port 17 in this sample
        SocketClient client;
        string result;
        Thread thread;
        public SocketCheck()
        {
            InitializeComponent();
            client = new SocketClient();
            Constatnt.loadSideBar(L1, balanceText);
            TextMerchant.Text = Constatnt.SHOPNAME;
            thread = new Thread(new ThreadStart(this.Thread_ContinuousChecker));
            thread.IsBackground = false;
            thread.Name = "Data Polling Thread";
            result = client.Connect("dev.pay1.in", ECHO_PORT);
            Log(result, false);
            Login();
        }

        private bool _isSettingsOpen = false;
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int index = L1.SelectedIndex;
            if (_isSettingsOpen)
            {
                VisualStateManager.GoToState(this, "SettingsClosedState", true);
                _isSettingsOpen = false;
            }
            else
            {
                VisualStateManager.GoToState(this, "SettingsOpenState", true);
                _isSettingsOpen = true;
            }
            Constatnt.handleSidebar(index, this.NavigationService, "NewMainPage");

        }




        private void Login()
        {



            //Log(String.Format("Connecting to server '{0}' over port {1} (echo) ...", "192.168.0.38", ECHO_PORT), true);


            String str =
                "<?xml version='1.0'?>" +
"<stream:stream xmlns:stream=\"http://etherx.jabber.org/streams\" to=\"dev.pay1.in\" xmlns=\"jabber:client\">" +
                "<iq type='set' is='auth'>" +
"<query xmlns='jabber:iq:auth'>" +
"<username>umesh</username>" +
"<password>123456</password>" +
"<resource>mobile</resource></query></iq><presence/>" +
        "<message to='adminsupport@dev.pay1.in' type='chat'>" +

"<body>Hi</body>" +
"</message>";




            Log(String.Format("Sending '{0}' to server ...", txtInput.Text), true);
            result = client.Send(str);
            Log(result, false);
            if (result == "Success")
            {
                thread.Start();
            }
            else
            {
            }

            Log("Requesting Receive ...", true);
            result = client.Receive();
            if (result.Equals(""))
            {
            }
            else
            {
                parseLoginXml(result);
            }
            Log(result, false);
            ClearLog();
        }

        private void parseLoginXml(string result)
        {

        }

        private void UpdateText(string text)
        {
            // Set the textbox text.
            txtOutput.Text = text;
        }

        public delegate void UpdateTextCallback(string text);



        private void btnGetQuote_Click(object sender, RoutedEventArgs e)
        {

            string msg = txtInput.Text;

            string str4Msg = "<message to=\"" + chatWith + "\" type=\"chat\">" + "<body>" + msg + "</body>" + "</message>";

            string result = client.Send(str4Msg);
            txtInput.Text = String.Empty;
            Log(msg, true);

        }



        private bool ValidateInput()
        {
            // txtInput must contain some text
            if (String.IsNullOrWhiteSpace(txtInput.Text))
            {
                MessageBox.Show("Please enter some text to echo");
                return false;
            }

            return true;
        }

        public void Thread_ContinuousChecker()
        {

            while (true)
            {

                String res = client.Receive();
                client.responseNew = System.String.Empty;
                System.Diagnostics.Debug.WriteLine("Recieve       " + res);
                if (res.Contains("message"))
                {

                    UIThread.Invoke(() => Log(parseMessage(res), false));


                }

                try
                {

                    Thread.Sleep(1000);

                }
                catch (Exception eee)
                {

                }


            }
        }

        private void Log(string message, bool isOutgoing)
        {
            string direction = (isOutgoing) ? ">> " : "<< ";
            txtOutput.Text += Environment.NewLine + direction + message;
        }


        private void ClearLog()
        {
            txtOutput.Text = String.Empty;
        }


        public String parseMessage(String message)
        {
            try
            {


                string parentTag = "<doc>";
                string endparentTag = "</doc>";

                XDocument doc;
                // XDocument ddd = XDocument.Load(message);
                if (message.Contains("<stream:stream xmlns"))
                {
                    doc = XDocument.Parse(message);
                }
                else
                {
                    doc = XDocument.Parse(parentTag + message + endparentTag);
                }







                var items = from i in doc.Descendants("message")
                            select new
                            {
                                to = (string)i.Attribute("to"),

                                type = (string)i.Attribute("type"),
                                fromSt = (string)i.Attribute("from")
                            };


                foreach (var item in items)
                {
                    System.Diagnostics.Debug.WriteLine("To              " + item.to);
                    System.Diagnostics.Debug.WriteLine("Type             " + item.type);
                    System.Diagnostics.Debug.WriteLine("From              " + item.fromSt);
                }


                string math = (string)doc.Element("body");
                /* var entry = XDocument.Parse(xml);
                 int math = (int)entry.Element("mathmarks");
                 int eng = (int)entry.Element("engmarks");
                 int phy = (int)entry.Element("phymarks");
                 */

                var students = doc.Root.Value;// ("body");
                string[] split = students.ToString().Split('|');

                if (split[0].Equals("1"))
                {
                    //string[] split1 = split[1].ToString().Split('/');
                    chatWith = split[1]; //"support-ex3@dev.pay1.in/TelnetClient";// "sandeep@dev.pay1.in/smack";// split1[0];
                    //Login();
                }
                else
                {

                }
                //string val = students.Value("");

                // var bodyTag = doc.Descendants("body").Select(o => o.Value).ToString();
                return students.ToString();
            }
            catch (Exception eee)
            {
                System.Diagnostics.Debug.WriteLine(eee.Message);
                // Log("Error",false);

            }
            return "Error";

        }

        private void txtInput_TextChanged(object sender, TextChangedEventArgs e)
        {

        }



    }

}

